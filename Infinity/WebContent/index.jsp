<!DOCTYPE html>
<html lang="en">

  <head>
    <title>City Bank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/stylecity.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	  
	  
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
	    <div class="container">
	      <!-- <a class="navbar-brand" href="index.html">INFINITY BANK</a> -->
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	          <li class="nav-item"><a href="#home-section" class="nav-link"><span>Home</span></a></li>
	          <li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
	          <!-- <li class="nav-item"><a href="#practice-section" class="nav-link"><span>Account</span></a></li> -->
	          <!-- <li class="nav-item"><a href="#attorneys-section" class="nav-link"><span>Attorneys</span></a></li> -->
	      
	          <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact</span></a></li>
	              <li class="nav-item"><a href="#saving-section" class="nav-link"><span>Login</span></a></li>  
	         <!--  <li class="nav-item cta"><div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="margin-top: 9%;border-radius: 6px">Login
				<span class="caret"></span></button>
				<ul class="dropdown-menu">
				  
				  <li><a href="#modalCustomer" data-toggle="modal" data-target="#modalAppointment">Customer Login</a></li>
				  <li><a href="#modalManager" data-toggle="modal" data-target="#modalAppointment2">Manager Login</a></li>
				</ul>
			
			  </div>
			   <a href="#" class="nav-link" data-toggle="modal" data-target="#modalAppointment">Login</a> 
			</li> -->
	        </ul>	
	      </div>
	    </div>
	  </nav>

	  <section class="hero-wrap js-fullheight" style="background-image: url('images/bg_1.jpg');" data-section="home">
      <div class="overlay"></div>
      <div class="container" >
        <div class="row no-gutters slider-text js-fullheight " data-scrollax-parent="true" >
		  <div class="col-lg-6 ftco-animate mx-auto pt-32 pl-lg-4" data-scrollax=" properties: { translateY: '70%' }" >
				<center>
			<div>	
			<img src="images/I2-logo.png" height="100px"  width="60%" alt="Cinque Terre"></div>
			<h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">INFINITY BANK</h1>
			
			<p class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Money never declines. Money just moves.</p>
				</center>
            <!-- <p><a href="#" class="btn btn-primary py-3 px-4" data-toggle="modal" data-target="#modalAppointment">Request a Quote</a></p> -->
          </div>
        </div>
      </div>
    </section>

  

    <section class="ftco-about ftco-no-pt ftco-no-pb img ftco-section bg-light" id="about-section">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 col-lg-6 d-flex">
    				<div class="img-about img d-flex align-items-stretch">
	    				<div class="img d-flex align-self-stretch align-items-end" style="background-image:url(images/about-1.jpg);">
	    				</div>
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-6 pl-lg-5 py-5">
    				<div class="row justify-content-start pb-3">
		          <div class="col-md-12 heading-section ftco-animate pb-5">
		          	<span class="subheading">About Us</span>
		            <h2 class="mb-4" style="font-size: 44px; text-transform: capitalize;">Who We Are</h2>
		          </div>
		          <div class="col-md-12 testimony-section">
								<div class="owl-carousel carousel-testimony">
									<div class="item">
										<div class="testimony-wrap">
				          		<div class="text mb-5">
				          			<div class="icon">
				          				<span class="icon-quote-left"></span>
									  </div>
				          			<p>“Rather than justice for all, we are evolving into a system of justice for those who can afford it. We have banks that are not only too big to fail, but too big to be held accountable.”	</p>
				          		</div>
				          		<div class="d-flex">
				          			<div class="user-img img mr-3" style="background-image: url(images/oldie1.png);"></div>
				          			<div>
				          				<p class="name mb-0">Joseph E. Stiglitz</p>
			                    <span class="position">American Economist</span>
				          			</div>
				          		</div>
				          	</div>
									</div>
									<div class="item">
										<div class="testimony-wrap">
				          		<div class="text mb-5">
				          			<div class="icon">
				          				<span class="icon-quote-left"></span>
				          			</div>
				          			<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
				          		</div>
				          		<div class="d-flex">
				          			<div class="user-img img mr-3" style="background-image: url(images/oldie1.jpg);"></div>
				          			<div>
				          				<p class="name mb-0">Luis Fox</p>
			                    <span class="position">Businessman</span>
				          			</div>
				          		</div>
				          	</div>
									</div>
									<div class="item">
										<div class="testimony-wrap">
				          		<div class="text mb-5">
				          			<div class="icon">
				          				<span class="icon-quote-left"></span>
				          			</div>
				          			<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
				          		</div>
				          		<div class="d-flex">
				          			<div class="user-img img mr-3" style="background-image: url(images/person_3.jpg);"></div>
				          			<div>
				          				<p class="name mb-0">Luis Fox</p>
			                    <span class="position">Businessman</span>
				          			</div>
				          		</div>
				          	</div>
									</div>
								</div>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>
<!--
    <section class="ftco-section ftco-no-pb ftco-services" id="practice-section">
      <div class="container">
      	<div class="row justify-content-center pb-5">
          <div class="col-md-10 heading-section text-center ftco-animate">
          	<span class="subheading">Practice Areas</span>
            <h2 class="mb-4">Practice Areas</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-5 col-lg-3 ftco-animate py-1 nav-link-wrap">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link px-4 py-1 active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true"><span class="mr-3 flaticon-ideas"></span> Savings Account</a>

              <a class="nav-link px-4 py-1" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false"><span class="mr-3 flaticon-flasks"></span> Business Law</a>

               <a class="nav-link px-4 py-1" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false"><span class="mr-3 flaticon-analysis"></span> Insurance Law</a>

              <a class="nav-link px-4 py-1" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false"><span class="mr-3 flaticon-web-design"></span> Criminal Law</a>


              <a class="nav-link px-4 py-1" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false"><span class="mr-3 flaticon-innovation"></span> Employment Law</a>

              <a class="nav-link px-4 py-1" id="v-pills-6-tab" data-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-6" aria-selected="false"><span class="mr-3 flaticon-idea"></span> Fire Accident</a>

              <a class="nav-link px-4 py-1" id="v-pills-7-tab" data-toggle="pill" href="#v-pills-7" role="tab" aria-controls="v-pills-7" aria-selected="false"><span class="mr-3 flaticon-idea"></span> Financial Law</a>

              <a class="nav-link px-4 py-1" id="v-pills-8-tab" data-toggle="pill" href="#v-pills-8" role="tab" aria-controls="v-pills-8" aria-selected="false"><span class="mr-3 flaticon-idea"></span> Drug Offenses</a>

              <a class="nav-link px-4 py-1" id="v-pills-9-tab" data-toggle="pill" href="#v-pills-9" role="tab" aria-controls="v-pills-9" aria-selected="false"><span class="mr-3 flaticon-idea"></span> Sexual Offenses</a>

              <a class="nav-link px-4 py-1" id="v-pills-10-tab" data-toggle="pill" href="#v-pills-10" role="tab" aria-controls="v-pills-10" aria-selected="false"><span class="mr-3 flaticon-ux-design"></span> Property Law</a> 
            </div>
          </div>
          <div class="col-md-7 col-lg-9 ftco-animate p-4 p-md-5 d-flex align-items-center">
            
            <div class="tab-content pl-lg-4" id="v-pills-tabContent">

              <div class="tab-pane fade show active py-0 py-lg-5" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-family"></span>
		              </div>
		              <div class="text">
		                <h2 class="mb-4">Savings Account</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p> 
		                <p style="text-align:center"><a href="#" class="btn btn-primary px-5 py-3" data-toggle="modal" data-target="#modalAppointment">Login</a></p>
	                </div>
                </div>
              </div>

               <div class="tab-pane fade py-0 py-lg-5" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-auction"></span>
		              </div>
		              <div class="text">
		                <h2 class="mb-4">Business Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->

              <!-- <div class="tab-pane fade py-0 py-lg-5" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-shield"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Insurance Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div>

              <div class="tab-pane fade py-0 py-lg-5" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-handcuffs"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Criminal Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->
<!-- 
              <div class="tab-pane fade py-0 py-lg-5" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-employee"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Employment Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->

              <!-- <div class="tab-pane fade py-0 py-lg-5" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-fire"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Fire Accident</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->
<!-- 
              <div class="tab-pane fade py-0 py-lg-5" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-7-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-money"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Financial Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->

              <!-- <div class="tab-pane fade py-0 py-lg-5" id="v-pills-8" role="tabpanel" aria-labelledby="v-pills-8-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-medicine"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Drug Offenses</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> -->

              <!-- <div class="tab-pane fade py-0 py-lg-5" id="v-pills-9" role="tabpanel" aria-labelledby="v-pills-9-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-handcuffs"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Sexual Offenses</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div> 

              <div class="tab-pane fade py-0 py-lg-5" id="v-pills-10" role="tabpanel" aria-labelledby="v-pills-10-tab">
              	<div class="d-lg-flex">
	              	<div class="icon-law mr-md-4 mr-lg-5">
		                <span class="icon mb-3 d-block flaticon-house"></span>
	                </div>
	                <div class="text">
		                <h2 class="mb-4">Property Law</h2>
		                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt voluptate, quibusdam sunt iste dolores consequatur</p>
		                <p>Inventore fugit error iure nisi reiciendis fugiat illo pariatur quam sequi quod iusto facilis officiis nobis sit quis molestias asperiores rem, blanditiis! Commodi exercitationem vitae deserunt qui nihil ea, tempore et quam natus quaerat doloremque.</p>
		                <p><a href="#" class="btn btn-primary px-4 py-3">Learn More</a></p>
	                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->
<!-- 		
   /* <section class="ftco-section bg-light" id="attorneys-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-10 heading-section text-center ftco-animate">
          	<span class="subheading">About Us</span>
            <h2 class="mb-4">Our Legal Attorneys</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>
        <div class="row">
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/staff-1.jpg);"></div>
							</div>
							<div class="text d-flex align-items-center pt-3 text-center">
								<div>
									<h3 class="mb-2">Lloyd Wilson</h3>
									<span class="position mb-4">CEO, Founder</span>
									<div class="faded">
										<ul class="ftco-social text-center">
			                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
			              </ul>
		              </div>
		            </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/staff-2.jpg);"></div>
							</div>
							<div class="text d-flex align-items-center pt-3 text-center">
								<div>
									<h3 class="mb-2">Rachel Parker</h3>
									<span class="position mb-4">Business Lawyer</span>
									<div class="faded">
										<ul class="ftco-social text-center">
			                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
			              </ul>
		              </div>
		            </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/staff-3.jpg);"></div>
							</div>
							<div class="text d-flex align-items-center pt-3 text-center">
								<div>
									<h3 class="mb-2">Ian Smith</h3>
									<span class="position mb-4">Insurance Lawyer</span>
									<div class="faded">
										<ul class="ftco-social text-center">
			                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
			              </ul>
		              </div>
		            </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/staff-4.jpg);"></div>
							</div>
							<div class="text d-flex align-items-center pt-3 text-center">
								<div>
									<h3 class="mb-2">Alicia Henderson</h3>
									<span class="position mb-4">Criminal Law</span>
									<div class="faded">
										<ul class="ftco-social text-center">
			                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
			                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
			              </ul>
		              </div>
		            </div>
							</div>
						</div>
					</div>
				</div>
    	</div>
    </section> -->

    <!-- <section class="ftco-section" id="blog-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-10 heading-section text-center ftco-animate">
            <span class="subheading">Blog</span>
            <h2 class="mb-4">Our Blog</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry justify-content-end">
          		<div class="text">
          			<h3 class="heading"><a href="single.html">All you want to know about industrial laws</a></h3>
          		</div>
              <a href="single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
              </a>
              <div class="text mt-3 float-right d-block">
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                <div class="d-flex align-items-center mt-4 meta">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry justify-content-end">
          		<div class="text">
          			<h3 class="heading"><a href="single.html">All you want to know about industrial laws</a></h3>
          		</div>
              <a href="single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
              </a>
              <div class="text mt-3 float-right d-block">
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                <div class="d-flex align-items-center mt-4 meta">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex ftco-animate">
          	<div class="blog-entry">
          		<div class="text">
          			<h3 class="heading"><a href="single.html">All you want to know about industrial laws</a></h3>
          		</div>
              <a href="single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
              </a>
              <div class="text mt-3 float-right d-block">
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                <div class="d-flex align-items-center mt-4 meta">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    
    
    
    
    <div class="site-section" id="saving-section" style="padding-top: 50px; padding-bottom: 50px">
            <h2 class="text-md-center font-size-30">Savings Accounts</h2>
        <div class="container">
          <div class="row">
                
            <div class="col-lg-4 mb-5 mb-lg-0" style="text-align:center">
              <div class="block-heading-1">
                    
                
            
              </div>
            </div>
            <div class="row">
              <div class="col pb-4">
              </div>
            </div>
            <span class="border" style="padding-top: 20px ;padding-bottom: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                  <div class="block-team-member-1 text-center rounded">
                   
                    <h3 class="font-size-20">Customer Login</h3>
                    <p class="px-3 mb-3">Proceed directly by entering your loginId and password </p>
  
                    <div class="row">
  
                      <div class="col-md-2"></div>
                    
                      <div class="col-md-8">
                          <form action="ValidationController" method="post">
                            <input type="hidden" name="action" value="login_form_customer"> 
                             <!-- <p style="color:red;"> ${msg }</p> -->
                              <div class="form-group row form-group-lg">
                                <div class="col-md-12 mb-4 mb-lg-0 ">
                                  <input type="text" class="form-control input-sm" placeholder="Email"  name="email">
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="col-md-12">
                                  <input type="password" class="form-control input-sm" placeholder="Password" name="password" >
                                </div>
                              </div>
                              
                              <div class="form-group row">
                                  <div class="col-md-6 mx-auto">
                                    <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
                                  </div>
                                </div>
                            </form>
                      </div>
                      <div class="col-md-2"></div>
  
                    </div>
                     </div>
                </div>
      
                <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                        <div class="block-team-member-1 text-center rounded">
                         
                          <h3 class="font-size-20" style="padding-right: 10px">Manager Login</h3>
                          <p class="px-3 mb-2 " style="text-align: center;word-spacing: 0px">Proceed directly by entering your loginId and password </p>
        
                          <div class="row">
        
                            <div class="col-md-2"></div>
                          
                            <div class="col-md-8">
                                <form action="ValidationController" method="post">
                                  <input type="hidden" name="action" value="login_form_manager"> 
                                   <!-- <p style="color:red;"> ${msg }</p> -->
                                    <div class="form-group row form-group-lg">
                                      <div class="col-md-12 mb-4 mb-lg-0 ">
                                        <input type="text" style="box-align: center" class="form-control input-sm" placeholder="Email"  name="email">
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <div class="col-md-12">
                                        <input type="password" class="form-control input-sm" placeholder="Password" name="password" >
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="col-md-6 mx-auto">
                                          <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
                                        </div>
                                      </div>
                                   </form>
                            </div>
                            <div class="col-md-2"></div>
        
                          </div>
                           </div>
                      </div>
      
                
              </div>
            </span>
          </div>
        </div>
      </div>

    <section class="ftco-section contact-section ftco-no-pb bg-light" id="contact-section">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-10 heading-section text-center ftco-animate">
            <span class="subheading">Contact</span>
            <h2 class="mb-4">Contact Us</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>
        <div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<h3 class="mb-4">Address</h3>
	            <p>Andheri East</p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<h3 class="mb-4">Contact Number</h3>
	            <p><a href="tel://1234567920">022-12345678</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<h3 class="mb-4">Email Address</h3>
	            <p><a href="mailto:info@yoursite.com">infinity@yoursite.com</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-globe"></span>
          		</div>
          		<h3 class="mb-4">Website</h3>
	            <p><a href="#">infinity.com</a></p>
	          </div>
          </div>
        </div>
        <!-- <div class="row no-gutters block-9">
          <div class="col-md-6 order-md-last d-flex">
            <form action="#" class="bg-primary p-5 contact-form">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Subject">
              </div>
              <div class="form-group">
                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Send Message" class="btn btn-darken py-3 px-5">
              </div>
            </form>
          
          </div> -->

          <div class="col-md-6 d-flex">
          	<div id="map" class="bg-white"></div>
          </div>
        </div>
      </div>
    </section>
		

    <footer class="ftco-footer ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">About <span>Infinity</span></h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Links</h2>
              <ul class="list-unstyled">
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Home</a></li>
                <!-- <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>About</a></li> -->
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>About</a></li>
                <!-- <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Attorneys</a></li> -->
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Contact</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Login</a></li>
              </ul>
            </div>
          </div>
          <!-- <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Practice Areas</h2>
              <ul class="list-unstyled">
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Family Law</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Business Law</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Insurance Law</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Criminal Law</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Drug Offenses</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Property Law</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Employment Law</a></li>
              </ul>
            </div>
          </div> -->
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Question?</h2>
            	<div class="block-23 mb-0">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Andheri West</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">022-12345678</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">infinity@yoursite.com</span></a></li>
	              </ul>
	            </div>
	            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

		  

 

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  
  <script src="js/main.js"></script>
    
  </body>
</html>