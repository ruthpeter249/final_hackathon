<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.*"%>
<%@page import="com.ofss.InfiBankBeans.*"%>

<!DOCTYPE html>
<html>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">


<link
	href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900"
	rel="stylesheet">

<link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">

<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="css/jquery.timepicker.css">

<link rel="stylesheet" href="css/flaticon.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/style.css">
</head>



<body>


	<section
		class="ftco-about ftco-no-pt ftco-no-pb ftco-section bg-light pt-4"
		id="about-section">
		<div class="container">
			<div class="row pb-3">
				<div class="col-md-11 heading-section ftco-animate pb-5 pt-3">
					<span class="subheading pt-5 pb-3"><%=session.getAttribute("email") %></span>
					<h2 class="mb-4"
						style="font-size: 44px; text-transform: capitalize;">
						Welcome
						<%=session.getAttribute("managername") %></h2>
				</div>
				<div class="col-md-1 pt-5">


					<form action="ValidationController" method="POST">
					
					<input name="action" value="logout" class="btn btn-primary" type="submit"
						style="margin-top: 9%; border-radius: 6px">
					
					
					</form>
					
				</div>
			</div>
		</div>
		</section>
					<div class="container">
					<nav class="bg-light">
					<div class="nav nav-tabs nav-fill " id="nav-tab" role="tablist">
						<a class="nav-item nav-link <c:if test="${selectedTab == '1'}">active</c:if>" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Transaction Logs</a>
						<a class="nav-item nav-link <c:if test="${selectedTab == '2'}">active</c:if>" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>
						<a class="nav-item nav-link <c:if test="${selectedTab == '3'}">active</c:if>" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Blacklisted customers</a>
						<a class="nav-item nav-link <c:if test="${selectedTab == '4'}">active</c:if>" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Interest Rates</a>
					</div>
				</nav>
				
				<div class="tab-content py-3 px-3 px-sm-0  bg-white" id="tabs">
					<div class="tab-pane fade  <c:if test="${selectedTab == '1'}">active show</c:if>" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

				 <input type="hidden" id="selectedTabInput" value="${requestScope.selectedTab}">
				 
				 
				 ${requestScope.selectedTab}
		

				<!-- input type="hidden" id="selectedTabInput" value="${request.getAttribute("selectedTab")}"> -->
				<%=request.getAttribute("selectedTab")%>
				

				<div class="row">
					
					<form action="ManagerController" method="POST">

						<h3 class="pt-5 pb-3">Refresh and Get Logs whenever API has
							been called for KYC</h3>
						<input type="submit" name="action" value="fetchlogs"
							class="btn btn-primary"
							style="text-align: center; border-radius: 6px">
						</center>


						<div class="col-md-12 col-lg-12 justify-content-center pt-4">

							<table class="table table-hover">

								<thead>
									<tr>
										<th>Ifsc Number</th>
										<th>Name</th>
										<th>branch name</th>
										<th>Time Stamp</th>
										<th>attempts</th>
									</tr>
								</thead>

								<tbody>
								
								
								
								 
								 
									<c:forEach items="${logs}" var="logs">
										<tr>
											<td><c:out value="${logs.ifscNumber}" /></td>
											<td><c:out value="${logs.infiBranchName}" /></td>
											<td><c:out value="${logs.infiFirstname}" /></td>

											<td><c:out value="${logs.infiLogTimestamp}" /></td>
											<td><c:out value="${logs.infiLogAttempt}" /></td>

										</tr>
									</c:forEach>

								</tbody>

							</table>





						</div>


					</form>



			</div>
				


					</div>
					<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
		
				<div class="row">
					<div class="col-md-12 col-lg-12 pl-lg-5 py-5"></div>
					Potato
				</div>

	</div>
					<div class="tab-pane fade <c:if test="${selectedTab == '3'}">active show</c:if>" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
					
					
					
					<div class="row">

					<input type="hidden" id="selectedTabInput" value="${requestScope.selectedTab}">
					
					
					<form action="ManagerController" method="POST">

						<h3 class="pt-5 pb-3">Get customers who have been blackListed</h3>
						<input type="submit" name="action" value="blacklist"
							class="btn btn-primary"
							style="text-align: center; border-radius: 6px">
						</center>


						<div class="col-md-12 col-lg-12 justify-content-center pt-4">

							<table class="table table-hover">

								<thead>
									<tr>
										<th>Customer name</th>
										<th>Customer ID</th>
										<th>Customer EmailID</th>
										<th>Last Login</th>
										<th>Kyc Done</th>
										
									</tr>
								</thead>

								<tbody>
								
									<c:forEach items="${blackList}" var="black">
										<tr>
											<td><c:out value="${black.fname}" /></td>
											<td><c:out value="${black.custId}" /></td>
											<td><c:out value="${black.email}" /></td>

											<td><c:out value="${black.lastLogin}" /></td>
											<td><c:out value="${black.customerKyc}" /></td>

										</tr>
									</c:forEach>

								</tbody>

							</table>





						</div>


					</form>



				</div></div>
					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
					
						<h3 class="pt-5 pb-3">Change Interest Rates</h3>
					
						

			</div>
				</div>



			

		</div>



</body>

<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/jquery.timepicker.min.js"></script>
<script src="js/scrollax.min.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="js/google-map.js"></script>

<script src="js/main.js"></script>

<script>
    $(function() {
        var param = document.getElementById("selectedTabInput").value;
        if (param != 0) {
            $('#tabs').tabs({
            	
                active : param
            });
        } else {
            $('#tabs').tabs();
        }
    });
</script>




</html>