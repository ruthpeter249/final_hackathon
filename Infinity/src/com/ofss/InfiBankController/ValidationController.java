package com.ofss.InfiBankController;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.InfiBankBeans.InfiBankCustomer;
import com.ofss.InfiBankDao.DBAccount;

@WebServlet("/ValidationController")
public class ValidationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	    HttpSession session;
	    DataSource ds;
		Connection con;
		ArrayList<InfiBankCustomer> list;
		@Override
		public void init(ServletConfig config) throws ServletException {
		try {
		InitialContext initcontext = new InitialContext();
		Context c = (Context) initcontext.lookup("java:comp/env");
		ds =(DataSource) c.lookup("jdbc/UsersDB1");

		} catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String action = request.getParameter("action");
			if(action== null){
			session = request.getSession();

			session.setAttribute("ds", ds);
			//request.getRequestDispatcher("result.jsp").forward(request, response);
			}
			else{
			doPost(request, response);
			}
		
			
		}

		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			//doGet(request, response);
			DBAccount account=new DBAccount();
			ManagerController managerDetails = new ManagerController();
			String action=request.getParameter("action");
			InfiBankCustomer customer=new InfiBankCustomer();
			session=request.getSession();
			System.out.println("hiii");
			try {
				con = ds.getConnection();
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}

				account.setConnection(con);
			
			if(action.equals("sign_up_form"))
			{
							System.out.println("inside signup form ");
							String fname=request.getParameter("fname");
							String lname=request.getParameter("lname");
							String email=request.getParameter("email");
							String password=request.getParameter("password");
							String repassword=request.getParameter("repassword");
							
							
							boolean status=customer.validateSignUpForm(fname,lname,email,password,repassword);
							if(status==true){
								System.out.println("Inside status true ");
								request.setAttribute("email", email);	
								request.setAttribute("msg", "Registration Successful,Please login again");
								request.getRequestDispatcher("index.jsp").forward(request, response);
								  
							
								System.out.println("Going in db");
							try {
								account.insertCustomer(fname,lname,email,password);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.println("Inserted one customer");
								
								
							}else{
								request.setAttribute("fname",fname);
								request.setAttribute("lname",lname);
								request.setAttribute("email",email);
								request.setAttribute("password",password);
								request.setAttribute("repassword",repassword);
								
								request.setAttribute("fnamemsg",customer.getFnamemsg());
								request.setAttribute("lnamemsg",customer.getLnamemsg());
								request.setAttribute("emailmsg",customer.getEmailmsg());
								request.setAttribute("passwordmsg",customer.getPasswordmsg());
								request.setAttribute("repasswordmsg",customer.getRepasswordmsg());
								
								request.getRequestDispatcher("index.jsp").forward(request, response);
								
							}

							
		}	

			if(action.equals("bank_acc_create"))
			{
							System.out.println("inside signup form ");
							String fname=request.getParameter("fname");
							String mname=request.getParameter("mname");
							String lname=request.getParameter("lname");
							String caddress=request.getParameter("caddress");
							String paddress=request.getParameter("paddress");
							String phonenumber=request.getParameter("phonenumber");
							
							String email=request.getParameter("email");
							String password=request.getParameter("password");
							String repassword=request.getParameter("repassword");
							
							
							boolean status=customer.validateUser(fname,mname,lname,caddress,paddress,phonenumber,email,password,repassword);
							if(status==true){
								System.out.println("Inside status true ");
								request.setAttribute("email", email);	
								request.setAttribute("msg", "Registration Successful,Please login again");
								request.getRequestDispatcher("index.jsp").forward(request, response);
								  
							
								
								//customer.populate1(email,password);
								
								
							}else{
								request.setAttribute("fname",fname);
								request.setAttribute("mname",mname);
								request.setAttribute("lname",lname);
								request.setAttribute("caddress",caddress);
								request.setAttribute("paddress",paddress);
								request.setAttribute("phonenumber",phonenumber);
								request.setAttribute("email",email);
								request.setAttribute("password",password);
								request.setAttribute("repassword",repassword);
								
								request.setAttribute("fnamemsg",customer.getFnamemsg());
								request.setAttribute("mnamemsg",customer.getMnamemsg());
								request.setAttribute("lnamemsg",customer.getLnamemsg());
								request.setAttribute("caddressmsg",customer.getCaddressmsg());
								request.setAttribute("paddressmsg",customer.getPaddressmsg());
								request.setAttribute("emailmsg",customer.getEmailmsg());
								request.setAttribute("passwordmsg",customer.getPasswordmsg());
								request.setAttribute("repasswordmsg",customer.getRepasswordmsg());
								
								request.getRequestDispatcher("index.jsp").forward(request, response);
								
							}

							
		}	
							
						
			if(action.equals("logout")) { 
	            System.out.println("logout section");
	            //invalidate the session if exists
	            HttpSession session = request.getSession(false);
	            if(session != null){
	                session.invalidate();
	            	
	            }
	            //System.out.println("session id"+session.getId());
	            response.sendRedirect(request.getContextPath() + "/index.jsp");
	        	//request.getRequestDispatcher("login.jsp").forward(request, response);
			}
			
			
			if(action.equals("login_form_customer")){
				String email=request.getParameter("email");
				String password=request.getParameter("password");
				
				boolean status=false;
				try {
				status = account.checkCustomerLogin(email, password);
				} catch (ClassNotFoundException |SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(status);
				if(status==true){
			            //get the old session and invalidate
			            HttpSession oldSession = request.getSession(false);
			            if (oldSession != null) {
			                oldSession.invalidate();
			            }
					session=request.getSession(true);  
					  //setting session to expiry in 5 mins
		            session.setMaxInactiveInterval(5*60);

		            Cookie message = new Cookie("message", "Welcome");
		            response.addCookie(message);
		        	System.out.println(email);
					
					session.setAttribute("email",email);
//					request.setAttribute("email",email);
					
				    response.sendRedirect("customer.jsp");
					//request.getRequestDispatcher("welcome.jsp").forward(request, response);
					
				}else{
					System.out.println("error");
					request.setAttribute("msg", "Invalid Login,Please Try Again <i class='far fa-frown' style='font-size:24px;color:red'></i>");
					request.setAttribute("email",email);
					
					request.getRequestDispatcher("error.jsp").forward(request, response);
					
				}
			}
			
			if(action.equals("login_form_manager")){
				
				System.out.println("Inside manager");
				String email=request.getParameter("email");
				String password=request.getParameter("password");
				String managerName="";
				
				
				boolean status=false;
				try {
				status = account.checkManagerLogin(email, password);
				} catch (ClassNotFoundException |SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(status);
				if(status==true){
			            //get the old session and invalidate
			            HttpSession oldSession = request.getSession(false);
			            if (oldSession != null) {
			                oldSession.invalidate();
			            }
					session=request.getSession(true);  
					  //setting session to expiry in 5 mins
		            session.setMaxInactiveInterval(5*60);

		            Cookie message = new Cookie("message", "Welcome");
		            response.addCookie(message);
		        	System.out.println(email);
		        	
		        	try {
						managerName = account.getManagerName(email);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					session.setAttribute("email",email);
					session.setAttribute("managername",managerName);
//					request.setAttribute("email",email);
					session.setAttribute("selectedTab","1");
				    response.sendRedirect("manager.jsp");
					//request.getRequestDispatcher("welcome.jsp").forward(request, response);
					
				}else{
					System.out.println("error");
					request.setAttribute("msg", "Invalid Login,Please Try Again <i class='far fa-frown' style='font-size:24px;color:red'></i>");
					request.setAttribute("email",email);
					
					request.getRequestDispatcher("error.jsp").forward(request, response);
					
				}
			}
			
				
			
		}
}
