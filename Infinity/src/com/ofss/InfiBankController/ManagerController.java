package com.ofss.InfiBankController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.InfiBankBeans.InfiBankCustomer;
import com.ofss.InfiBankBeans.InfiLogs;
import com.ofss.InfiBankDao.DBAccount;

/**
 * Servlet implementation class ManagerController
 */
@WebServlet("/ManagerController")
public class ManagerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	DataSource ds;
	HttpSession session;
	Connection con;
	ArrayList<InfiLogs> logs;
	ArrayList<InfiBankCustomer> custList;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB1");
	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}
	
	
    public ManagerController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String action = request.getParameter("action");
		if(action== null){
		session = request.getSession();

		session.setAttribute("ds", ds);
		request.getRequestDispatcher("result.jsp").forward(request, response);
		}
		else{
		doPost(request, response);
		}
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String action = request.getParameter("action");
		
		if(action.equals("fetchlogs")){
		//String cat_id=request.getParameter("cat_id");
			System.out.println("Inside ManagerController");
			DBAccount account=new DBAccount();

		try {
		con = ds.getConnection();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

		account.setConnection(con);

		try {
		logs = account.getLogs();
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		session = request.getSession();
		request.setAttribute("logs", logs);
		
		request.setAttribute("selectedTab","1");
		request.getRequestDispatcher("manager.jsp").forward(request, response);

		}
		
		
		if(action.equals("blacklist")){
			//String cat_id=request.getParameter("cat_id");
				System.out.println("Inside Blacklisted");
				DBAccount account=new DBAccount();

			try {
			con = ds.getConnection();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			account.setConnection(con);

			try {
			custList = account.getBlackListedCustomers();
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			session = request.getSession();
			request.setAttribute("blackList", custList);
			request.setAttribute("selectedTab", "3");

			
			request.getRequestDispatcher("manager.jsp").forward(request, response);
			

			}
		
		
	}

}
