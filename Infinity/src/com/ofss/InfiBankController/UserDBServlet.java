package com.ofss.InfiBankController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.InfiBankDao.DBAccount;
import com.ofss.InfiBankBeans.InfiBankCustomer;

public class UserDBServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	DataSource ds;
	HttpSession session;
	Connection con;
	ArrayList<InfiBankCustomer> list;
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB");

	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action== null){
		session = request.getSession();

		session.setAttribute("ds", ds);
		request.getRequestDispatcher("result.jsp").forward(request, response);
		}
		else{
		doPost(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action.equals("test_db")){
		//String cat_id=request.getParameter("cat_id");
		DBAccount account=new DBAccount();

		try {
		con = ds.getConnection();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

		account.setConnection(con);

		try {
		list = account.getUsers();
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		session = request.getSession();
		session.setAttribute("list", list);

		request.getRequestDispatcher("result.jsp").forward(request, response);

		}
	}


}
