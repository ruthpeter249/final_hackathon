package com.ofss.InfiBankBeans;

public class InfiLogs {

	private String ifscNumber;
	private String infiBranchName;
	private String infiFirstname;
	private String infiLogTimestamp;
	private String infiLogAttempt;
	public String getIfscNumber() {
		return ifscNumber;
	}
	public void setIfscNumber(String ifscNumber) {
		this.ifscNumber = ifscNumber;
	}
	public String getInfiBranchName() {
		return infiBranchName;
	}
	public void setInfiBranchName(String infiBranchName) {
		this.infiBranchName = infiBranchName;
	}
	public String getInfiFirstname() {
		return infiFirstname;
	}
	public void setInfiFirstname(String infiFirstname) {
		this.infiFirstname = infiFirstname;
	}
	public String getInfiLogTimestamp() {
		return infiLogTimestamp;
	}
	public void setInfiLogTimestamp(String infiLogTimestamp) {
		this.infiLogTimestamp = infiLogTimestamp;
	}
	public String getInfiLogAttempt() {
		return infiLogAttempt;
	}
	public void setInfiLogAttempt(String infiLogAttempt) {
		this.infiLogAttempt = infiLogAttempt;
	}
	
	
	
}
