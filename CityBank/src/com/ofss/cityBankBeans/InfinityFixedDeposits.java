package com.ofss.cityBankBeans;

public class InfinityFixedDeposits {
	
	private String fdno;
	private String amount;
	private String startdate;
	private String edate;
	private String interest;
	
	private String accno;
	private String acctype;
	private String ifsc;
	private String acc_bal;
	private String acc_create_date;
	private String acc_create_time;
	private String translimit;
	private String min_bal;
	private String aadharno;
	private String cust_id;
	
	
	
	public String getFdno() {
		return fdno;
	}
	public void setFdno(String fdno) {
		this.fdno = fdno;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}

}
