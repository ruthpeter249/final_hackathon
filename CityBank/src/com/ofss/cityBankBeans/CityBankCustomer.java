package com.ofss.cityBankBeans;

import java.util.HashMap;

public class CityBankCustomer implements BankOperations{
	
	private String fname;
	private String mname;
	private String lname;
	private String caddress;
	private String paddress;
	private String phonenumber;
	private String city;
	private String email;
	private String state;
	private String password;
	private String accno;
	private String acctype;
	private String ifsc;
	private String acc_bal;
	private String acc_create_date;
	private String acc_create_time;
	private String translimit;
	private String min_bal;
	private String aadharno;
	private String cust_id;
	private String gender;
	private String status;
	private String panno;
	
	
	private String fdno;
	private String amount;
	private String startdate;
	private String edate;
	private String interest;
	 
	
	
	
	
	
	
	
	
	
	public String getFdno() {
		return fdno;
	}

	public void setFdno(String fdno) {
		this.fdno = fdno;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getGender() {
		return gender;
	}

	public String getPanno() {
		return panno;
	}

	public void setPanno(String panno) {
		this.panno = panno;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccno() {
		return accno;
	}

	public void setAccno(String accno) {
		this.accno = accno;
	}

	public String getAcctype() {
		return acctype;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getAcc_bal() {
		return acc_bal;
	}

	public void setAcc_bal(String acc_bal) {
		this.acc_bal = acc_bal;
	}

	public String getAcc_create_date() {
		return acc_create_date;
	}

	public void setAcc_create_date(String acc_create_date) {
		this.acc_create_date = acc_create_date;
	}

	public String getAcc_create_time() {
		return acc_create_time;
	}

	public void setAcc_create_time(String acc_create_time) {
		this.acc_create_time = acc_create_time;
	}

	public String getTranslimit() {
		return translimit;
	}

	public void setTranslimit(String translimit) {
		this.translimit = translimit;
	}

	public String getMin_bal() {
		return min_bal;
	}

	public void setMin_bal(String min_bal) {
		this.min_bal = min_bal;
	}

	public String getAadharno() {
		return aadharno;
	}

	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getAadharnomsg() {
		return aadharnomsg;
	}

	public void setAadharnomsg(String aadharnomsg) {
		this.aadharnomsg = aadharnomsg;
	}

	public String getPannonomsg() {
		return pannonomsg;
	}

	public void setPannonomsg(String pannonomsg) {
		this.pannonomsg = pannonomsg;
	}

	private String fnamemsg="";
	private String mnamemsg="";
	private String  lnamemsg="";
	private String caddressmsg="";
	private String paddressmsg="";
	private String phonenumbermsg="";
	private String emailmsg="";
	private String passwordmsg="";
	private String repasswordmsg="";
	private String aadharnomsg="";
	private String pannonomsg="";
	HashMap<String,String> map	=new HashMap<String,String>();;
	
	
	
	
	
	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCaddress() {
		return caddress;
	}

	public void setCaddress(String caddress) {
		this.caddress = caddress;
	}

	public String getPaddress() {
		return paddress;
	}

	public void setPaddress(String paddress) {
		this.paddress = paddress;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFnamemsg() {
		return fnamemsg;
	}

	public void setFnamemsg(String fnamemsg) {
		this.fnamemsg = fnamemsg;
	}

	public String getMnamemsg() {
		return mnamemsg;
	}

	public void setMnamemsg(String mnamemsg) {
		this.mnamemsg = mnamemsg;
	}

	public String getLnamemsg() {
		return lnamemsg;
	}

	public void setLnamemsg(String lnamemsg) {
		this.lnamemsg = lnamemsg;
	}

	public String getCaddressmsg() {
		return caddressmsg;
	}

	public void setCaddressmsg(String caddressmsg) {
		this.caddressmsg = caddressmsg;
	}

	public String getPaddressmsg() {
		return paddressmsg;
	}

	public void setPaddressmsg(String paddressmsg) {
		this.paddressmsg = paddressmsg;
	}

	public String getPhonenumbermsg() {
		return phonenumbermsg;
	}

	public void setPhonenumbermsg(String phonenumbermsg) {
		this.phonenumbermsg = phonenumbermsg;
	}

	public String getEmailmsg() {
		return emailmsg;
	}

	public void setEmailmsg(String emailmsg) {
		this.emailmsg = emailmsg;
	}

	public String getPasswordmsg() {
		return passwordmsg;
	}

	public void setPasswordmsg(String passwordmsg) {
		this.passwordmsg = passwordmsg;
	}

	public String getRepasswordmsg() {
		return repasswordmsg;
	}

	public void setRepasswordmsg(String repasswordmsg) {
		this.repasswordmsg = repasswordmsg;
	}

	private void populate() {
	
		map.put("admin@oracle.com", "1234");
		map.put("rucha@gmail.com", "coffeebydibella");
		map.put("gauri@gmail.com", "starbucks");
		map.put("bhanu@carwale.com", "vadapav");
		map.put("edwin@browserstack.com", "kitkat");
		map.put("shweta@gmail.com", "thej");
	
	}
	
public boolean validate(String password, String repassword) {
		
	
	
	if(password.equals(""))
		passwordmsg="Please Enter password";
	  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
	 if(!(password.matches(pattern)))
		passwordmsg="a digit must occur at least once\r <br/>" + 
				"a lower case letter must occur at least once\r <br/>" + 
				"an upper case letter must occur at least once\r <br/>" + 
				" a special character must occur at least once\r <br/>" + 
				" no whitespace allowed in the entire string\r <br/>" + 
				"at least 8 characters";
	 
	 
	if(repassword.equals(""))
		repasswordmsg="Please Enter repassword";
	
	else if(!(password.equals(repassword)))
		repasswordmsg="Repassword should match password";
	if(passwordmsg.equals("")&& repasswordmsg.equals("")) {
		return true;
	}
		return false;
	}
	public boolean validateUser(String fname,String  mname, String  lname, String  caddress, String paddress,String  phonenumber,String  email,String  password,String  repassword) {
		
		if(fname.equals("")){
			fnamemsg="Please Enter First name";
			System.out.println(fname);
		}
		if(mname.equals("")){
			mnamemsg="Please Enter middle name";
			System.out.println(mname);
		}
		if(lname.equals("")){
			lnamemsg="Please Enter last name";
		}
		if(caddress.equals("")){
			caddressmsg="Address Cannot be EMpty ";
		}
		if(paddress.equals("")){
			paddressmsg="Address Cannot be EMpty ";
		}
		if(phonenumber.equals("")){
			phonenumbermsg="Please Enter Phone Number";
		}
		else if(!(phonenumber.length()<=10 || phonenumber.length()>=11)){
			phonenumbermsg="Contact should be of 10/11 digits";
		}
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password.equals(""))
			passwordmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(password.matches(pattern)))
			passwordmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 
		if(repassword.equals(""))
			repasswordmsg="Please Enter repassword";
		
		else if(!(password.equals(repassword)))
			repasswordmsg="Repassword should match password";
		
		if(fnamemsg.equals("") && mnamemsg.equals("") && lnamemsg.equals("") && caddressmsg.equals("")&& paddressmsg.equals("")&& phonenumbermsg.equals("")&& phonenumbermsg.equals("")
				&& emailmsg.equals("")&& passwordmsg.equals("")&& repasswordmsg.equals(""))
		{ System.out.println("fnamemsg"+fnamemsg+"\tmnamemsg"+mnamemsg);
		
			return true;
		}
		
		return false;
	}
	public HashMap<String, String> getMap() {
		return map;
	}
	public void setMap(HashMap<String, String> map) {
		this.map = map;
	}
	public void populate1(String email, String password) {
		// TODO Auto-generated method stub
		map.put(email, password);
			
	}

	public boolean validateSignUpForm(String fname, String lname, String email, String password, String repassword) {
		if(fname.equals("")){
			fnamemsg="Please Enter First name";
			System.out.println(fname);
		}
		if(lname.equals("")){
			lnamemsg="Please Enter last name";
		}
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password.equals(""))
			passwordmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(password.matches(pattern)))
			passwordmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 
		if(repassword.equals(""))
			repasswordmsg="Please Enter repassword";
		
		else if(!(password.equals(repassword)))
			repasswordmsg="Repassword should match password";
		if(fnamemsg.equals("") && lnamemsg.equals("") && emailmsg.equals("")&& passwordmsg.equals("")&& repasswordmsg.equals(""))
		{ System.out.println("fnamemsg"+fnamemsg+"\tmnamemsg"+emailmsg);
		
			return true;
		}
		
		return false;
	}

	public boolean validateBankCreateForm(String fname2, String mname2, String lname2, String gender, String birthdate,
			String status, String caddress2, String paddress2, String phonenumber2, String aadharno, String panno,
			String passno, String email2, String password2, String repassword) {
		
		if(fname2.equals("")){
			fnamemsg="Please Enter First name";
			System.out.println(fname);
		}
		if(mname2.equals("")){
			mnamemsg="Please Enter middle name";
			System.out.println(mname);
		}
		if(lname2.equals("")){
			lnamemsg="Please Enter last name";
		}
		if(caddress2.equals("")){
			caddressmsg="Address Cannot be EMpty ";
		}
		if(paddress2.equals("")){
			paddressmsg="Address Cannot be EMpty ";
		}
		if(phonenumber2.equals("")){
			phonenumbermsg="Please Enter Phone Number";
		}
		else if(!(phonenumber.length()<=10 || phonenumber.length()>=11)){
			phonenumbermsg="Contact should be of 10/11 digits";
		}
	
		if(aadharno.equals("")){
			aadharnomsg="Please Enter Aadhar no";
		}
		if(panno.equals("")){
			pannonomsg="Please Enter Pan no";
		}
		
		if(passno.equals("")){
			pannonomsg="Please Enter Pan no";
		}
		if(email2.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password2.equals(""))
			passwordmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(password.matches(pattern)))
			passwordmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 
		if(repassword.equals(""))
			repasswordmsg="Please Enter repassword";
		
		else if(!(password2.equals(repassword)))
			repasswordmsg="Repassword should match password";
		
		if(fnamemsg.equals("") && mnamemsg.equals("") && lnamemsg.equals("") && caddressmsg.equals("")&& paddressmsg.equals("")&& phonenumbermsg.equals("")&& phonenumbermsg.equals("")
				&& emailmsg.equals("")&& passwordmsg.equals("")&& repasswordmsg.equals("")&&  aadharnomsg.equals("")&&  pannonomsg.equals(""))
		{ System.out.println("fnamemsg"+fnamemsg+"\tmnamemsg"+mnamemsg);
		
			return true;
		}
		return false;
	}

	public boolean validateLogin(String email2, String password2) {
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password.equals(""))
			passwordmsg="Please Enter password";
		if(emailmsg.equals("")&& passwordmsg.equals("")) {
			return true;
		}
		
		return false;
	}

	@Override
	public void withdraw(String amt) {
//		if(amt>acc_bal) {
//			transmsg="Sorry ,your request amount is more than balance in your amount,Please Enter Other Amount";
//		}
//		acc_bal=acc_bal-amt;
//		return acc_bal;
		
	}

	@Override
	public void deposit(String amt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showDetails() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString() {
		return "CityBankCustomer [fname=" + fname + ", mname=" + mname + ", lname=" + lname + ", caddress=" + caddress
				+ ", paddress=" + paddress + ", phonenumber=" + phonenumber + ", city=" + city + ", email=" + email
				+ ", state=" + state + ", password=" + password + ", accno=" + accno + ", acctype=" + acctype
				+ ", ifsc=" + ifsc + ", acc_bal=" + acc_bal + ", acc_create_date=" + acc_create_date
				+ ", acc_create_time=" + acc_create_time + ", translimit=" + translimit + ", min_bal=" + min_bal
				+ ", aadharno=" + aadharno + ", cust_id=" + cust_id + ", gender=" + gender + ", status=" + status
				+ ", panno=" + panno + ", fdno=" + fdno + ", amount=" + amount + ", startdate=" + startdate + ", edate="
				+ edate + ", interest=" + interest + ", fnamemsg=" + fnamemsg + ", mnamemsg=" + mnamemsg + ", lnamemsg="
				+ lnamemsg + ", caddressmsg=" + caddressmsg + ", paddressmsg=" + paddressmsg + ", phonenumbermsg="
				+ phonenumbermsg + ", emailmsg=" + emailmsg + ", passwordmsg=" + passwordmsg + ", repasswordmsg="
				+ repasswordmsg + ", aadharnomsg=" + aadharnomsg + ", pannonomsg=" + pannonomsg + ", map=" + map + "]";
	}
	
}
