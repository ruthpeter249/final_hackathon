package com.ofss.cityBankBeans;

public interface BankOperations {

	void withdraw(String amt);
	void deposit(String amt);
	void showDetails();
	
	
}
