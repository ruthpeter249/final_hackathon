package com.ofss.cityBankBeans;

public class HdccFixedDeposits {
	
	private String fdno;
	private String amount;
	private String startdate;
	private String edate;
	private String interest;
	
	private String accno;
	private String acctype;
	private String ifsc;
	private String acc_bal;
	private String acc_create_date;
	private String acc_create_time;
	private String translimit;
	private String min_bal;
	private String aadharno;
	private String cust_id;
	
	
	public String getAccno() {
		return accno;
	}
	public void setAccno(String accno) {
		this.accno = accno;
	}
	public String getAcctype() {
		return acctype;
	}
	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getAcc_bal() {
		return acc_bal;
	}
	public void setAcc_bal(String acc_bal) {
		this.acc_bal = acc_bal;
	}
	public String getAcc_create_date() {
		return acc_create_date;
	}
	public void setAcc_create_date(String acc_create_date) {
		this.acc_create_date = acc_create_date;
	}
	public String getAcc_create_time() {
		return acc_create_time;
	}
	public void setAcc_create_time(String acc_create_time) {
		this.acc_create_time = acc_create_time;
	}
	public String getTranslimit() {
		return translimit;
	}
	public void setTranslimit(String translimit) {
		this.translimit = translimit;
	}
	public String getMin_bal() {
		return min_bal;
	}
	public void setMin_bal(String min_bal) {
		this.min_bal = min_bal;
	}
	public String getAadharno() {
		return aadharno;
	}
	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getFdno() {
		return fdno;
	}
	public void setFdno(String fdno) {
		this.fdno = fdno;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}
	

}
