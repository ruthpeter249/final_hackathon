package com.ofss.cityBankController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.cityBankBeans.CityBankCustomer;
import com.ofss.cityBankBeans.HdccFixedDeposits;
import com.ofss.cityBankDAO.DBAccount;
import com.ofss.resources.RestFetchAccountHdcc;
import com.ofss.resources.RestFetchAccountInfinity;
import com.ofss.resources.RestHdccAccTransactions;
import com.ofss.resources.RestJavaNetClient;
import com.ofss.resources.RestJavaNetInfinityClient;




@WebServlet("/RestServiceController")
public class RestServiceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 Map<String,String> resultMap;
	 Map<String,String> accountmaphdcc;
	 Map<String,String> accountmapinfinity;
	ArrayList<CityBankCustomer> citylist;
	ArrayList<CityBankCustomer> citylist1 = new ArrayList<CityBankCustomer>();
	ArrayList<HdccFixedDeposits> hdlist;
	
	
    HttpSession session;
    DataSource ds;
	Connection con;
	ArrayList<CityBankCustomer> list;
	ArrayList<HdccFixedDeposits> hdccfdlist;
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB");

	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if(action.equals("traditional_way")) {
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//doGet(request, response);
		DBAccount account=new DBAccount();
	
		String action=request.getParameter("action");
		CityBankCustomer customer=new CityBankCustomer();
		session=request.getSession();
		try {
			con = ds.getConnection();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			account.setConnection(con);
		
		
		if(action.equals("kyc_verify_option")) {
			request.getRequestDispatcher("kycoption.jsp").forward(request, response);
		}
		if(action.equals("traditional_way")) {
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
		if(action.equals("kyc")) {

			
				String aadharno=request.getParameter("aadharno");
				String bankOption=request.getParameter("bankOption");
				
				if(bankOption.equals("-------------------------------------------------------------------------------")&& aadharno.equals("")) {
					request.setAttribute("msg", "Please Enter Aadhar no");
					request.setAttribute("msg2", "Please Select a bank");
					request.getRequestDispatcher("kycoption.jsp").forward(request, response);
				}
				System.out.println(bankOption);
				if(bankOption.equals("HDCC Bank")) {
					 RestJavaNetClient restobj=new RestJavaNetClient();
					resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		        	if(resultMap==null) {
		        		request.setAttribute("msgerr", "Some Problem occured in the system,Please ReEnter Aadhar no ");
		        		 request.getRequestDispatcher("kycoption.jsp").forward(request, response);
						
		        	}
		    		System.out.println("Result Map valyues is "+resultMap);
		    		
		    		/*resultMap.entrySet()
		    		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));*/
		    		
		    		//System.out.println("FNAME="+resultMap.get("fname"));
		    		
		    		
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}
				if(bankOption.equals("Infinty Bank")) {
					 RestJavaNetInfinityClient restobj=new RestJavaNetInfinityClient();
			 resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		            if(resultMap==null) {
		        		request.setAttribute("msgerr", "Some Problem occured in the system,Please ReEnter Aadhar no ");
		        		 request.getRequestDispatcher("kycoption.jsp").forward(request, response);
						
		        	}
		            resultMap.entrySet()
		    		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));
		    		
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}
            
	}
		if(action.equals("sign_up_form_proceedtopassword")) {
			request.setAttribute("passwordmsg", "");
			request.setAttribute("repasswordmsg", "");
			request.getRequestDispatcher("kyc_password.jsp").forward(request, response);
			
		}
		if(action.equals("sign_up_form"))
	{
		
		System.out.println("inside signup form ");
		String fname=resultMap.get("fname");
		String mname=resultMap.get("mname");
		String lname=resultMap.get("lname");
		String gender=resultMap.get("gender");
		String birthdate=resultMap.get("bdate");
		String status=resultMap.get("status");
		String caddress=resultMap.get("cadd");
		String paddress=resultMap.get("padd");
		String city=resultMap.get("city");
		String state=resultMap.get("state");
		String phonenumber=resultMap.get("phnno");
		String aadharno=resultMap.get("aadharno");
		String panno=resultMap.get("panno");
		String passno=resultMap.get("passno");
		String email=resultMap.get("emailId");
		String password=request.getParameter("password");
		String repassword=request.getParameter("repassword");
		String pincode=resultMap.get("pincode");
		System.out.println("ResultMap Values \n="+resultMap);
		resultMap.entrySet()
		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));
		System.out.println();
	

	
	
		boolean status1=customer.validate(password,repassword);
		if(status1==true){
			System.out.println("Inside status true ");
			String custid1=resultMap.get("custID");
			System.out.println("custid1="+custid1);
			String custid=custid1.substring(0, 2);
			
			System.out.println("custid="+custid);
		//request.setAttribute("email", email);	
			//request.setAttribute("msg", "Registration Successful,Please login again");
			
			if(custid.equals("HD")) {
			 RestFetchAccountHdcc restobjhdcc=new RestFetchAccountHdcc();
			accountmaphdcc=restobjhdcc.fetchHdccAccountDetails(aadharno);
			request.setAttribute("accmaphdcc", accountmaphdcc);
			System.out.println("Acc detials city:"+resultMap);
			System.out.println("Acc details:"+accountmaphdcc);
			request.setAttribute("selectbankview", "infinity");
			
			}if(custid.equals("IN")) {
			 RestFetchAccountInfinity restobjinfi=new RestFetchAccountInfinity();
			 accountmapinfinity=restobjinfi.fetchHdccAccountDetails(aadharno);
			request.setAttribute("accmapinfinity", accountmapinfinity);
			System.out.println("Acc detials city:"+resultMap);
			System.out.println("Acc detials infinity:"+accountmapinfinity);
			request.setAttribute("selectbankview", "hdcc");
			}
			
			
			System.out.println("Going in db");
			
			
			boolean dbstatus=false;
			
//			 dbstatus=account.insertBankUserDetails(fname,mname,lname,gender,birthdate,status,caddress,paddress,phonenumber,aadharno,panno,passno,email,password,repassword,city,state,pincode);
			  CityBankCustomer customer1 = new CityBankCustomer();
			  customer1.setFname(fname);
			  customer1.setMname("");
			  customer1.setLname("");
			  customer1.setAccno("CT");
			  customer1.setIfsc("CITY00000001");
			  customer1.setAcctype("SAVINGS");
			  customer1.setGender(gender);
			  customer1.setStatus(status);
			  customer1.setPaddress(paddress);
			  customer1.setCaddress(caddress);
			  customer1.setPhonenumber(phonenumber);
			  customer1.setAadharno(aadharno);
			  customer1.setPanno(panno);
			  customer1.setEmail(email);
			  customer1.setPassword(password);
			  customer1.setCity(city);
			  customer1.setState(state);	
			  System.out.println(customer1.toString());
			 	citylist1.add(customer1);
//			 	request.getRequestDispatcher("test.jsp").forward(request, response);
			 
				
			
//				account.createAccount(aadharno);
//				citylist=account.fetchAccDetailsCity(aadharno);

		
			System.out.println("create acc");
			//dbstatus=account.updateCustid(aadharno);
			 System.out.println("CityAcc detials \n"+citylist1);
			 request.setAttribute("citylist", citylist1);
             request.getRequestDispatcher("test.jsp").forward(request, response);

			//account.insertCustomer( fname,  lname,  email,  password);
	
		
			
		}else {
			
			request.setAttribute("passwordmsg", customer.getPasswordmsg());
			request.setAttribute("repasswordmsg",customer.getRepasswordmsg());
			request.setAttribute("msg", "Something Wrong with password or repassword ,Please Enter Again");	
			request.getRequestDispatcher("kyc_password.jsp").forward(request, response);
		}

}
		if(action.equals("infinity")) {
			 String custid1=resultMap.get("custID");
			 RestFetchAccountInfinity restobjinfi=new RestFetchAccountInfinity();
			 accountmapinfinity=restobjinfi.fetchHdccAccountDetails(resultMap.get("aadharno"));
			 request.setAttribute("accmaphdcc", accountmaphdcc);
			 request.setAttribute("accmapinfinity", accountmapinfinity);
			 request.setAttribute("selectbankview", "both");
			 
//			 System.out.println("aadhar no"+accountmaphdcc.get("aadharno"));
				
			 
			 request.setAttribute("cityfd",list);
			 request.setAttribute("hdccfixeddeposits", hdccfdlist);
			
				 
			 try {
				list = account.getCityFixedDeposits(custid1);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			 
			 try {
				hdccfdlist = account.getHdccFixedDeposits(custid1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				try {
					System.out.println(accountmapinfinity.get("aadharno"));
					citylist=account.fetchAccDetailsCity(accountmapinfinity.get("aadharno"));
				} catch (SQLException e) {
					e.printStackTrace();
				}
					// TODO Auto-generated catch block
				 System.out.println("CityAcc detials lolololol \n"+citylist1);
//				 request.setAttribute("citylist", citylist);
				 request.setAttribute("citylist", citylist1);
			
			
			System.out.println("Acc details infinity after password when user selected this bank:"+accountmapinfinity);
			
			request.getRequestDispatcher("aggregated_view.jsp").forward(request, response);
			
		}
		if(action.equals("hdcc")) {
			
			 String custid1=resultMap.get("custID");

			RestFetchAccountHdcc restobjhdcc=new RestFetchAccountHdcc();
			accountmaphdcc=restobjhdcc.fetchHdccAccountDetails(resultMap.get("aadharno"));
			request.setAttribute("accmaphdcc", accountmaphdcc);
			System.out.println("mappp"+resultMap);
			System.out.println("aadhar no"+accountmaphdcc.get("aadharno"));
			request.setAttribute("accmapinfinity", accountmapinfinity);
			
			request.setAttribute("selectbankview", "both");
			request.setAttribute("cityfd",list);
			
			 
			 try {
				list = account.getCityFixedDeposits(custid1);
				hdccfdlist = account.getHdccFixedDeposits(custid1);

			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
				try {
					citylist=account.fetchAccDetailsCity(accountmaphdcc.get("aadharno"));
				} catch (SQLException e) {
					e.printStackTrace();
				}
					// TODO Auto-generated catch block
				 System.out.println("CityAcc detials \n"+citylist);
				 request.setAttribute("citylist", citylist1);
				
			
			request.setAttribute("hdccfixeddeposits", hdccfdlist);
			System.out.println("Acc details infinity after password when user selected this bank:"+accountmaphdcc);
			request.getRequestDispatcher("aggregated_view.jsp").forward(request, response);
		
		}
		
		//hdcc only
		if(action.equals("nofusion1")) {
//			try {
//			//	citylist=account.fetchAccDetailsCity(accountmaphdcc.get("aadharno"));
//				
//				
//				citylist=account.fetchAccDetailsCity(accountmapinfinity.get("aadharno"));
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
				// TODO Auto-generated catch block
			 System.out.println("CityAcc detials \n"+citylist);
			 request.setAttribute("citylist", citylist1);
			
			request.setAttribute("accmaphdcc", accountmaphdcc);
			request.setAttribute("selectbankview", "hdcc");

			System.out.println(citylist1.get(14));
			System.out.println(citylist1.get(16));
			System.out.println(citylist1.get(15));
			request.getRequestDispatcher("aggregated_view.jsp").forward(request, response);
			
		}
		
		//infinity only
		if(action.equals("nofusion2")) {
//			try {
////				citylist=account.fetchAccDetailsCity(accountmaphdcc.get("aadharno"));
//			//citylist=account.fetchAccDetailsCity(accountmapinfinity.get("aadharno"));
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			request.setAttribute("accmapinfinity", accountmapinfinity);
			request.setAttribute("selectbankview", "infinity");
			System.out.println(citylist1.get(0));

			
			request.setAttribute("citylist", citylist1);
			request.getRequestDispatcher("aggregated_view.jsp").forward(request, response);
			
		}
		
		if(action.equals("transactions")) {
			System.out.println("Inside transactions");
			request.getRequestDispatcher("transaction.jsp").forward(request, response);
		}
		
				if(action.equals("deposit")) {
					String aadharno=request.getParameter("aadharno");
					String amt=request.getParameter("amt");
					String bankname=request.getParameter("bankname");
					RestHdccAccTransactions transobj=new RestHdccAccTransactions();
					
					String opstatus=transobj.operate(aadharno,amt,"deposit");
					request.setAttribute("msg", opstatus);
					String aadhar="110022001120";
					try {
						hdlist= account.fetchHdccDetails(aadhar);
						System.out.println("hdlist"+ hdlist.get(0));
						request.setAttribute("aadhar", hdlist);


					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					request.getRequestDispatcher("transaction.jsp").forward(request, response);
					
				}if(action.equals("withdraw")) {
					String aadharno=request.getParameter("aadharno");
					String amt=request.getParameter("amt");
					String bankname=request.getParameter("bankname");
					RestHdccAccTransactions transobj=new RestHdccAccTransactions();
					
					String opstatus=transobj.operate(aadharno,amt,"withdraw");
					request.setAttribute("msg", opstatus);
					request.getRequestDispatcher("transaction.jsp").forward(request, response);
					
				}
	}
	
}
