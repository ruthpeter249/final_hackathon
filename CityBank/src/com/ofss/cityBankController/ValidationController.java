package com.ofss.cityBankController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.cityBankBeans.CityBankCustomer;
import com.ofss.cityBankBeans.HdccFixedDeposits;
import com.ofss.cityBankDAO.DBAccount;


@WebServlet("/ValidationController")
//@WebServlet("{.do}")
public class ValidationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   HttpSession session;
   DataSource ds;
	Connection con;
	ArrayList<CityBankCustomer> list;
	DBAccount account;
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB");

	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CityBankCustomer customer=new CityBankCustomer();
		String action = request.getParameter("action");
		if(action== null){
		session = request.getSession();

		session.setAttribute("ds", ds);
		//request.getRequestDispatcher("result.jsp").forward(request, response);
		}
		else{
		doPost(request, response);
		}
	
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		DBAccount account=new DBAccount();
		String action=request.getParameter("action");
		CityBankCustomer customer=new CityBankCustomer();
		session=request.getSession();
		try {
			con = ds.getConnection();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			account.setConnection(con);
		
		if(action.equals("sign_up_form"))
		{
		
						System.out.println("inside signup form ");
						String fname=request.getParameter("fname");
						String mname=request.getParameter("mname");
						String lname=request.getParameter("lname");
						String gender=request.getParameter("gender");
						String birthdate=request.getParameter("birthdate");
						String status=request.getParameter("status");
						String caddress=request.getParameter("caddress");
						String paddress=request.getParameter("paddress");
						String phonenumber=request.getParameter("phonenumber");
						String aadharno=request.getParameter("aadharno");
						String panno=request.getParameter("panno");
						String passno=request.getParameter("passno");
						String email=request.getParameter("email");
						String password=request.getParameter("password");
						String repassword=request.getParameter("repassword");
						
						System.out.println("fname="+fname);
						System.out.println("mname"+mname);
						System.out.println("lname"+lname);
						System.out.println("gender"+birthdate);
						
						/*boolean status1=customer.validateBankCreateForm(fname,mname,lname,gender,birthdate,status,caddress,paddress,phonenumber,aadharno,panno,passno,email,password,repassword);
						if(status1==true){
							System.out.println("Inside status true ");
							request.setAttribute("email", email);	
							request.setAttribute("msg", "Registration Successful,Please login again");
							request.getRequestDispatcher("login.jsp").forward(request, response);
							  */
						
							System.out.println("Going in db");
						
							
							
						
							
							
							
							request.getRequestDispatcher("test.jsp").forward(request, response);
							
						}

						
		

		if(action.equals("bank_acc_create"))
		{
		request.getRequestDispatcher("create_acc.jsp").forward(request, response);			
							
		}	
						
					
		if(action.equals("log_out_form")) { 
            System.out.println("logout section");
            //invalidate the session if exists
            HttpSession session = request.getSession(false);
            if(session != null){
                session.invalidate();
            	
            }
            //System.out.println("session id"+session.getId());
            response.sendRedirect(request.getContextPath() + "/index.jsp");
        	//request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		if(action.equals("login_form")){
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			//CityBankCustomer cust=new CityBankCustomer();
			boolean statuslogin=true;
			//statuslogin=customer.validateLogin(email,password);
			
			
			boolean status=false;
			try {
				status = account.checkLogin(email, password);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(status==true){
		            //get the old session and invalidate
		            HttpSession oldSession = request.getSession(false);
		            if (oldSession != null) {
		                oldSession.invalidate();
		            }
				session=request.getSession(true);  
				  //setting session to expiry in 5 mins
	            session.setMaxInactiveInterval(5*60);

	          
				session.setAttribute("email",email);
			    //response.sendRedirect("welcome.jsp");
				request.getRequestDispatcher("transaction.jsp").forward(request, response);
				
			}else{
				request.setAttribute("msg", "Invalid Login,Please Try Again <i class='far fa-frown' style='font-size:24px;color:red'></i>");
				request.setAttribute("email",email);
				request.setAttribute("emailmsg",customer.getEmailmsg());
				request.setAttribute("passwordmsg",customer.getPassword());
				
				
			}
		}
		
			
		
	}

}

