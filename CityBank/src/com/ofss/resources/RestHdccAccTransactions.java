package com.ofss.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
@Path("HdccWithDraw")
public class RestHdccAccTransactions {
@GET
@Produces(MediaType.APPLICATION_JSON)

   public String operate(String aadharno1, String amt2, String op){
 
	String op_status="";
       String aadharno="110022001190";
       String amt="500";
       
       String url1 = "http://10.180.40.134:8022/HDCC_Bank_Api/webapi/FetchAccountDetails";
       StringBuilder str=new StringBuilder(url1);
str.append("/");
str.append(aadharno1);
str.append("/");
if(op.equals("deposit")) {
	str.append("deposit/");
}else {
str.append("withdraw/");
}
str.append(amt);
String url =str.toString();
      System.out.println(url);
       HttpURLConnection urlConn = null;
       BufferedReader reader = null;
       OutputStream ouputStream = null;
       try {
           URL urlObj = new URL(url);
           urlConn = (HttpURLConnection) urlObj.openConnection();
           urlConn.setDoOutput(true);
           urlConn.setRequestMethod("PUT");
           urlConn.setRequestProperty("Content-Type", "application/json");
           urlConn.setConnectTimeout(5000);
           urlConn.setReadTimeout(5000);
           urlConn.setRequestProperty("Accept", "application/json");
           // send json input request
           ouputStream = urlConn.getOutputStream();
           ouputStream.flush();
           if (urlConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
               System.err.println("Unable to connect to the URL...");
               return "Something went wrong the System,,Please Try Again";
           }
           System.out.println("Connected to the server...");
           InputStream is = urlConn.getInputStream();
           reader = new BufferedReader(new InputStreamReader((is)));
           String tmpStr = null;
           while((tmpStr = reader.readLine()) != null){
               System.out.println(tmpStr);
           }
       if(tmpStr.equals("1")) {
    	   op_status= "Transaction was Successful!";
       }else {
    	   op_status= "Transaction was UnSuccessful!"; 
       }
       } catch (MalformedURLException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       } catch (IOException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       } finally {
           try {
               if(reader != null) reader.close();
               if(urlConn != null) urlConn.disconnect();
               return "Transaction was Successful!"; 
           } catch(Exception ex){
               
           }
       }
	return op_status;
  
}
}

