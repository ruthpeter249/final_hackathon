package com.ofss.cityBankDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ofss.cityBankBeans.CityBankCustomer;
import com.ofss.cityBankBeans.HdccFixedDeposits;
public class DBAccount {
	Connection con;
	PreparedStatement pstmt;
	ArrayList<CityBankCustomer> list;
	ArrayList<HdccFixedDeposits> fdlist;
	public void setConnection(Connection con) {
	this.con = con;
	}
	public ArrayList<CityBankCustomer> getUsers() throws SQLException {
			list = new ArrayList<CityBankCustomer>();
			 String sql = "select CITY_CUST_FIRSTNAME, CITY_CUST_EMAILID from CITY_CUSTOMER";
			PreparedStatement pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, cat_id);
			ResultSet rst =pstmt.executeQuery();
			while(rst.next()){
			String namedb=rst.getString("CITY_CUST_FIRSTNAME");
			String emaildb=rst.getString("CITY_CUST_EMAILID");
			System.out.println("name:"+namedb);
			System.out.println("email:"+emaildb);
			CityBankCustomer c = new CityBankCustomer();
			c.setFname(namedb);
			c.setEmail(emaildb);
			list.add(c);
			c=null;
			}
			return list;
		}
	public void  insertCustomer(String fname, String lname, String email, String password) throws SQLException {
		System.out.println("fname in meth "+con); 
		String sql = "insert into CITY_CUSTOMER(CITY_CUSTID,CITY_CUST_FIRSTNAME,CITY_CUST_LASTNAME,CITY_CUST_EMAILID,CITY_CUST_PASSWORD)values ('CT'||CUSTOMER_CUSTID.NEXTVAL,?,?,?,?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, fname);
			
			pstmt.setString(2, lname);
			pstmt.setString(3, email);
			pstmt.setString(4, password);
			pstmt.executeUpdate();//executeUpdate for insert
		System.out.println("just inserted db");
	}
	
	
	public boolean checkLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("fname in mesdgf "+con); 
		System.out.println("inside login code ");
		String sql="select * from CITY_CUSTOMER";//put the first statement of sql query as string
		//sql="insert into CITY_CUSTOMER(CITY_CUSTID,CITY_CUST_FIRSTNAME,CITY_CUST_LASTNAME,CITY_CUST_EMAILID,CITY_CUST_PASSWORD)values ('CT'||CUSTOMER_CUSTID.NEXTVAL,'Test','testing','test@test.com','Oracle@1234')";
		pstmt=con.prepareStatement(sql);//convert into sql query
		ResultSet result=pstmt.executeQuery();
		list=new ArrayList<CityBankCustomer>();
		while(result.next()){
			String uemail=result.getString("CITY_CUST_EMAILID");
			String upassword=result.getString("CITY_CUST_PASSWORD");
			CityBankCustomer c=new CityBankCustomer();
		c.setEmail(uemail);
		c.setPassword(upassword);
			list.add(c);
		}
		Iterator<CityBankCustomer> itr=list.iterator();
	while(itr.hasNext()){
		CityBankCustomer c=(CityBankCustomer) itr.next();
		if(c.getEmail().equals(email) && c.getPassword().equals(password)){
			return true;
		}
	
	}return false;
	}
	public boolean insertBankUserDetails(String fname, String mname, String lname, String gender, String birthdate,
			String status, String caddress, String paddress, String phonenumber, String aadharno, String panno,
			String passno, String email, String password, String repassword,String city ,String state,String pincode)  {
		System.out.println("Inside insertBankUserDetails");

		String sql = "insert into CITY_CUSTOMER(CITY_CUSTID,CITY_CUST_FIRSTNAME,CITY_CUST_MIDDLENAME ,CITY_GENDER ,CITY_MARITAL_STATUS ,CITY_CUST_PERMANENTADDRESS ,CITY_CUST_CURRENTADDRESS ,CITY_PHONE ,CITY_CUST_AADHARNO ,CITY_CUST_PANNO ,CITY_CUST_PPNO ,CITY_CUST_LASTNAME,CITY_CUST_EMAILID,CITY_CUST_PASSWORD,CITY_CUST_CCITY,CITY_CUST_CSTATE ,CITY_CUST_CPINCODE,CITY_CUST_BIRTHDATE)values ('CT'||CUSTOMER_CUSTID.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);

			pstmt.setString(1, fname);
			pstmt.setString(2, mname);
			pstmt.setString(3, gender);
			
			pstmt.setString(4, status);
			pstmt.setString(5, paddress);
			pstmt.setString(6, caddress);
			pstmt.setString(7, phonenumber);
			pstmt.setString(8, aadharno);
			pstmt.setString(9, panno);
			pstmt.setString(10, passno);
			pstmt.setString(11, lname);
			pstmt.setString(12, email);
			pstmt.setString(13, password);
			pstmt.setString(14, city);
			pstmt.setString(15, state);
			pstmt.setString(16, pincode);
			pstmt.setString(17, birthdate);
			pstmt.executeUpdate();//executeUpdate for insert
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	System.out.println("just inserted city db");
	return true;
	}
	
	public ArrayList<CityBankCustomer> fetchAccDetailsCity(String aadharno) throws SQLException {
		String sql="select * from CITY_ACCOUNT WHERE CITY_ACC_AADHARNO =?";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		pstmt.setString(1, aadharno);
		
		ResultSet result=pstmt.executeQuery();
		list=new ArrayList<CityBankCustomer>();
		while(result.next()){
			CityBankCustomer c=new CityBankCustomer();
			c.setAccno(result.getString("CITY_ACCNO "));
			c.setAcctype(result.getString("CITY_ACCTYPE "));
			c.setIfsc(result.getString("CITY_IFSC"));
			c.setAcc_bal(result.getString("CITY_ACCOUNTBALANCE "));
			c.setAccno(result.getString("CITY_ACCOUNTCREATEDATE "));
			c.setAcc_create_time(result.getString("CITY_ACCOUNTCREATETIME"));
			c.setTranslimit(result.getString("CITY_TRANSLIMIT"));
			c.setMin_bal(result.getString("CITY_MINBALANCE"));
			c.setCust_id(result.getString("CITY_CUSTID  "));
			list.add(c);
		}
	return list; 
	
	}
	public void createAccount(String aadharno) throws SQLException {
		String sql = "insert into CITY_ACCOUNT(CITY_ACCNO ,CITY_ACCTYPE  ,CITY_IFSCNUMBER  ,CITY_ACCOUNTBALANCE  ,CITY_ACCOUNTCREATEDATE  ,CITY_ACCOUNTCREATETIME  ,CITY_TRANSLIMIT  ,CITY_MINBALANCE ,CITY_ACC_AADHARNO ,CITY_CUSTID )values (CITY_ACC_NO.NEXTVAL,'SAVINGS','CITY00000001',0,SYSDATE,TO_CHAR(SYSDATE,'HH24:MI:SS'),100000,0,?,CT||CUSTOMER_CUSTID.CURRVAL)";
		pstmt=con.prepareStatement(sql);//convert into sql query
		pstmt.setString(1, aadharno);
		pstmt.executeUpdate();
	
	}
	public boolean updateCustid(String aadharno) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public ArrayList<HdccFixedDeposits> getHdccFixedDeposits(String custid) throws SQLException {
		fdlist = new ArrayList<HdccFixedDeposits>();
		String sql = "select * from HDCC_FD_DETAILS@CITY_HDCC_LINK where HDCC_CUSTID = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1,custid);
		//pstmt.setString(1, cat_id);
		
		ResultSet rst =pstmt.executeQuery();
		HdccFixedDeposits fd = new HdccFixedDeposits();
		
		while(rst.next()){
		fd.setFdno("HDCC_FD_NO");
		fd.setAmount("HDCC_FD_AMMOUNT");
		fd.setStartdate("HDCC_FD_SDATE");
		fd.setEdate("HDCC_FD_EDATE");
		fd.setInterest("HDCC_FD_INTEREST");
		fdlist.add(fd);
		fd=null;
		}
		return fdlist;
	}
	
	public ArrayList<CityBankCustomer> getCityFixedDeposits(String custid) throws SQLException {
		fdlist = new ArrayList<HdccFixedDeposits>();
		String sql = "select * from CITY_FD_DETAILS where CITY_CUSTID = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1,custid);
	
		
		ResultSet rst =pstmt.executeQuery();
		CityBankCustomer fd = new CityBankCustomer();
		
		while(rst.next()){
		fd.setFdno("CITY_FD_NO");
		fd.setAmount("CITY_FD_AMMOUNT");
		fd.setStartdate("CITY_FD_SDATE");
		fd.setEdate("CITY_FD_EDATE");
		fd.setInterest("CITY_FD_INTEREST");
		list.add(fd);
		fd=null;
		}
		return list;
	}
	
	
	public ArrayList<HdccFixedDeposits> fetchHdccDetails(String aadharno) throws SQLException {
		fdlist = new ArrayList<HdccFixedDeposits>();
		String sql = "select * from HDCC_ACCOUNT@CITY_HDCC_LINK where HDCC_ACC_AADHARNO = ?";
		
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1,aadharno);
	
		
		ResultSet rst =pstmt.executeQuery();
		HdccFixedDeposits fd = new HdccFixedDeposits();
		
	
		while(rst.next()){
			fd.getAccno();
			fdlist.add(fd);
			
		}
		return fdlist;
	}
	
	
	
	
}

