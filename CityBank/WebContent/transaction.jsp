<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Citybank</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta http-equiv="content-type" content="3,url=kyc_fetch_display.jsp">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/test.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700"
	rel="stylesheet">

<link rel="stylesheet" href="fonts/icomoon/style.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">

<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/stylecity.css">
<link rel="stylesheet"
	href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://code.angularjs.org/1.4.0-rc.0/angular.js"></script>
<script src="js/app.js"></script>

<style>
#overlay {
	display: none;
	background: red;
}

#overlay {
	display: block;
	background: red;
}
</style>
</head>

<body>


	<c:import url="header.jsp"></c:import>

	<section
		class="ftco-about ftco-no-pt ftco-no-pb ftco-section bg-light pt-4"
		id="about-section">

		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="pb-5">
						CityBank Welcomes You !
					</h1>
					<h2 class="text-primary mb-4">Do your bank transactions here!</h2>
				</div>

			</div>

		</div>
	</section>



	<div class="container">

		<nav class="bg-light">
			<div class="nav nav-tabs nav-fill " id="nav-tab" role="tablist">
				<a class="nav-item nav-link active" id="nav-home-tab"
					data-toggle="tab" href="#nav-home" role="tab"
					aria-controls="nav-home" aria-selected="true">Citibank
					Operations</a> <a
					class="nav-item nav-link <c:if test="${selectedTab == '2'}">active</c:if>"
					id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
					role="tab" aria-controls="nav-profile" aria-selected="false">Across
					Bank Operations </a>


			</div>
		</nav>

		<div class="tab-content py-3 px-3 px-sm-0  bg-white" id="tabs">
			<div class="tab-pane fade active show" id="nav-home" role="tabpanel"
				aria-labelledby="nav-home-tab">






				<div class="row">
					<h3>
						Your account status :
						<%
						request.getAttribute("status");
					%>
					</h3>
					<br>
					</div>



				<div ng-app>
					<div class="row">

						<div class="col-md-6">
							<input type="button" value="Deposit Amount"
								ng-click="show='deposit'" class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
							<input type="button" value="Withdraw Amount"
								ng-click="show='withdraw'" class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
							<input type="button" value="Details" ng-click="show='details'"
								class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
						</div>

						<div class="col-md-6">

							<div class="card">
								<div class="card-body" ng-show="show=='deposit'">
									<h4 class="card-title">Deposits</h4>

									<p class="card-text">
									<form action="BankTransactionController" method="POST">

										<input type="hidden" name="action" value="deposit" /> 
										<input
											type="number" name="amt" placeholder="Please enter amount">
											
										<p>Enter Your Aadhar Number:</p>
											<input type="text" name="aadharno" placeholder="enter aadhar number" maxlimit="12">

				
										<button class="btn btn-primary"
											style="text-align: center; border-radius: 6px">Deposit</button>
									</form>



								</div>

								<!-- widraws -->
								<div class="card-body" ng-show="show=='withdraw'">
									<h4 class="card-title">Withdraw</h4>

									<p class="card-text">
									<form action="RestServiceController" method="POST">

										<input type="hidden" name="action" value="deposit" /> <input
											type="number" name="amt" placeholder="Please enter amount">
											<p>Enter Your Aadhar Number:</p>
											<input type="text" name="aadharno" placeholder="enter aadhar number" maxlimit="12">

										<button class="btn btn-primary"
											style="text-align: center; border-radius: 6px">Withdraw</button>
									</form>



								</div>

								<div class="card-body" ng-show="show=='details'">
									<h4 class="card-title">Details</h4>
									<p class="card-text">
									<h6
										class="card-subtitle mb-2 text-muted justify-content-center">Your
										citibank Details</h6>
									<p class="card-text justify-content-center">
									<div class="row">
										<div class="col-md-6">
											<strong>Aadhar no:</strong><br> <strong>Account
												Create Date:</strong><br> <strong>Account Number:</strong><br>
											<strong>Account type:</strong><br> <strong>Ifsc
												Code:</strong><br> <strong>Account balance:</strong><br>


										</div>

										<!--  get citi details here-->
										<div class="col-md-6">
											<c:out value='${citylist.aadharno}' />
											<c:out value='${citylist.acccreatedate}' />
											<c:out value='${citylist.accno}' />
											<c:out value='${citylist.acctype}' />
											<c:out value='${citylist.ifsc}' />
											<c:out value='${citylist.accbalance}' />

											<br>

										</div>
									</div>
									</p>



								</div>
							</div>

						</div>


					</div>

				</div>



			</div>
			<div class="tab-pane fade show" id="nav-profile" role="tabpanel"
				aria-labelledby="nav-profile-tab">

					<div class="row">
					<h3>
						Your account status :
						<%
						request.getAttribute("status");
					%>
					</h3>
					<br>
					</div>
					

				<div ng-app>
					<div class="row">

						<div class="col-md-6">
							<input type="button" value="Deposit Amount"
								ng-click="show='deposit'" class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
							<input type="button" value="Withdraw Amount"
								ng-click="show='withdraw'" class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
							<input type="button" value="Details" ng-click="show='details'"
								class="btn btn-primary"
								style="text-align: center; border-radius: 6px" /> <br /> <br />
						</div>

						<div class="col-md-6">

							<div class="card">
								<div class="card-body" ng-show="show=='deposit'">
									<h4 class="card-title">Deposits</h4>

									<p class="card-text">
									<form action="RestServiceController" method="POST">

										<input type="hidden" name="action" value="deposit" /> 
										<input type="number" name="amt" placeholder="Please enter amount">
											
								
										<select name="bankname" class="btn btn-primary dropdown-toggle" type="button">
											<option value="choose">Choose Bank</option>
										  	<option value="hdcc">hdcc</option>
										  <option value="infinity">infinity</option>
										</select>
									<button class="btn btn-primary"
											style="text-align: center; border-radius: 6px">Deposit</button>
											<br/>
											<p>Enter Your Aadhar Number:</p>
											<input type="text" name="aadharno" placeholder="enter aadhar number" maxlimit="12">
											
									</form>



								</div>

								<!-- widraws -->
								<div class="card-body" ng-show="show=='withdraw'">
									<h4 class="card-title">Withdraw</h4>

									<p class="card-text">
									<form action="BankTransactionController" method="POST">

										<input type="hidden" name="action" value="deposit" /> 
										<input type="number" name="amt" placeholder="Please enter amount">
											
								
										<select name="bankname" class="btn btn-primary dropdown-toggle" type="button">
											<option value="choose">Choose Bank</option>
										  	<option value="hdcc">hdcc</option>
										  <option value="infinity">infinity</option>
										</select>
									<button class="btn btn-primary"
											style="text-align: center; border-radius: 6px">Deposit</button>
											<br/>
											<p>Enter Your Aadhar Number:</p>
											<input type="text" name="aadharno" placeholder="enter aadhar number" maxlimit="12">
									</form>



								</div>

								
							</div>

						</div>


					</div>
					
					<div class="row">
						<div class="col-md-4">
						<div class="card" ng-show="show=='details'">
							<div class="card-body">
									<h4 class="card-title">Details</h4>
									<p class="card-text">
									<h6
										class="card-subtitle mb-2 text-muted justify-content-center">Your
										citibank Details</h6>
									<p class="card-text justify-content-center">
									<div class="row">
										<div class="col-md-6">
											<strong>Aadhar no:</strong><br> <strong>Account
												Create Date:</strong><br> <strong>Account Number:</strong><br>
											<strong>Account type:</strong><br> <strong>Ifsc
												Code:</strong><br> <strong>Account balance:</strong><br>
										</div>

										<!--  get citi details here-->
										<div class="col-md-6">
											<c:out value='${citylist.aadharno}' />
											<c:out value='${citylist.acccreatedate}' />
											<c:out value='${citylist.accno}' />
											<c:out value='${citylist.acctype}' />
											<c:out value='${citylist.ifsc}' />
											<c:out value='${citylist.accbalance}' />

											<br>

										</div>
									</div>
									</p>



								</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card" ng-show="show=='details'">
							<div class="card-body">
									<h4 class="card-title">Details</h4>
									<p class="card-text">
									<h6
										class="card-subtitle mb-2 text-muted justify-content-center">Your
										HDCC Details</h6>
									<p class="card-text justify-content-center">
									<div class="row">
										<div class="col-md-6">
											<strong>Aadhar no:</strong><br> <strong>Account
												Create Date:</strong><br> <strong>Account Number:</strong><br>
											<strong>Account type:</strong><br> <strong>Ifsc
												Code:</strong><br> <strong>Account balance:</strong><br>


										</div>

										<!--  get hdcc details here-->

										<div class="col-md-6">
											<br>
											<c:forEach items="${aadhar}" var="values">
										<tr>
											<td><c:out value="${values.aadhar}"/></td>
										

										</tr>
									</c:forEach>
										</div>
									</div>
									</p>



								</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card" ng-show="show=='details'">
							<div class="card-body">
									<h4 class="card-title">Details</h4>
									<p class="card-text">
									<h6
										class="card-subtitle mb-2 text-muted justify-content-center">Your
										Infinity Details</h6>
									<p class="card-text justify-content-center">
									<div class="row">
										<div class="col-md-6">
											<strong>Aadhar no:</strong><br> <strong>Account
												Create Date:</strong><br> <strong>Account Number:</strong><br>
											<strong>Account type:</strong><br> <strong>Ifsc
												Code:</strong><br> <strong>Account balance:</strong><br>


										</div>

										<!--  get citi details here-->
										<div class="col-md-6">
											<c:out value='${citylist.aadharno}' />
											<c:out value='${citylist.acccreatedate}' />
											<c:out value='${citylist.accno}' />
											<c:out value='${citylist.acctype}' />
											<c:out value='${citylist.ifsc}' />
											<c:out value='${citylist.accbalance}' />

											<br>

										</div>
									</div>
									</p>



								</div>
						</div>
					</div>
					</div>

				</div>



			</div>
			<div
				class="tab-pane fade <c:if test="${selectedTab == '3'}">active show</c:if>"
				id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">


				<form action="ManagerController" method="POST">

					<div class="row">
						<div class="col-md-6">
							<div class="card ">
								<div class="card-body">
									<h4 class="card-title justify-content-center">Account
										Details</h4>
									<h6
										class="card-subtitle mb-2 text-muted justify-content-center">Your
										Infinity Details</h6>
									<p class="card-text justify-content-center">
									<div class="row">
										<div class="col-md-6">
											<strong>Aadhar no:</strong><br> <strong>Account
												Create Date:</strong><br> <strong>Account Number:</strong><br>
											<strong>Account type:</strong><br> <strong>Ifsc
												Code:</strong><br> <strong>Account balance:</strong><br>


										</div>
										<div class="col-md-6">value</div>
									</div>
									</p>

								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Fixed Deposits</h4>
									<h6 class="card-subtitle mb-2 text-muted">Your total fixed
										Deposits :</h6>

									<p class="card-text">
									<table>

										<thead>
											<tr>
												<th>Fd No.</th>
												<th>Amount</th>
												<th>Start Date</th>
												<th>End Date</th>
											</tr>
										</thead>

										<!-- infiniti details -->
										<tbody>
											<c:forEach items="${hdccfdlist}" var="fds">
												<tr>
													<td><c:out value="${hdccfdlist.fixedNo}" /></td>
													<td><c:out value="${hdccfdlist.amount}" /></td>
													<td><c:out value="${hdccfdlist.startdate}" /></td>
													<td><c:out value="${hdccfdlist.enddate}" /></td>
												</tr>
											</c:forEach>
										</tbody>



									</table>


								</div>

							</div>

						</div>

					</div>



				</form>




			</div>
		</div>
	</div>




	</div>


</body>

<script>
	$(".dropdown-menu li a").click(
			function() {
				$(this).parents(".dropdown").find('.btn').html(
						$(this).text() + ' <span class="caret"></span>');
				$(this).parents(".dropdown").find('.btn').val(
						$(this).data('value'));
			});
</script>

</html>

