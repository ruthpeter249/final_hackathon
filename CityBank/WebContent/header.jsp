<!DOCTYPE html>
<html lang="en">
  <head>
    <title>City Bank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/stylecity.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://code.angularjs.org/1.4.0-rc.0/angular.js"></script>
    <script src="js/app.js"></script>
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" ng-app="animation">
  
    <div ng-controller="MainController">

          <div class="container" animate-hide="hide" > 
            <div class="row align-items-center justify-content-center pt-4">
              <div class="col-md-12 col-lg-7 text-center">
                <h1>Current Rates</h1>  
             
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                  <div class="jumbotron">
                      <h1 class="display-8">Fixed Deposit Rates</h1>
                      <hr class="my-4">
                      <p>
                      
                          <table style="width:100%">
                              <tr>
                                <th>Domestic General:</th>
                                <td>6.70%</td>
                                <td>6.70%</td>
                              </tr>
                              <tr>
                                  <th>Domestic General:</th>
                                  <td>6.70%</td>
                                  <td>6.70%</td>
                              </tr>
                              <tr>
                                  <th>Domestic General:</th>
                                  <td>6.70%</td>
                                  <td>6.70%</td>
                              </tr>
                            </table>
                            
                      </p>
                    </div>
                
              </div>
              <div class="col-md-6 col-lg-6">
                  <div class="jumbotron">
                      <h1 class="display-8">Saving Account rates</h1>
                      <hr class="my-4">
                      <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                  
                    </div>
              </div>
            </div>
            </div>

  
     
  


  <div class="site-wrap"  id="home-section">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
      
    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center position-relative">
          
            
            <div class="site-logo">
              <a href="index.html" class="text-black"><span class="text-primary">CITYBANK</a>
            </div>
            
              <nav class="site-navigation text-center ml-auto" role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  <li><a href="index.jsp" class="nav-link">Home</a></li>
               
                
                  <li><a href="" class="nav-link" ng-click="hide = !hide" >Rates</a></li>
                
                  
                <li>
               
                   
                   </div>
                   <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                   <form action="ValidationController" method="POST"> 
                     <a class="dropdown-item" href="">Logout</a>
              </form>
                   </div>
                 </li>

                  
                
                </ul>
              </nav>
          
            
            </div>
          <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>
    