

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Modal</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta http-equiv="content-type" content="3,url=kyc_fetch_display.jsp">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/test.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700"
	rel="stylesheet">

<link rel="stylesheet" href="fonts/icomoon/style.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">

<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/stylecity.css">
<link rel="stylesheet"
	href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://code.angularjs.org/1.4.0-rc.0/angular.js"></script>
<script src="js/app.js"></script>

<style>
#overlay {
	display: none;
	background: red;
}

#overlay {
	display: block;
	background: red;
}
</style>
</head>

<body>



	<div class="row">
		<div>

			<div class="site-wrap" id="home-section">

				<div class="site-mobile-menu site-navbar-target">
					<div class="site-mobile-menu-header">
						<div class="site-mobile-menu-close mt-3">
							<span class="icon-close2 js-menu-toggle"></span>
						</div>
					</div>
					<div class="site-mobile-menu-body"></div>
				</div>


				<header class="site-navbar js-sticky-header site-navbar-target"
					role="banner">

					<div class="container">
						<div class="row align-items-center position-relative">


							<div class="site-logo">
								<a href="index.html" class="text-black"><span
									class="text-primary">CITYBANK</a>
							</div>

							<nav class="site-navigation text-center ml-auto"
								role="navigation">

								<ul
									class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
									<li><a href="index.jsp" class="nav-link">Home</a></li>





									<li><a class="dropdown-item" href="index.jsp">Logout</a></li>



									</form>
						</div>
						</li>



						</ul>
						</nav>


					</div>
					<div class="toggle-button d-inline-block d-lg-none">
						<a href="#"
							class="site-menu-toggle py-5 js-menu-toggle text-black"><span
							class="icon-menu h3"></span></a>
					</div>
			</div>


			</header>

		</div>
		<br />
		<div class="col-lg-1"></div>
		<div class="col-lg-8">
			<br />

			<p class="h4 mb-4 pl-4">Have a Account in Other Bank? Want us to
				show you sophisticated all banks view?</p>
			<input type="hidden" name="selectbankview"
				value="<%request.getAttribute("selectbankview");%>">

		<!-- ${accmaphdcc} <br /> ${accmapinfinity} --> Please Select the bank:


			<div class="row">

				<c:if test="${selectbankview=='hdcc'}">

					<div class="col-md-3">
						<form action=RestServiceController method="POST">

							<input type="hidden" name="action" value="hdcc">
							<button class="btn btn-primary my-4 btn-block" type="submit"
								name="kyc_create_account">HDCC Bank</button>
						</form>

					</div>

				</c:if>


				<c:if test="${selectbankview=='infinity'}">
					<div class="col-md-3">
						<form action=RestServiceController method="POST">
							<input type="hidden" name="action" value="infinity">


							<button class="btn btn-primary my-4 btn-block" type="submit"
								name="kyc_create_account">Infinity Bank</button>

						</form>
					</div>
				</c:if>
			</div>
			<div class="row">

				<!-- For hdcc only-->
				<c:if test="${selectbankview=='hdcc'}">
					<div class="col-md-6">
						<form action=RestServiceController method="POST">
							<input type="hidden" name="action" value="nofusion1">


							<button class="btn btn-danger my-4 btn-block" type="submit"
								name="kyc_create_account">No don't fetch details from
								other bank</button>

						</form>
					</div>

				</c:if>

				<!-- For infiniti only -->

				<c:if test="${selectbankview=='infinity'}">
					<div class="col-md-6">
						<form action=RestServiceController method="POST">
							<input type="hidden" name="action" value="nofusion2">


							<button class="btn btn-danger my-4 btn-block" type="submit"
								name="kyc_create_account">No don't fetch details from
								other bank</button>

						</form>
					</div>

				</c:if>

			</div>
		</div>


	</div>
	<div class="col-lg-2"></div>


	</div>

</body>


</html>

