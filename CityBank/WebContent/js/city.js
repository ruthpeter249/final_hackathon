var citiesByState = {
	Odisha: ["Bhubaneswar","Puri","Cuttack"],
	Andhra_Pradesh: ["Hyderabad ", "Amaravati","Visakhapatnam", "Vijayawada", "Guntur", "Nellore"," Tirupati"],
	West_Bengal: ["Alipore", "Alipur Duar","Asansol "," Baharampur","Bally ","Balurghat","Bankura","Baranagar","Barasat","Barrackpore","Bhatpara","Bishnupur","Budge Budge","Burdwan","Chandernagore","Darjiling","Diamond Harbour","Dum Dum","Durgapur","Halisahar","Haora","Hugli"," Ingraj"," Bazar", "Jalpaiguri", "Kalimpong", "Kamarhati", "Kanchrapara", "Kharagpur", "Koch", "Bihar", "Kolkata"," Krishnanagar"," Malda"," Midnapore"," Murshidabad", "Navadwip"," Palashi ","Panihati" ,"Purulia" ,"Raiganj" ,"Santipur" ,"Shantiniketan ","Shrirampur ","Siliguri" ,"Siuri ","Tamluk ","Titagar"],
	ArunachalPradesh: ["Itanagar"],
	Bihar: [ "Ara","Baruni","Begusarai","Bettiah","Bhagalpur","  Bihar Sharif"," Bodh Gaya","Buxar","Chapra","Darbhanga","Dehri"," Dinapur Nizamat","Gaya","Hajipur","Jamalpur",  "Madhubani",
			"Katihar","Motihari","Munger","Muzaffarpur","Patna","Purnia","Pusa","Saharsa","Samastipur","Sasaram","Sitamarhi","Siwan"],
    Chandigarh:["Chandigarh"],
    Chhattisgarh:["Ambikapur","Bhilai","Bilaspur","Dhamtari","Durg","Jagdalpur","Raipur","Rajnandgaon"],
    Gujarat:["Ahmedabad", "Surat", "Rajkot", "Junagadh" ," Vadodara"],
    Himachal_Pradesh:["Shimla","Dharamshala", "Mandi","Solan"," Bilaspur" ," Chamba"],
    Haryana:["Chandigarh ","Faridabad"," Gurgaon", "Sonipat", "Panipat "," Ambala"],
    Jammu_Kashmir:["Srinagar","Jammu","Anantnag", "Leh", "Udhampur", "Ramnagar", "Baramulla"],
    Jharkhand:["Ranchi","Bokaro", "Jamshedpur"," Deoghar"," Hazaribagh", "Dhanbad"],
    Karnataka:["Bengaluru",	"Mysore", "Davangere"," Mangalore", "Hubli-Dharwad" ," Belgaum"],
    Kerala: ["Alappuzha", "Badagara","Idukki","Kannur","Kochi","Kollam","Kottayam","Kozhikode","Mattancheri","Palakkad","Thalassery","Thiruvananthapuram","Thrissur"],
    Maharashtra:["Mumbai","Pune","Nagpur"],
    Mizoram:["Aizawl","Lunglei","Serchhip"," Champhai"," Tuipang" , "Mamit"],
    Manipur:["Imphal","Bishnupur"," Ukhrul","Tamenglong"," Chandel"," Senapati"],
    Daman_Diu:["Daman"],
    Dadra_NagarHaveli: ["Silvassa"],
    Goa:["Vasco-da-Gama", "Ponda", "Margao"," Mapusa"," Goa Velha"],
    Uttar_Pradesh:["Lucknow	","Noida"," Varanasi"," Allahabad"," Agra" ," Kanpur"],
    Telangana:["Hyderabad ","Warangal", "Nizamabad"," Karimnagar"," Adilabad "," Khammam"],
    Punjab:["Chandigarh ","Amritsar"," Jalandhar"," Ludhiana"," Patiala","Kapurthala"],
    Rajasthan:["Jaipur",	"Bikaner"," Jaisalmer"," Jodhpur"," Udaipur" ," Ajmer"],
    Sikkim:["Gangtok","Namchi", "Gyalshing", "Mangan "," Rabdentse"],
    Tamil_Nadu:["Chennai",	"Tiruchirappalli"," Madurai"," Erode"," Vellore" ," Coimbatore"],
    Tripura:["Agartala","Amarpur", "Kumarghat", "Udaipur", "Gakulnagar","Kunjaban"],
    Uttarakhand:["Dehradun",	"Haridwar", "Roorkee", "Rishikesh", "Kashipur ","Haldwani"],
    Madhya_Pradesh:["Bhopal","Indore", "Gwalior", "Jabalpur", "Ujjain ","Sagar"],
    Meghalaya:["Shillong","Cherrapunji", "Tura", "Jowai", "Baghmara","Nongpoh"],
    Nagaland:["Kohima","Tuensang", "Zunheboto"," Mokokchung"," Kiphire Sadar", "Phek"],
    New_Delhi:["Delhi"],
    Lakshadweep:["Kavaratti"],
    Pondicherry:["Puducherry"],

}


function makeSubmenu(value) {
if(value.length==0) 
	document.getElementById("citySelect").innerHTML = "<option></option>";
else {
var citiesOptions = "";
for(cityId in citiesByState[value]) {
citiesOptions+="<option>"+citiesByState[value][cityId]+"</option>";
}

document.getElementById("citySelect").innerHTML = citiesOptions;
}
}
function displaySelected() { var country = document.getElementById("countrySelect").value;
var city = document.getElementById("citySelect").value;
alert(country+"\n"+city);
}
function resetSelection() {
document.getElementById("countrySelect").selectedIndex = 0;
document.getElementById("citySelect").selectedIndex = 0;
}

