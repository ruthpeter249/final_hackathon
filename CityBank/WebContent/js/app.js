var app = angular.module('animation', [
      
]).controller('MainController', function($scope) {
  $scope.hide = true;      
}).directive('animateHide', function() {
  return {
    link: function(scope, element, attrs) {
      
      scope.$watch(attrs.animateHide, function(val) {
        if(!val) {
          element.animate({
            "height": '500px',
            "opacity": "1"
          }, 300).show();
        } else {
          element.animate({
            "height": '0px',
            "opacity": "0"
          }, 1000, function() {
            $(this).hide();
          });
        }
      });
    }
  }
});

var app1 = angular.module('hide', [])
app.controller('hidecontroller', function ($scope) {
    //This will hide the DIV by default.
    $scope.IsVisible = false;
    $scope.ShowHide = function () {
        //If DIV is visible it will be hidden and vice versa.
        $scope.IsVisible = $scope.IsVisible ? false : true;
    }
});


