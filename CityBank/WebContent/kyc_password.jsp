<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City Bank Registration</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Material Design Bootstrap -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="css/test.css">
</head>
<body>
<br/>
<br/>

	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
			<form action=RestServiceController method="POST">
				<input type="hidden" name="action" value="sign_up_form">

				<p class="h4 mb-4 text-center">You are Almost Done! Now set your password</p>
		 <div style="color:red;"><%request.getAttribute("msg"); %></div><br/>
				Please Set a Password for your Account
				<div class="md-form">
					<i class="fas fa-lock prefix"></i> <input type="password"
						id="defaultRegisterFormPassword" name="password"
						class="form-control" placeholder="Password"
						aria-describedby="defaultRegisterFormPasswordHelpBlock">
					 <div style="color:red;"><%request.getAttribute("passwordmsg"); %></div>
				</div>
				<small id="defaultRegisterFormPhoneHelpBlock"
					class="form-text text-muted mb-4">Minimal 8 characters
					length</small>


				<div class="md-form">
					<i class="fas fa-lock prefix"></i> <input type="password"
						id="passwdInput" class="form-control mb-4"
						placeholder="Re Enter Your Password" name="repassword">
					 <div style="color:red;"><%request.getAttribute("repasswordmsg"); %></div><br/>
				</div>
				
					<div class="row">
					<div class="col-md-3">
							<h5 class="pb-2">Please Enter Captcha:</h5>
								<img src="CaptchaServlet"> <br/><br/>
								<input type="text" name="code">
								
							${captchamsg}
						</div>
							<div class="col-md-4 pt-4">
							
							<button class="btn btn-primary mt-5 " 
							name="kyc_create_account" onclick="verifyCaptcha()">Verify Captcha!</button>
							</div>
						
						</div>

				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-3">
					<h5> Do you want to proceed with these detials?</h5>
						<button class="btn btn-primary my-4 btn-block" type="submit"
							name="kyc_create_account">Yes ! Confirm</button>
					</div>
			</form>
			
			
		</div>
	</div>
	<div class="col-lg-2"></div>
	</div>
</body>

<!-- JQuery -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</html>