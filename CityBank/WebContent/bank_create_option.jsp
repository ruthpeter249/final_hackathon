<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City Bank Registration </title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
<style>

.pt-32{
	padding-top : 20rem !important;
}
</style>
</head>
<body  onload="resetSelection()">
<div class="container">
<div class="row">

		<div class="col-lg-5 pt-32 pr-5 m-5">
	<form class=" " method="post" action="ValidationController">
	
 <input type="hidden" name="action" value="bank_acc_create"> 

    <p class="h4 text-center m-5" >Create Your Bank Account The Traditional way!</p>
<div class="md-form">

   
</div>

    <button class="btn btn-info my-4 btn-block" type="submit" href="create_acc.jsp">Make bank Account The Old Way</button>

</form>
   

	</div>
	<div style=" border-left: 2px solid DarkBlue;height: 900px;margin-top:30px"></div>
		<div class="col-lg-5 pl-5 ml-5 pt-32">
		<div>
		  <img src="images/hdcclogo.PNG" >
		</div>
		
		<div> 
		    <img src="images/Infinitybank.JPG" height="50%" width="50%" style="margin-left:50%;">
		</div>    
		
       	Have a verfied KYC account in another bank? Now create your account in City quickly!!
       	Verify your account using Other bank!!!
       	  <a href="kycoption.jsp"><button class="btn btn-info my-4 btn-block" type="submit">Click Here for Instant Creation !</button></a>
</form>
	</div>

</div>
</div>

</body>


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
<script type="text/javascript" src="js/city.js"></script>

</html>