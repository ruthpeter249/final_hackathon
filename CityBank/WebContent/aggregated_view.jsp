

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Modal</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<meta http-equiv="content-type" content="3,url=kyc_fetch_display.jsp">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/test.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700"
	rel="stylesheet">

<link rel="stylesheet" href="fonts/icomoon/style.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">

<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/stylecity.css">
<link rel="stylesheet"
	href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="css/test.css">
	


<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://code.angularjs.org/1.4.0-rc.0/angular.js"></script>
<script src="js/app.js"></script>

<style>
#overlay {
	display: none;
	background: red;
}

#overlay {
	display: block;
	background: red;
}
</style>
</head>

<body>

	
	<c:import url="header.jsp"></c:import>

	<div id="tick">
		<svg class="checkmark" xmlns="http://www.w3.org/2000/svg"
			width="100px" height="75px" viewBox="0 0 52 52">
		<circle class="checkmark__circle" cx="26" cy="26" r="50" fill="none" />
		<path class="checkmark__check" fill="none"
				d="M14.1 27.2l7.1 7.2 16.7-16.8" />

</svg>

		<center>
			<h1>
				<strong>Your account was created successfully !</strong>
			</h1>

		</center>
	</div>


	<div id="content" style="display: none">

		<section
			class="ftco-about ftco-no-pt ftco-no-pb ftco-section bg-light pt-4" id="about-section">

			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<h1 class="pb-5">
							Hello
							<%
							request.getAttribute("name");
						%>
							name <img src="avatar.png" class="avatar">

						</h1>
						<h2 class="text-primary mb-4">Here is your integrated view</h2>
					</div>
					<form action="RestServiceController" method="post">
					<div class="col-lg-7 float-right" style="text-align: right;">
						<input type="hidden" name="action" value="transactions"> 
						<button class="btn btn-primary"
							style="text-align: center; border-radius: 6px">Go To Transactions</button>
					</div>
					</form>
				</div>

			</div>
		</section>



		<div class="container">
			<nav class="bg-light">
				<div class="nav nav-tabs nav-fill " id="nav-tab" role="tablist">
					<a
						class="nav-item nav-link active"
						id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
						aria-controls="nav-home" aria-selected="true">Citybank</a>
					<c:if test="${(selectbankview == 'both') || (selectbankview == 'hdcc')}">
						<a
							class="nav-item nav-link <c:if test="${selectedTab == '2'}">active</c:if>"
							id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
							role="tab" aria-controls="nav-profile" aria-selected="false">HDCC
							Bank</a>
					</c:if>
					<c:if test="${(selectbankview == 'both') || (selectbankview == 'infinity')}">
						<a
							class="nav-item nav-link <c:if test="${selectedTab == '3'}">active</c:if>"
							id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
							role="tab" aria-controls="nav-contact" aria-selected="false">Infinity
							Bank</a>
					</c:if>

				</div>
			</nav>

			<div class="tab-content py-3 px-3 px-sm-0  bg-white" id="tabs">
				<div
					class="tab-pane fade active show"
					id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

					<input type="hidden" id="selectedTabInput"
						value="${requestScope.selectedTab}">



					<form action="RestServiceController" method="POST">

						<div class="row">
							<div class="col-md-6">
								<div class="card ">
									<div class="card-body">
										<h4 class="card-title justify-content-center">Account
											Details</h4>
										<h6
											class="card-subtitle mb-2 text-muted justify-content-center">Your
											citibank Details</h6>
										<p class="card-text justify-content-center">
										<div class="row">
											<div class="col-md-6">
																								<strong>Aadhar no:</strong><br> 
												<strong>Account Create Date:</strong><br> 
												<strong>Account Number:</strong><br>
												<strong>Account type:</strong><br> 
												<strong>Ifsc Code:</strong><br>
												<strong>Account balance:</strong><br>


											</div>
											
											<!--  get citi details here-->
											${citylist.get(14)}
											${citylist.get(14)}
											${citylist.get(14)}
										
											</div>
										</p>

									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">Fixed Deposits</h4>
										<h6 class="card-subtitle mb-2 text-muted">Your total
											fixed Deposits :</h6>

										<p class="card-text">
										<table>

											<thead>
												<tr>
													<th>Fd No.</th>
													<th>Amount</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
											</thead>
											<tbody>
											<!-- citi fixed deposits -->
												<c:forEach items="${cityfd}" var="fds">
													<tr>
														<td><c:out value="${cityfd.fixedNo}" /></td>
														<td><c:out value="${cityfd.amount}" /></td>
														<td><c:out value="${cityfd.startdate}" /></td>
														<td><c:out value="${cityfd.enddate}" /></td>
													</tr>
												</c:forEach>

												</tr>
											</tbody>


										</table>


									</div>

								</div>

							</div>

						</div>

				
					</form>





				</div>
				<div class="tab-pane fade show" id="nav-profile" role="tabpanel"
					aria-labelledby="nav-profile-tab">

						<form action="RestServiceController" method="POST">

						<div class="row">
							<div class="col-md-6">
								<div class="card ">
									<div class="card-body">
										<h4 class="card-title justify-content-center">Account
											Details</h4>
										<h6
											class="card-subtitle mb-2 text-muted justify-content-center">Your
											HDCC Details</h6>
										<p class="card-text justify-content-center">
										<div class="row">
										<div class="col-md-6">
											
											    <strong>Aadhar no:</strong><br> 
												<strong>Account Create Date:</strong><br> 
												<strong>Account Number:</strong><br>
												<strong>Account type:</strong><br> 
												<strong>Ifsc Code:</strong><br>
												<strong>Account balance:</strong><br>
											</div>
												<!-- hdcc details -->
										<div class="col-md-6">
													<c:out value='${accmaphdcc["aadharno"]}'/>
													<c:out value='${accmaphdcc["acccreatedate"]}'/>
													<c:out value='${accmaphdcc["accno"]}'/>
													<c:out value='${accmaphdcc["acctype"]}'/>
													<c:out value='${accmaphdcc["ifsc"]}'/>
													<c:out value='${accmaphdcc["accbalance"]}'/>
													
													<br>
													
											</div>
										</div>
										</p>

									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">Fixed Deposits</h4>
										<!-- input
						type="submit" name="action" value="fetchFdDetails"
						class="btn btn-primary "
						style="text-align: center; border-radius: 6px">
										 -->
										<p class="card-text">
										<table>
											<thead>
												<tr>
													<th>Fd No.</th>
													<th>Amount</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
											</thead>
											<!-- hdcc fd details -->
											<tbody>
											<c:forEach items="${hdccfdlist}" var="fds">
													<tr>
														<td><c:out value="${hdccfdlist.fixedNo}" /></td>
														<td><c:out value="${hdccfdlist.amount}" /></td>
														<td><c:out value="${hdccfdlist.startdate}" /></td>
														<td><c:out value="${hdccfdlist.enddate}" /></td>
													</tr>
												</c:forEach>
											</tbody>


										</table>


									</div>

								</div>

							</div>

						</div>

						
					</form>


				</div>
				<div
					class="tab-pane fade <c:if test="${selectedTab == '3'}">active show</c:if>"
					id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">


					<form action="ManagerController" method="POST">

						<div class="row">
							<div class="col-md-6">
								<div class="card ">
									<div class="card-body">
										<h4 class="card-title justify-content-center">Account
											Details</h4>
										<h6
											class="card-subtitle mb-2 text-muted justify-content-center">Your
											Infinity Details</h6>
										<p class="card-text justify-content-center">
										<div class="row">
											<div class="col-md-6">
												<strong>Aadhar no:</strong><br> <strong>Account
													Create Date:</strong><br> <strong>Account Number:</strong><br>
												<strong>Account type:</strong><br> <strong>Ifsc
													Code:</strong><br> <strong>Account balance:</strong><br>


											</div>
											<div class="col-md-6">value</div>
										</div>
										</p>

									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">Fixed Deposits</h4>
										<h6 class="card-subtitle mb-2 text-muted">Your total
											fixed Deposits :</h6>
									
										<p class="card-text">
										<table>

											<thead>
												<tr>
													<th>Fd No.</th>
													<th>Amount</th>
													<th>Start Date</th>
													<th>End Date</th>
												</tr>
											</thead>
											
											<!-- infiniti details -->
											<tbody>
											<c:forEach items="${hdccfdlist}" var="fds">
													<tr>
														<td><c:out value="${hdccfdlist.fixedNo}" /></td>
														<td><c:out value="${hdccfdlist.amount}" /></td>
														<td><c:out value="${hdccfdlist.startdate}" /></td>
														<td><c:out value="${hdccfdlist.enddate}" /></td>
													</tr>
												</c:forEach>
											</tbody>
											


										</table>


									</div>

								</div>

							</div>

						</div>

						

					</form>




				</div>
			</div>
		</div>




	</div>


</body>
<script>
	setTimeout(function() {
		$('#tick').fadeOut();
	}, 3000);
</script>
<script>
	setTimeout(function() {
		$('#content').fadeIn();
	}, 4000);
	//$('#content').show();
</script>

</html>

