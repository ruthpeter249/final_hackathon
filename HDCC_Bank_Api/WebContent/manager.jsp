<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.*"%>
<%@page import="com.ofss.model.*"%>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

</head>

<body id="body">
  	<section
		class="ftco-about ftco-no-pt ftco-no-pb ftco-section bg-light pt-4"
		id="about-section">
		<div class="container">
			<div class="row pb-3">
				<div class="col-md-11 heading-section ftco-animate pb-5 pt-3">
					<span class="subheading pt-5 pb-3"><%=session.getAttribute("email") %></span>
					<h2 class="mb-4"
						style="font-size: 44px; text-transform: capitalize;">
						Welcome
						<%=session.getAttribute("managername") %></h2>
				</div>
				<div class="col-md-1 pt-5">


					<form action="ValidationController" method="POST">
					
					<input name="action" value="logout" class="btn btn-primary" type="submit"
						style="margin-top: 9%; border-radius: 6px">
					
					
					</form>
					
				</div>
			</div>
		</div>
		</section>
					<div class="container">
					<nav class="bg-light">
					<div class="nav nav-tabs nav-fill " id="nav-tab" role="tablist">
						<a class="nav-item nav-link <c:if test="${selectedTab == '1'}">active</c:if>" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Transaction Logs</a>
						<!--  <a class="nav-item nav-link <c:if test="${selectedTab == '2'}">active</c:if>" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>-->
						<a class="nav-item nav-link <c:if test="${selectedTab == '3'}">active</c:if>" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Blacklisted customers</a>
					<!--  	<a class="nav-item nav-link <c:if test="${selectedTab == '4'}">active</c:if>" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">Interest Rates</a> -->
					</div>
				</nav>
				
				<div class="tab-content py-3 px-3 px-sm-0  bg-white" id="tabs">
					<div class="tab-pane fade  <c:if test="${selectedTab == '1'}">active show</c:if>" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

				 <input type="hidden" id="selectedTabInput" value="${requestScope.selectedTab}">
				 
				 
				 ${requestScope.selectedTab}
		

				<!-- input type="hidden" id="selectedTabInput" value="${request.getAttribute("selectedTab")}"> -->
				<%=request.getAttribute("selectedTab")%>
				

				<div class="row">
					
					<form action="ManagerController" method="POST">

						<h3 class="pt-5 pb-3">Refresh and Get Logs whenever API has
							been called for KYC</h3>
						<input type="submit" name="action" value="fetchlogs"
							class="btn btn-primary"
							style="text-align: center; border-radius: 6px">
						</center>


						<div class="col-md-12 col-lg-12 justify-content-center pt-4">

							<table class="table table-hover">

								<thead>
									<tr>
										<th>Ifsc Number</th>
										<th>Name</th>
										<th>branch name</th>
										<th>Time Stamp</th>
										<th>attempts</th>
									</tr>
								</thead>

								<tbody>
								
		
								
								 
								 
									<c:forEach items="${logs}" var="logs">
										<tr>
											<td><c:out value="${logs.hdccNumber}" /></td>
											<td><c:out value="${logs.hdccBranchName}" /></td>
											<td><c:out value="${logs.hdccFirstname}" /></td>

											<td><c:out value="${logs.hdccLogTimestamp}" /></td>
											<td><c:out value="${logs.hdccLogAttempt}" /></td>

										</tr>
									</c:forEach>

								</tbody>

							</table>





						</div>


					</form>



			</div>
				


					</div>
					<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
		
				<div class="row">
					<div class="col-md-12 col-lg-12 pl-lg-5 py-5"></div>
					Potato
				</div>

	</div>
					<div class="tab-pane fade <c:if test="${selectedTab == '3'}">active show</c:if>" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
					
					
					
					<div class="row">

					<input type="hidden" id="selectedTabInput" value="${requestScope.selectedTab}">
					
					
					<form action="ManagerController" method="POST">

						<h3 class="pt-5 pb-3">Get customers who have been blackListed</h3>
						<input type="submit" name="action" value="blacklist"
							class="btn btn-primary"
							style="text-align: center; border-radius: 6px">
						</center>


						<div class="col-md-12 col-lg-12 justify-content-center pt-4">

							<table class="table table-hover">

								<thead>
									<tr>
										<th>Customer name</th>
										<th>Customer ID</th>
										<th>Customer EmailID</th>
										<th>Last Login</th>
										<th>Kyc Done</th>
										
									</tr>
								</thead>

								<tbody>
								
									<c:forEach items="${blackList}" var="black">
										<tr>
											<td><c:out value="${black.fname}" /></td>
											<td><c:out value="${black.custID}" /></td>
											<td><c:out value="${black.emailId}" /></td>

											<td><c:out value="${black.lastlogin}" /></td>
											<td><c:out value="${black.kycCheck}" /></td>

										</tr>
									</c:forEach>

								</tbody>

							</table>





						</div>


					</form>



				</div></div>
					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
					
						<h3 class="pt-5 pb-3">Change Interest Rates</h3>
					
						

			</div>
				</div>



			

		</div>



</body>






    <!--==========================
      Services Section
    ============================-->
   

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>


	
<script>
    $(function() {
        var param = document.getElementById("selectedTabInput").value;
        if (param != 0) {
            $('#tabs').tabs({
            	
                active : param
            });
        } else {
            $('#tabs').tabs();
        }
    });
</script>
	
</body>
</html>
