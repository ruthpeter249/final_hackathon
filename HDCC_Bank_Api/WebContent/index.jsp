<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body id="body">

  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto">HDC<span>C</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#body">Home</a></li>
          <li><a href="#about">About Us</a></li>          
          <li class="nav-item"><a href="#saving-section" class="nav-link"><span>Login</span></a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->



  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

    <div class="intro-content">
      <h2>We Understand Your <span>World</span>!<br></h2>
      <div>
        <a href="#about" class="btn-get-started scrollto">Get Started</a>
      </div>
    </div>

   <!--   <div id="intro-carousel" class="owl-carousel" >
      <div class="item" style="background-image: url('img/intro-carousel/1.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/2.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/3.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/4.jpg');"></div>
      <div class="item" style="background-image: url('img/intro-carousel/5.jpg');"></div>
    </div>-->

  </section><!-- #intro -->

  <main id="main">

    <section id="about">
      <div class="container">
        <div class="section-header">
          <h2>About Us</h2>
          <p>HDCC Bank is an Indian banking and financial services company headquartered in Mumbai, Maharashtra. It has a base of 170,190 permanent employees as of 30 June 2019. HDCC Bank is India largest private sector lender by assets</p>
        </div>
        <div>
          <h3> <b> Our Core Values </b></h3>
        </div>
        <div class="row">
            
          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="fa fa-street-view"></i></div>
              <h4 class="title"><a href="">Customer Centricity</a></h4>
              <p class="description"></p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="fa fa-trophy"></i></div>
              <h4 class="title"><a href="">Pursue Excellence</a></h4>
              <p class="description"></p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-handshake-o"></i></div>
              <h4 class="title"><a href="">Integrity and honesty</a></h4>
              <p class="description"></p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-users"></i></div>
              <h4 class="title"><a href="">Teamwork</a></h4>
              <p class="description"></p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->
    
    <section id="saving-section">
      <div class="container">
        <div class="section-header">
          <h2>Account Login</h2>
        </div>
      </div>
			<div class="row">
			  <div class="col pb-4">
			  </div>
			</div>
			<span class="border" style="padding-top: 20px ;padding-bottom: 20px">
			
			<div class="row">
											  <div class="col-md-2"></div>
			
				<div class="col-lg-4 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
				  <div class="block-team-member-1 text-center rounded">
				   
					<h3 class="font-size-20">Customer Login</h3>
					<p class="px-3 mb-3">Proceed directly by entering your loginId and password </p>
  
					<div class="row">
  
					  <div class="col-md-2"></div>
					
					  <div class="col-md-8">
						  <form action="ValidationController" method="post">
							<input type="hidden" name="action" value="login_form_customer"> 
							 <!-- <p style="color:red;"> ${msg }</p> -->
							  <div class="form-group row form-group-lg">
								<div class="col-md-12 mb-4 mb-lg-0 ">
								  <input type="text" class="form-control input-sm" placeholder="Email"  name="email">
								</div>
							  </div>
							  <div class="form-group row">
								<div class="col-md-12">
								  <input type="password" class="form-control input-sm" placeholder="Password" name="pass" >
								</div>
							  </div>
							  
							  <div class="form-group row">
								  <div class="col-md-6 mx-auto">
									<input type="submit" href="customer.jsp" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
								  </div>
								</div>
						 	</form>
					  </div>
					  
  
					</div>
					 </div>
				</div>
	  
				<div class="col-lg-4 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
						<div class="block-team-member-1 text-center rounded">
						 
						  <h3 class="font-size-20">Manager Login</h3>
						  <p class="px-3 mb-3 ">Proceed directly by entering your loginId and password </p>
		
						  <div class="row">
		
							<div class="col-md-2"></div>
						  
							<div class="col-md-8">
								<form action="ValidationController" method="post">
								<div style="color:red">${msg }</div>
								  <input type="hidden" name="action" value="login_form_manager"> 
								   <!-- <p style="color:red;"> ${msg }</p> -->
									<div class="form-group row form-group-lg">
									  <div class="col-md-12 mb-4 mb-lg-0 ">
										<input type="text" class="form-control input-sm" placeholder="Email"  name="email">
									  </div>
									</div>
									<div class="form-group row">
									  <div class="col-md-12">
										<input type="password" class="form-control input-sm" placeholder="Password" name="pass" >
									  </div>
									</div>
									
									<div class="form-group row">
										<div class="col-md-6 mx-auto">
										  <input type="submit" href="manager.jsp"class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
										</div>
									  </div>
								   </form>
							</div>
		
						  </div>
						   </div>
					  </div>
	  
				
			  </div>
			</span>
		  </div>
		</div>
	  </div>
			
			
</section>
   
   
   
     <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Contact Us</h2>
          <h3><b>We would love to hear from you </b></h3>
            <p>Contact us through any mode for your queries.</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>HDCC Bank , HDCC House, C-2,IOH Blk, Pandurang Budhkar Marg, Worli, Mumbai - 400 025</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+999999999999">+9 9999 99999 99</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:@example.com">contact@example.com</a></p>
            </div>
          </div>

        </div>
      </div>

     
      </div>
    </section><!-- #contact -->

  </main>
 
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
