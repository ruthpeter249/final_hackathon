package com.ofss.controller;

import java.io.IOException;
import java.util.ArrayList;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.dao.DBAccount;
import com.ofss.model.Hdcc_CustomerLogin;
import com.ofss.model.Hdcc_ManagerLogin;

/**
 * Servlet implementation class ValidationController
 */
@WebServlet("/ValidationController")
public class ValidationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ValidationController() {
        super();
    }

    HttpSession session;
    DataSource ds;
	Connection con;
	ArrayList<Hdcc_CustomerLogin> list;
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB");

	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
		Hdcc_CustomerLogin customer =new Hdcc_CustomerLogin();
		String action = request.getParameter("action");
		if(action== null){
		session = request.getSession();

		session.setAttribute("ds", ds);
		}
		else{
		doPost(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		DBAccount account=new DBAccount();
		String action=request.getParameter("action");
		Hdcc_CustomerLogin customer=new Hdcc_CustomerLogin();
		session=request.getSession();
		System.out.println("hiii");
		try {
			con = ds.getConnection();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			account.setConnection(con);
		
			if(action.equals("login_form_customer")){
				String email=request.getParameter("email");
				String password=request.getParameter("pass");
				
				boolean status=false;
				try {
				status = account.checkCustomerLogin(email, password);
				} catch (ClassNotFoundException |SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(status);
				if(status==true){
			            //get the old session and invalidate
			     HttpSession oldSession = request.getSession(false);
			     if (oldSession != null) {
			         oldSession.invalidate();
			            }
					session=request.getSession(true);  
					  //setting session to expiry in 5 mins
		            session.setMaxInactiveInterval(5*60);

		            Cookie message = new Cookie("message", "Welcome");
		            response.addCookie(message);
		        	System.out.println(email);
					
					session.setAttribute("email",email);
//					request.setAttribute("email",email);
					
				    response.sendRedirect("customer.jsp");
					//request.getRequestDispatcher("welcome.jsp").forward(request, response);
					
				}else{
					System.out.println("error");
					request.setAttribute("msg", "Invalid Login,Please Try Again <i class='far fa-frown' style='font-size:24px;color:red'></i>");
					request.setAttribute("email",email);
					
					request.getRequestDispatcher("error.jsp").forward(request, response);
					
				}
			}
			
			if(action.equals("logout")) { 
	            System.out.println("logout section");
	            //invalidate the session if exists
	            HttpSession session = request.getSession(false);
	            if(session != null){
	                session.invalidate();
	            	
	            }
	            //System.out.println("session id"+session.getId());
	            response.sendRedirect(request.getContextPath() + "/index.jsp");
	        	//request.getRequestDispatcher("login.jsp").forward(request, response);
			}
			
			if(action.equals("login_form_manager")){
				String email=request.getParameter("email");
				String password=request.getParameter("pass");
				String managerName="";
				Hdcc_ManagerLogin m=new Hdcc_ManagerLogin();
				
				boolean status=false;
				try {
				status = account.checkManagerLogin(email, password);
				} catch (ClassNotFoundException |SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(status);
				if(status==true){
			            //get the old session and invalidate
			            HttpSession oldSession = request.getSession(false);
			            if (oldSession != null) {
			                oldSession.invalidate();
			            }
					session=request.getSession(true);  
					  //setting session to expiry in 5 mins
		            session.setMaxInactiveInterval(5*60);

		            Cookie message = new Cookie("message", "Welcome");
		            response.addCookie(message);
		        	System.out.println(email);

		        	try {
						managerName = account.getManagerName(email);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					session.setAttribute("email",email);
//					request.setAttribute("email",email);
					session.setAttribute("managername",managerName);
//					request.setAttribute("email",email);
					session.setAttribute("selectedTab","1");
				  
					
				    response.sendRedirect("manager.jsp");
					//request.getRequestDispatcher("welcome.jsp").forward(request, response);
					
				}else{
					System.out.println("error");
					request.setAttribute("msg", "Invalid Login,Please Try Again <i class='far fa-frown' style='font-size:24px;color:red'></i>");
					request.setAttribute("email",email);
					
					request.getRequestDispatcher("index.jsp").forward(request, response);
					
				}
			}
		
	}

}
