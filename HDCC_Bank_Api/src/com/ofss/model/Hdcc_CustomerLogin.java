package com.ofss.model;

import java.util.HashMap;

public class Hdcc_CustomerLogin {

	private String email;
	private String pass;
	
	private String emailmsg = " ";
	private String passmsg = " ";
	
	HashMap<String,String> map = new HashMap<String,String>();
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getEmailidmsg() {
		return emailmsg;
	}
	public void setEmailidmsg(String emailidmsg) {
		this.emailmsg = emailidmsg;
	}
	public String getPassmsg() {
		return passmsg;
	}
	public void setPassmsg(String passmsg) {
		this.passmsg = passmsg;
	}
	
	public boolean validateUser(String email,String pass)
	{
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		
		if(pass.equals(""))
			passmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(pass.matches(pattern)))
			passmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 if(emailmsg.equals("") && passmsg.equals(""))
		 {
			 System.out.println("emailmsg"+emailmsg);
			 

						return true;
					}
					
					return false;
		}
		
	
	
	
}
