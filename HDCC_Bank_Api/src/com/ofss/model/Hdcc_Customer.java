package com.ofss.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class Hdcc_Customer {

	private String custID;
	private String fname;
	private String mname;
	private String lname;
	private String padd;
	private String cadd;
	private String pincode;
	private String city;
	private String State;
	private String emailId;
	private String occupation;
	private String bdate ;
	private String aadharno;
	private String panno;
	private String passno;
	private String kycCheck;
	private String ifsc;
	private String phnno;
	private String gender;
	private String status;
	private String lastlogin;
	public String getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
	public String getPhnno() {
		return phnno;
	}
	public void setPhnno(String phnno) {
		this.phnno = phnno;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	private String msg;
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Hdcc_Customer()
	{
		
	}
	public String getCustID() {
		return custID;
	}
	public void setCustID(String custID) {
		this.custID = custID;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPadd() {
		return padd;
	}
	public void setPadd(String padd) {
		this.padd = padd;
	}
	public String getCadd() {
		return cadd;
	}
	public void setCadd(String cadd) {
		this.cadd = cadd;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getBdate() {
		return bdate;
	}
	public void setBdate(String bdate) {
		this.bdate = bdate;
	}
	public String getAadharno() {
		return aadharno;
	}
	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getPassno() {
		return passno;
	}
	public void setPassno(String passno) {
		this.passno = passno;
	}
	public String getKycCheck() {
		return kycCheck;
	}
	public void setKycCheck(String kycCheck) {
		this.kycCheck = kycCheck;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public Hdcc_Customer(String custID, String fname, String mname, String lname, String padd, String cadd, String pincode,
			String city, String state, String emailId, String occupation, String bdate, String aadharno, String panno,
			String passno, String kycCheck, String ifsc, String phnno, String gender, String status) {
		super();
		this.custID = custID;
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.padd = padd;
		this.cadd = cadd;
		this.pincode = pincode;
		this.city = city;
		State = state;
		this.emailId = emailId;
		this.occupation = occupation;
		this.bdate = bdate;
		this.aadharno = aadharno;
		this.panno = panno;
		this.passno = passno;
		this.kycCheck = kycCheck;
		this.ifsc = ifsc;
		this.phnno = phnno;
		this.status = status;
		this.gender = gender;
	}
	
}
