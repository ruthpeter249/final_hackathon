package com.ofss.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Hdcc_Account {

	
	private String aadharno;
	private String accno;
	private String acctype;
	private String ifsc;
	private String accbalance;
	private String acccreatedate;
	public Hdcc_Account()
	{
		
	}
	
	public Hdcc_Account(String aadharno, String accno, String acctype, String ifsc, String accbalance,
			String acccreatedate) {
		super();
		this.aadharno = aadharno;
		this.accno = accno;
		this.acctype = acctype;
		this.ifsc = ifsc;
		this.accbalance = accbalance;
		this.acccreatedate = acccreatedate;
	}

	public String getAadharno() {
		return aadharno;
	}
	public void setAadharno(String aadharno) {
		this.aadharno = aadharno;
	}
	public String getAccno() {
		return accno;
	}
	public void setAccno(String accno) {
		this.accno = accno;
	}
	public String getAcctype() {
		return acctype;
	}
	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getAccbalance() {
		return accbalance;
	}
	public void setAccbalance(String accbalance) {
		this.accbalance = accbalance;
	}
	public String getAcccreatedate() {
		return acccreatedate;
	}
	public void setAcccreatedate(String acccreatedate) {
		this.acccreatedate = acccreatedate;
	}
}
