
package com.ofss.resources;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ofss.dao.Hdcc_CustomerDAO;

@Path("testConnection")
public class Hdcc_Customer_Resources {

	Hdcc_CustomerDAO hdccdao = new Hdcc_CustomerDAO();
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String testConnection() throws SQLException {
		return hdccdao.testConnection();
	}
	

}