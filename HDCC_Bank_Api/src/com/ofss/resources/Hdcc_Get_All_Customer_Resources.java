package com.ofss.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ofss.dao.Hdcc_CustomerDAO;
import com.ofss.model.Hdcc_Account;
import com.ofss.model.Hdcc_Customer;


@Path("Hdcc_Customers")
public class Hdcc_Get_All_Customer_Resources {
	Hdcc_CustomerDAO dao = new Hdcc_CustomerDAO();

	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Hdcc_Customer> printAllCustomer()
	{   
		return dao.getAllHdccCustomer();
	}
	
	
	@GET
	@Path("{aadharno}")
	@Produces(MediaType.APPLICATION_JSON)
	public Hdcc_Customer printCustomer(@PathParam ("aadharno") String hdcc_aadharno)
	{
		System.out.println("Inside printHdcc_Customer()");
		return dao.getHdccCustomer(hdcc_aadharno);
	}
}
