package com.ofss.resources;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PUT;


import com.ofss.dao.Hdcc_CustomerDAO;
import com.ofss.model.Hdcc_Account;

@Path("FetchAccountDetails")
public class Hdcc_Acc_Cust_resources {
	Hdcc_CustomerDAO dao = new Hdcc_CustomerDAO();


	@GET
	@Path("{aadharno}")
	@Produces(MediaType.APPLICATION_JSON)
	public Hdcc_Account printEmployee(@PathParam ("aadharno") String hdcc_aadharno)
	{
		System.out.println("Inside printHdcc_Customer()");
		return dao.getHdccAccount(hdcc_aadharno);
	}
	
	
	@PUT
	@Path("/{aadharno}/deposit/{amount}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public int depositHdccAmount(@PathParam ("aadharno") String hdcc_aadharno,@PathParam("amount") String amount)
	{
		System.out.println("Inside withdrawHdcc_Amount()");
		return dao.depositInHdccCustomer(hdcc_aadharno,amount);
	}

	@PUT
	@Path("/{aadharno}/withdraw/{amount}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public int withdrawHdccAmount(@PathParam ("aadharno") String hdcc_aadharno,@PathParam("amount") String amount)
	{
		System.out.println("Inside withdrawHdcc_Amount()");
		return dao.withdrawFromHdccCustomer(hdcc_aadharno,amount);
	}

}
