package com.ofss.InfiBankBeans;

public class InfiBankManager {

	private String infiManagerid;
	private String infiMname;
	private String infiMgrEmailid;
	private String infiMgrPassword;

	public String getInfiManagerid() {
		return infiManagerid;
	}
	public void setInfiManagerid(String infiManagerid) {
		this.infiManagerid = infiManagerid;
	}
	public String getInfiMname() {
		return infiMname;
	}
	public void setInfiMname(String infiMname) {
		this.infiMname = infiMname;
	}
	public String getInfiMgrEmailid() {
		return infiMgrEmailid;
	}
	public void setInfiMgrEmailid(String infiMgrEmailid) {
		this.infiMgrEmailid = infiMgrEmailid;
	}
	public String getInfiMgrPassword() {
		return infiMgrPassword;
	}
	public void setInfiMgrPassword(String infiMgrPassword) {
		this.infiMgrPassword = infiMgrPassword;
	}

}
