package com.ofss.InfiBankBeans;

import java.util.HashMap;

public class InfiBankCustomer {
	private String fname;
	private String mname;
	private String lname;
	private String caddress;
	private String paddress;
	private String phonenumber;
	private String city;
	private String email;
	private String state;
	private String password;
	
	private String fnamemsg="";
	private String mnamemsg="";
	private String  lnamemsg="";
	private String caddressmsg="";
	private String paddressmsg="";
	private String phonenumbermsg="";
	private String emailmsg="";
	private String passwordmsg="";
	private String repasswordmsg="";
	HashMap<String,String> map	=new HashMap<String,String>();;
	
	
	
	
	
	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCaddress() {
		return caddress;
	}

	public void setCaddress(String caddress) {
		this.caddress = caddress;
	}

	public String getPaddress() {
		return paddress;
	}

	public void setPaddress(String paddress) {
		this.paddress = paddress;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFnamemsg() {
		return fnamemsg;
	}

	public void setFnamemsg(String fnamemsg) {
		this.fnamemsg = fnamemsg;
	}

	public String getMnamemsg() {
		return mnamemsg;
	}

	public void setMnamemsg(String mnamemsg) {
		this.mnamemsg = mnamemsg;
	}

	public String getLnamemsg() {
		return lnamemsg;
	}

	public void setLnamemsg(String lnamemsg) {
		this.lnamemsg = lnamemsg;
	}

	public String getCaddressmsg() {
		return caddressmsg;
	}

	public void setCaddressmsg(String caddressmsg) {
		this.caddressmsg = caddressmsg;
	}

	public String getPaddressmsg() {
		return paddressmsg;
	}

	public void setPaddressmsg(String paddressmsg) {
		this.paddressmsg = paddressmsg;
	}

	public String getPhonenumbermsg() {
		return phonenumbermsg;
	}

	public void setPhonenumbermsg(String phonenumbermsg) {
		this.phonenumbermsg = phonenumbermsg;
	}

	public String getEmailmsg() {
		return emailmsg;
	}

	public void setEmailmsg(String emailmsg) {
		this.emailmsg = emailmsg;
	}

	public String getPasswordmsg() {
		return passwordmsg;
	}

	public void setPasswordmsg(String passwordmsg) {
		this.passwordmsg = passwordmsg;
	}

	public String getRepasswordmsg() {
		return repasswordmsg;
	}

	public void setRepasswordmsg(String repasswordmsg) {
		this.repasswordmsg = repasswordmsg;
	}


	
public boolean validate(String email, String password) {
		
	
		for(String s:map.keySet()){
			if(email.equals(s)){
				System.out.println(""+s);
				String p=map.get(s);
				if(password.equals(p)){
					return true;
				}
			}
		}
		return false;
	}
	public boolean validateUser(String fname,String  mname, String  lname, String  caddress, String paddress,String  phonenumber,String  email,String  password,String  repassword) {
		
		if(fname.equals("")){
			fnamemsg="Please Enter First name";
			System.out.println(fname);
		}
		if(mname.equals("")){
			mnamemsg="Please Enter middle name";
			System.out.println(mname);
		}
		if(lname.equals("")){
			lnamemsg="Please Enter last name";
		}
		if(caddress.equals("")){
			caddressmsg="Address Cannot be EMpty ";
		}
		if(paddress.equals("")){
			paddressmsg="Address Cannot be EMpty ";
		}
		if(phonenumber.equals("")){
			phonenumbermsg="Please Enter Phone Number";
		}
		else if(!(phonenumber.length()<=10 || phonenumber.length()>=11)){
			phonenumbermsg="Contact should be of 10/11 digits";
		}
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password.equals(""))
			passwordmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(password.matches(pattern)))
			passwordmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 
		if(repassword.equals(""))
			repasswordmsg="Please Enter repassword";
		
		else if(!(password.equals(repassword)))
			repasswordmsg="Repassword should match password";
		
		if(fnamemsg.equals("") && mnamemsg.equals("") && lnamemsg.equals("") && caddressmsg.equals("")&& paddressmsg.equals("")&& phonenumbermsg.equals("")&& phonenumbermsg.equals("")
				&& emailmsg.equals("")&& passwordmsg.equals("")&& repasswordmsg.equals(""))
		{ System.out.println("fnamemsg"+fnamemsg+"\tmnamemsg"+mnamemsg);
		
			return true;
		}
		
		return false;
	}
	public HashMap<String, String> getMap() {
		return map;
	}
	public void setMap(HashMap<String, String> map) {
		this.map = map;
	}
	public void populate1(String email, String password) {
		// TODO Auto-generated method stub
		map.put(email, password);
			
	}

	public boolean validateSignUpForm(String fname, String lname, String email, String password, String repassword) {
		if(fname.equals("")){
			fnamemsg="Please Enter First name";
			System.out.println(fname);
		}
		if(lname.equals("")){
			lnamemsg="Please Enter last name";
		}
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		if(password.equals(""))
			passwordmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(password.matches(pattern)))
			passwordmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 
		if(repassword.equals(""))
			repasswordmsg="Please Enter repassword";
		
		else if(!(password.equals(repassword)))
			repasswordmsg="Repassword should match password";
		if(fnamemsg.equals("") && lnamemsg.equals("") && emailmsg.equals("")&& passwordmsg.equals("")&& repasswordmsg.equals(""))
		{ System.out.println("fnamemsg"+fnamemsg+"\tmnamemsg"+emailmsg);
		
			return true;
		}
		
		return false;
	}

}
