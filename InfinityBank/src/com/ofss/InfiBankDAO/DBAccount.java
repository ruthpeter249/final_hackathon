package com.ofss.InfiBankDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.ofss.InfiBankBeans.InfiBankCustomer;
import com.ofss.InfiBankBeans.InfiBankManager;


public class DBAccount {
	Connection con;
	PreparedStatement pstmt;
	ArrayList<InfiBankCustomer> list;
	ArrayList<InfiBankManager> listMgr;
	
	public void setConnection(Connection con) {
	this.con = con;
	}
	public ArrayList<InfiBankCustomer> getUsers() throws SQLException {
			list = new ArrayList<InfiBankCustomer>();
			 String sql = "select INFI_CUST_FIRSTNAME, INFI_CUST_EMAILID from INFI_CUSTOMER";
			PreparedStatement pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, cat_id);
			ResultSet rst =pstmt.executeQuery();
			while(rst.next()){
			String namedb=rst.getString("INFI_CUST_FIRSTNAME");
			String emaildb=rst.getString("INFI_CUST_EMAILID");
			System.out.println("name:"+namedb);
			System.out.println("email:"+emaildb);
			InfiBankCustomer c = new InfiBankCustomer();
			c.setFname(namedb);
			c.setEmail(emaildb);
			list.add(c);
			c=null;
			}
			return list;
		}
	public void  insertCustomer(String fname, String lname, String email, String password) throws SQLException {
		 String sql = "insert into INFI_CUSTOMER(INI_CUSTID,INFI_CUST_FIRSTNAME,INFI_CUST_LASTNAME,INFI_CUST_EMAILID,INFI_CUST_PASSWORD)values ('INFI'||CUSTOMER_CUSTID.NEXTVAL,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, fname);
			pstmt.setString(2, lname);
			pstmt.setString(3, email);
			pstmt.setString(4, password);
			pstmt.executeUpdate();//executeUpdate for insert
		System.out.println("just inserted db");
	}
	
	
	public boolean checkCustomerLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("inside login code ");
		String sql="select * from INFI_CUSTOMER";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		ResultSet result=pstmt.executeQuery();
		list=new ArrayList<InfiBankCustomer>();
		while(result.next()){
			String uemail=result.getString("INFI_CUST_EMAILID");
			String upassword=result.getString("INFI_CUST_PASSWORD");
			InfiBankCustomer c=new InfiBankCustomer();
		c.setEmail(uemail);
		c.setPassword(upassword);
			list.add(c);
		}
		
		Iterator itr=list.iterator();
	while(itr.hasNext()){
		InfiBankCustomer c=(InfiBankCustomer) itr.next();
		if(c.getEmail().equals(email) && c.getPassword().equals(password)){
			System.out.println("validated for custmer");

			return true;
		}
	
	}return false;
	}
	
	public boolean checkManagerLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("inside login code ");
		String sql="select * from INFI_MANAGER";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		System.out.println("preparedStatemnt");
		ResultSet result=pstmt.executeQuery();
		listMgr=new ArrayList<InfiBankManager>();
		while(result.next()){
			String memail=result.getString("INFI_MGR_EMAILID");
			String mpassword=result.getString("INFI_MGR_PASSWORD");
			InfiBankManager m=new InfiBankManager();
		m.setInfiMgrEmailid(memail);
		m.setInfiMgrPassword(mpassword);
			listMgr.add(m);
		}
		
		Iterator itr=listMgr.iterator();
	while(itr.hasNext()){
		InfiBankManager m=(InfiBankManager) itr.next();
		if(m.getInfiMgrEmailid().equals(email) && m.getInfiMgrPassword().equals(password)){
			System.out.println("validated for manager");

			return true;
		}
	
	}return false;
	}
	

}
