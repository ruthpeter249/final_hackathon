package com.ofss;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class RetrieveImage
 */
@WebServlet("/RetrieveImage")
public class RetrieveImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveImage() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        //HttpSession session = req.getSession();
        //String email = (String)session.getAttribute("ses_email");
        //String imgLen="";
        byte[] iLen = null;
        try {
        	Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/UsersDB");
            Connection conn = ds.getConnection();
            
            PreparedStatement ps ;
            ps=conn.prepareStatement("select IMAGE from IMG_TABLE where PID = 2");
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
			      iLen = rs.getBytes(1);
			      System.out.println(iLen.length);
			      }  
			ps=conn.prepareStatement("select IMAGE from IMG_TABLE where PID = 2");
			ResultSet rs2 = ps.executeQuery();
			if(rs2.next()){
				
                int len = iLen.length;
                byte [] rb = new byte[len];
                InputStream readImg = rs.getBinaryStream(1);
                int index=readImg.read(rb, 0, len);  
                System.out.println("index"+index);
                ps.close();
                resp.reset();
                resp.setContentType("image/jpg");
                resp.getOutputStream().write(rb,0,len);
                resp.getOutputStream().flush();  
                }
                }
                catch (Exception e){
                e.printStackTrace();
                }
           }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
