package com.ofss;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
//public class InsertImageToOracleDB {
//   public static void main(String args[]) throws Exception{
//      //Registering the Driver
//      DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver ());
//      //Getting the connection
//      String oracleUrl = "jdbc:oracle:thin:@localhost:1521/xe";
//      Connection con = DriverManager.getConnection(oracleUrl, "system", "password");
//      System.out.println("Connected to Oracle database.....");
//      PreparedStatement pstmt = con.prepareStatement("INSERT INTO MyTable VALUES(?,?)");
//      pstmt.setString(1, "sample image");
//      //Inserting Blob type
//      InputStream in = new FileInputStream("E:\\images\\cat.jpg");
//      pstmt.setBlob(2, in);
//      //Executing the statement
//      pstmt.execute();
//      System.out.println("Record inserted");
//   }
//}
////

@WebServlet("/listUsers")
public class InsertImageToOracleDB extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/UsersDB");
            Connection conn = ds.getConnection();
             
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO IMG_TABLE VALUES(2,?,?)");
            pstmt.setString(1, "sample image");
            //Inserting Blob type
            InputStream in = new FileInputStream("D:\\index.jpg");
            pstmt.setBlob(2, in);
            //Executing the statement
            pstmt.execute();
            System.out.println("Record inserted");
            writer.println("test inserted");
            
        } catch (NamingException ex) {
            System.err.println(ex);
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }
 
}