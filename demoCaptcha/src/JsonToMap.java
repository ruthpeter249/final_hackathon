
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;



class JsonToMap {
	public static void main(String a[]) {
		String jsonStr = "{\"name\":\"Nataraj\", \"job\":\"Programmer\"}";
		Map<String,String> resultMap = new HashMap<String,String>();
		ObjectMapper mapperObj = new ObjectMapper();
		System.out.println("Input Json: "+jsonStr);
			try {
				resultMap = mapperObj.readValue(jsonStr, 
			new TypeReference<HashMap<String,String>>(){});
				System.out.println("Output Map: "+resultMap);
	} catch (IOException e) {
		
	}
	
	}
}
	