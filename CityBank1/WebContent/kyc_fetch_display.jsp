<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>City Bank Registration</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Material Design Bootstrap -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="css/test.css">
<link rel="stylesheet" href="css/stylecity.css">
</head>
<body>
	<div class="row" >
		<div class="col-lg-2"></div>
		<div class="col-lg-8" style="padding-top:40px">
			<form action=RestServiceController method="POST">
				<input type="hidden" name="action" value="sign_up_form">

				<p class="h4 mb-4 text-center">Bank Account Creation</p>
				<div class="md-form">
					<div class="row">
						<div class="col-md-4">

							<i class="fas fa-user prefix" style="color:#7f4782"></i> <input type="text"
								id="defaultRegisterFormFirstName" class="form-control"
								placeholder="First name" name="fname" value="<c:out value='${list["fname"] }'/>"
								disabled="disabled">

						</div>


						<div class="col-md-4">

							<input type="text" id="defaultRegisterFormMiddleName"
								class="form-control" placeholder="Middle name" name="mname"
								value="<c:out value='${list["mname"] }'/>" disabled="disabled">
							${mnamemsg}
						</div>

						<div class="col-md-4">

							<input type="text" id="defaultRegisterFormLastName"
								class="form-control" placeholder="Last name" name="lname"
								value="<c:out value='${list["lname"] }'/>" disabled="disabled"/>
							${lnamemsg}
						</div>
					</div>
				</div>
				
				
			<div class="md-form">
					<div class="row">
						<div class="col-md-4">
							<i class="fas fa-user prefix" style="color:#7f4782"></i> 
							<input type="text" id="defaultRegistergender" class="form-control" placeholder="gender" name="gender" value="<c:out value='${list["gender"] }'/>" disabled="disabled"> ${gendermsg}
						</div>


						<div class="col-md-4">
							<i class="far fa-calendar-alt prefix" style="color:#7f4782" ></i> <input type="text"
								id="defaultRegisterFormBirthDate" class="form-control"
								placeholder="Birthdate" name="birthdate"
								value="<c:out value='${list["bdate"] }'/>" disabled="disabled">
							${birthdatemsg}
						</div>

						<div class="col-md-4">

							<input type="text" id="defaultRegisterFormStatus"
								class="form-control" placeholder="Status" name="status"
								value="<c:out value='${list["status"] }'/>" disabled="disabled">
							${statusmsg}
						</div>
					</div>

				</div>




<div class="md-form">
					<div class="row">
						<div class="col-md-3">

					<h5 class="pb-2">Current Address:</h5>
					<i class="fas fa-home prefix pt-5 " style="color:#7f4782"></i> <input type="text"
						id="defaultRegisterFormCurrentAddress" class="form-control mb-4"
						placeholder="Current Address" name="paddress"
						value="<c:out value='${list["cadd"] }'/>" disabled="disabled">
					${caddressmsg}
				</div>
				<div class="col-md-3">
							<h5 class="pb-2">Pincode</h5>
							 <input
								type="text" id="defaultRegisterFormPincode" class="form-control"
								placeholder="pincode" name="pincode"
								value="<c:out value='${list["pincode"] }'/>" disabled="disabled">
							${pincodemsg}
						</div>
							<div class="col-md-3">
							<h5 class="pb-2">City</h5>
							 <input
								type="text" id="defaultRegisterFormCity" class="form-control"
								placeholder="city" name="city"
								value="<c:out value='${list["city"] }'/>" disabled="disabled">
							${citymsg}
						</div>
							<div class="col-md-3">
							<h5 class="pb-2">City</h5>
							 <input
								type="text" id="defaultRegisterFormState" class="form-control"
								placeholder="state" name="state"
								value="<c:out value='${list["state"] }'/>" disabled="disabled">
							${citymsg}
						</div>
				</div>
				</div>
				
				
			<div class="md-form">
					<div class="row">
						<div class="col-md-3">

					<h5 class="pb-2">Permanent Address:</h5>
					<i class="fas fa-home prefix pt-5" style="color:#7f4782"></i> <input type="text"
						id="defaultRegisterFormCurrentAddress" class="form-control mb-4"
						placeholder="Permanent Address" name="paddress"
						value="<c:out value='${list["padd"] }'/>" disabled="disabled">
					${paddressmsg}
				</div>
				<div class="col-md-3">
							<h5 class="pb-2">Pincode</h5>
							 <input
								type="text" id="defaultRegisterFormPincode" class="form-control"
								placeholder="pincode" name="pincode"
								value="<c:out value='${list["pincode"] }'/>" disabled="disabled">
							${pincodemsg}
						</div>
							<div class="col-md-3">
							<h5 class="pb-2">City</h5>
							 <input
								type="text" id="defaultRegisterFormCity" class="form-control"
								placeholder="city" name="city"
								value="<c:out value='${list["city"] }'/>" disabled="disabled">
							${citymsg}
						</div>
							<div class="col-md-3">
							<h5 class="pb-2">State</h5>
							 <input
								type="text" id="defaultRegisterFormState" class="form-control"
								placeholder="state" name="state"
								value="<c:out value='${list["state"] }'/>" disabled="disabled">
							${citymsg}
						</div>
				</div>
				</div>
				<div class="md-form">
					<i class="fas fa-mobile-alt prefix" style="color:#7f4782"></i> <input type="text"
						id="defaultRegisterPhonePassword" class="form-control"
						name="phonenumber" value="<c:out value='${list["phnno"] }'/>"
						disabled="disabled"
						aria-describedby="defaultRegisterFormPhoneHelpBlock">
					${phonenumbermsg}
				</div>
				<hr>
				<p class="h4 mb-4 ">Your Document Details:</p>
				<div class="md-form">
					<div class="row">
						<div class="col-md-4">

							<h5 class="pb-2">Aadhar no.:</h5>
							<i class="fas fa-user prefix pt-5" style="color:#7f4782"></i> <input type="text"
								id="defaultRegisteraadharno" class="form-control"
								placeholder="aadharno" name="aadharno"
								value="<c:out value='${list["aadharno"] }'/>"
								disabled="disabled"> ${aadharnomsg}
						</div>

						<div class="col-md-4">
							<h5 class="pb-2">Pan no.:</h5>
							<i class="far fa-calendar-alt prefix pt-5" style="color:#7f4782"></i> <input
								type="text" id="defaultRegisterFormPanno" class="form-control"
								placeholder="panno" name="panno"
								value="<c:out value='${list["panno"] }'/>" disabled="disabled">
							${pannonamemsg}
						</div>

						<div class="col-md-4">
							<h5 class="pb-2">Passport no.:</h5>
							<i class="fas fa-user prefix pt-5" style="color:#7f4782"></i> <input type="text"
								id="defaultRegisterFormpassno" class="form-control"
								placeholder="passno" name="passno"
								value="<c:out value='${list["passno"] }'/>" disabled="disabled">
							${pannomsg}
						</div>
					</div>

				</div>
				<hr>
				<div class="md-form">
					<i class="fas fa-envelope prefix" style="color:#7f4782"></i> <input type="email"
						id="defaultRegisterFormEmail" class="form-control mb-4"
						placeholder="E-mail" name="email"
						value="<c:out value='${list["emailId"] }'/>" disabled="disabled">
					${emailmsg}
				</div>
				Following are your details and now set password
				<div class="md-form">
					<i class="fas fa-lock prefix" style="color:#7f4782"></i> <input type="password"
						id="defaultRegisterFormPassword" name="password"
						class="form-control" placeholder="Password"
						aria-describedby="defaultRegisterFormPasswordHelpBlock">
					${passwordmsg}
				</div>
				<small id="defaultRegisterFormPhoneHelpBlock"
					class="form-text text-muted mb-4">Minimal 8 characters
					lenght</small>


				<div class="md-form">
					<i class="fas fa-lock prefix" style="color:#7f4782"></i> <input type="password"
						id="passwdInput" class="form-control mb-4"
						placeholder="Re Enter Your Password" name="repassword">
					${repasswordmsg}
				</div>
				
					<div class="row">
					<div class="col-md-3">
							<h5 class="pb-2">Please Enter Captcha:</h5>
								<img src="CaptchaServlet"> <br/><br/>
								<input type="text" name="code">
								
							${captchamsg}
						</div>
							<div class="col-md-4 pt-4" >
						
							
							<button class="btn btn-primary text-white py-3 px-5 mt-5 " 
							name="kyc_create_account" onclick="verifyCaptcha()" >Verify Captcha!</button>
							</div>
						
						</div>

				<div class="row">
					<div class="col-md-6"></div>
					<div class="col-md-3">
						<button class="btn btn-primary my-4 text-white py-4 px-5" type="submit"
							name="kyc_create_account" >Yes ! Confirm</button>
					</div>
			</form>
			<form action="RestServiceController" method="post">
				<input type="hidden" name="action" value="traditional_way"  >
				<div>
					<button class="btn btn-danger my-4 btn-block">Go the
						Traditional way!</button>
				</div>
			</form>
		</div>
	</div>
	<div class="col-lg-2"></div>
	</div>
</body>
<%
  String captcha = (String) session.getAttribute("captcha");
  String code = (String) request.getParameter("code");

  if (captcha != null && code != null) {

    if (captcha.equals(code)) {
	  out.print("alert('Correct')");
    } else {
    	 out.print("alert('Not Correct')");
    }
  }
%>
<!-- JQuery -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</html>