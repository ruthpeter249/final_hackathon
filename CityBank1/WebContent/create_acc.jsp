<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City Bank Registration </title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
<style>

.pt-32{
	padding-top : 20rem !important;
}
</style>
</head>
<body  onload="resetSelection()">
<div class="container">
<div class="row">

		<div class="col-lg-6">
	<form class="border border-light p-5" method="post" action="ValidationController">

 <input type="hidden" name="action" value="sign_up_form"> 

    <p class="h4 mb-4 text-center">Bank Account Creation</p>
    
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormFirstName" class="form-control" name="fname" value="${fname }" placeholder="First name">   
</div>
 <div style="color:red;"> ${fnamemsg}</div>
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormMiddleName" class="form-control" placeholder="Middle name" name="mname" value="${mname }">   
	   <div style="color:red;"> ${mnamemsg}</div>
</div>

<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name"  name="lname" value="${lname }">   
	   <div style="color:red;"> ${lnamemsg}</div>
</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormCurrentAddress" class="form-control mb-4" placeholder="Current Address" name="caddress" value="${caddress }">
	   <div style="color:red;"> ${caddressmsg}</div>
	</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormPermanentAddress" class="form-control mb-4" placeholder="Permanent Address" name="paddress"value="${paddress }" >
   <div style="color:red;">  ${paddressmsg}</div>
	</div>
	
	<div class="md-form">
	
	<select id="countrySelect" size="1" onchange="makeSubmenu(this.value)" class="browser-default custom-select">
<option value="" disabled selected>Choose State</option>

<option>Andhra_Pradesh</option>
<option>Assam</option>
<option>Bihar</option>
<option>Chandigarh</option>
<option>Chhattisgarh</option>
<option>Dadra_NagarHaveli</option>
<option>Daman_Diu</option>
<option>Goa</option>
<option>Gujarat</option>
<option>Himachal_Pradesh</option>
<option>Haryana</option>
<option>Jammu_Kashmir</option>
<option>Jharkhand</option>
<option>Karnataka</option>
<option>Kerala</option>
<option>Lakshadweep</option>
<option>Maharashtra</option>

<option>Mizoram</option>
<option>Manipur</option>

<option>Madhya_Pradesh</option>
<option>Meghalaya</option>

<option>Nagaland</option>
<option>New_Delhi</option>
<option>Odisha</option>

<option>Pondicherry</option>
<option>Punjab</option>

<option>Rajasthan</option>

<option>Sikkim</option>

<option>Tamil_Nadu</option>
<option>Tripura</option>

<option>Uttarakhand</option>
<option>Uttar_Pradesh</option>

<option>West_Bengal</option>
</select>
<br/>
<br/>
<select id="citySelect" size="1"  class="browser-default custom-select">
<option value="" disabled selected>Choose City</option>
<option></option>
</select>

	</div>

	
	<div class="md-form">
	<i class="fas fa-phone prefix"></i>
   <input type="text" id="defaultRegisterPhonePassword" class="form-control"  name="phonenumber" value="${phonenumber }" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">
   <div style="color:red;">  ${phonenumbermsg}</div>
   </div>
<div class="md-form">
  <i class="fas fa-envelope prefix"></i>
    <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email" value="${email }" >
   <div style="color:red;">  ${emailmsg}</div>
</div>

<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="defaultRegisterFormPassword" name="password"  class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
   <div style="color:red;">  ${repasswordmsg}</div>
</div>
    <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">Minimal 8 characters lenght</small>


<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="passwdInput" class="form-control mb-4" placeholder="Re Enter Your Password" name="repassword">
   <div style="color:red;">  ${repasswordmsg}</div>
</div>
   
    <button class="btn btn-info my-4 btn-block" type="submit">Register</button>

    <div class="text-center">
        
	
        <hr>
        
    </div>
</form>
   <hr>

	</div>

		
	
</div>
</div>
</body>


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
<script type="text/javascript" src="js/city.js"></script>

</html>