<!DOCTYPE html>
<html lang="en">
  <head>
    <title>City Bank</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/stylecity.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


	<style>
	.modal-backdrop {
  	z-index: -1;
	}
	</style>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  	<script src="https://code.angularjs.org/1.4.0-rc.0/angular.js"></script>
    <script src="js/app.js"></script>
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" ng-app="animation">
  
    <div ng-controller="MainController">

          <div class="container" animate-hide="hide" > 
            <div class="row align-items-center justify-content-center pt-4">
              <div class="col-md-12 col-lg-7 text-center">
                <h1>Current Rates</h1>  
             
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                  <div class="jumbotron">
                      <h1 class="display-8">Fixed Deposit Rates</h1>
                      <hr class="my-4">
                      <p>
                      
                          <table style="width:100%">
                              <tr>
                                <th>Domestic General:</th>
                                <td>6.70%</td>
                                <td>6.70%</td>
                              </tr>
                              <tr>
                                  <th>Domestic General:</th>
                                  <td>6.70%</td>
                                  <td>6.70%</td>
                              </tr>
                              <tr>
                                  <th>Domestic General:</th>
                                  <td>6.70%</td>
                                  <td>6.70%</td>
                              </tr>
                            </table>
                            
                      </p>
                    </div>
                
              </div>
              <div class="col-md-6 col-lg-6">
                  <div class="jumbotron">
                      <h1 class="display-8">Saving Account rates</h1>
                      <hr class="my-4">
                      <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                  
                    </div>
              </div>
            </div>
            </div>

  
     
  


  <div class="site-wrap"  id="home-section">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
      
    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center position-relative">
          
            
            <div class="site-logo">
              <a href="index.html" class="text-black"><span class="text-primary">CITYBANK</a>
            </div>
            
  
  
            
              <nav class="site-navigation text-center ml-auto" role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  <li><a href="#home-section" class="nav-link">Home</a></li>
                  <li><a href="#about-section" class="nav-link">About</a></li>
                  <li><a href="#login-section" class="nav-link">Login</a></li>
                  
                  
                  <li><a href="#login-section" class="nav-link">Register</a></li>
                  <li><a href="" class="nav-link" ng-click="hide = !hide" >Rates</a></li>
                </ul>
              </nav>
              
              <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog ">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Register</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
                <div class="row">

                <div class="col-lg-6 pt-2">
                <form class=" " method="post" action="ValidationController">
                
                 <input type="hidden" name="action" value="bank_acc_create">
                
                    <p class="h4 text-center m-5 pb-4" >Create Your Bank Account The Traditional way!</p>
                
                    <button class="btn btn-primary my-4 btn-block text-white mx-auto" type="submit" href="create_acc.jsp">Make bank Account The Old Way</button>
                
                </form>
                   
                
                </div>
                <div style=" border-left: 1px solid black;height:100px;margin-top:30px"></div>
                <div class="col-lg-5">
                <div>
                 <img src="images/hdcclogo.PNG" >
                </div>
                
                <div>
                   <img src="images/Infinitybank.JPG" height="50%" width="50%" style="margin-left:50%;">
                </div>    
                
                        Have a verfied KYC account in another bank? Now create your account in City quickly!!
                        Verify your account using Other bank!!!
                         <a href="kycoption.jsp"><button class="btn btn-primary text-white my-4 btn-block" type="submit">Click Here for Instant Creation !</button></a>
                </form>
                </div>
                
                </div>        </div>
        
        <!-- Modal footer -->
        
      </div>
    </div>
  </div>
  
          
          
            </div>
          <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>
    
    <div class="owl-carousel slide-one-item">
    
      

      <div class="site-section-cover overlay img-bg-section" style="background-image: url('images/hero_1.jpg'); " >
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12 col-lg-7 text-center">
              <h1>CityBank</h1>  
              <p>Get to know more about the first something something something </p>
              <p><a href="#" class="btn btn-white-outline border-w-2 btn-md">Know More</a></p>
            </div>
          </div>
        </div>

      </div>

      <div class="site-section-cover overlay img-bg-section" style="background-image: url('images/hero_1.jpg'); " >
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12 col-lg-7 text-center">
              <h1>Learn more about our integrated platform</h1>      
              <p><a href="#" class="btn btn-white-outline border-w-2 btn-md">Find out more</a></p>
            </div>
          </div>
        </div>

      </div>
      

    </div>
    
    
    <div class="pb-5" style="position: relative; z-index: 8;">
    <div class="container">
      <div class="row pt-8">
       <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 ">

  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Savings account</h5>
      <h6 class="card-subtitle mb-2 text-muted">see your saving details</h6>
      <center pt-1> 
        <a href="#saving-section">
        <img class="zoom logosize" src="images/hero_1.jpg" alt="money">
      </a>
      </center>
    </div>
  </div>

   </div>
        <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 ">
        
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Fixed Deposits</h5>
              <h6 class="card-subtitle mb-2 text-muted">see your saving details</h6>
              <center> 
                <a href="#fixed-section">

                <img class="zoom logosize" src="/CitiClient/WebContent/images/hero_1.jpg" alt="money">
              </a>
                
              </center>
            </div>
          </div>

          
        </div>

        <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 ">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Netbanking</h5>
             
              <h6 class="card-subtitle mb-2 text-muted">Go to Netbanking</h6>
              <center> 
                <a href="#netbanking-section">
                    <img class="zoom logosize" src="/CitiClient/WebContent/images/hero_1.jpg" alt="money" >

                </a>
                
              </center>
              
            </img>
            </div>
          </div>
      
           </div>
        <div class="col-md-6 col-lg-3 mb-4 mb-lg-0 ">
         
          <div class="card" >
            <div class="card-body">
              <h5 class="card-title">Updates</h5>
              <h6 class="card-subtitle mb-2 text-muted">see your saving details</h6>
              <center> 
                <img class="zoom logosize" src="C:\Users\trainingfunc\Downloads\inves\inves\images\66185.png" alt="money">

              </center>
            </div>
          </div>
      
        </div>
      </div>
    </div>
  </div>

  

    <div class="site-section" id="about-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-6 mb-4">
            <figure class="block-img-video-1" data-aos="fade">
              <a href="https://vimeo.com/45830194" class="popup-vimeo">
                <span class="icon"><span class="icon-play"></span></span>
                <img src="images/hero_1.jpg" alt="Image" class="img-fluid">
              </a>
            </figure>
          </div>
          <div class="col-lg-5 ml-auto">
            <h2 class="text-primary mb-4">Welcome to Inves</h2>
            <p>Accusantium dignissimos voluptas rem consequatur blanditiis error ratione illo sit quasi ut, praesentium magnam, pariatur quae, necessitatibus</p>
            <p>Dolor, eligendi voluptate ducimus itaque esse autem perspiciatis sint! Recusandae dolor aliquid inventore sit,</p>
            <p>Recusandae dolor aliquid inventore sit, maiores quisquam nostrum quaerat dolorum error rerum</p>
            <p><a href="#">Read More</a></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-4 col-lg-0 col-lg-3">
            <div class="block-counter-1">
              <span class="number"><span data-number="15">0</span></span>
              <span class="caption">Year of Experience</span>
            </div>
          </div>
          <div class="col-md-6 mb-4 col-lg-0 col-lg-3">
            <div class="block-counter-1">
              <span class="number"><span data-number="392">0</span></span>
              <span class="caption">Number of Engineers</span>
            </div>
          </div>
          <div class="col-md-6 mb-4 col-lg-0 col-lg-3">
            <div class="block-counter-1">
              <span class="number"><span data-number="3293">0</span></span>
              <span class="caption">Number of Employees</span>
            </div>
          </div>
          <div class="col-md-6 mb-4 col-lg-0 col-lg-3">
            <div class="block-counter-1">
              <span class="number"><span data-number="1212">0</span></span>
              <span class="caption">Number of Golds</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    


    <div class="site-section" id="login-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="block-heading-1">
              <h2>Savings Accounts</h2>
            </div>
          </div>
          <div class="row">
            <div class="col pb-4">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nihil sed sit, nobis odio perspiciatis voluptas cumque non ipsa incidunt voluptates. Nesciunt quibusdam officia minus corporis consequuntur magni esse omnis.
              
            </div>
          </div>
          <div class="row">
              <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                <div class="block-team-member-1 text-center rounded">
                 
                  <h3 class="font-size-20">EXISTING ACCOUNT</h3>
                  <p class="px-3 mb-3">Proceed directly by entering your loginId and password </p>

                  <div class="row">

                    <div class="col-md-2"></div>
                  
                    <div class="col-md-8">
                        <form action="#" method="post">
                            <div class="form-group row">
                              <div class="col-md-12 mb-4 mb-lg-0">
                                <input type="text" class="form-control" placeholder="Email">
                              </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Password">
                              </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-6 mx-auto">
                                  <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
                                </div>
                              </div>
                         
                          </form>
                     

                    </div>
                    <div class="col-md-2"></div>

                  </div>
                   </div>
              </div>
    
              <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                  <div class="block-team-member-1 text-center rounded">
                   
                    <h3 class="font-size-20">CREATE NEW ACCOUNT</h3>
                    <p class="px-3 mb-3">Fill in your details to register to our banking portal ! Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam officia, recusandae fugit beatae soluta tenetur amet deleniti obcaecati ab nobis minima quidem tempora unde animi autem illum esse maiores laudantium.</p>
              
                    <div class="form-group row">
                        <div class="col-md-6 mx-auto pt-5">
                       	 <button type="button" class="btn btn-primary text-white py-3 px-5" data-toggle="modal" data-target="#myModal">
    Register
  </button>	 
                        </div>
                      </div>
                  </div>
                </div>
    
              
            </div>
        </div>
      </div>
    </div>

    <div class="site-section block-13 overlay bg-image" id="testimonials-section" data-aos="fade" style="background-image: url('images/hero_1.jpg');">
    </div>

   
      <div class="site-section" id="fixed-section">
          <div class="container">
            <div class="row">
              <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="block-heading-1">
                  <h2>Fixed Deposits</h2>
                </div>
              </div>
              <div class="row">
                <div class="col pb-4">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nihil sed sit, nobis odio perspiciatis voluptas cumque non ipsa incidunt voluptates. Nesciunt quibusdam officia minus corporis consequuntur magni esse omnis.
                  
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                    <div class="block-team-member-1 text-center rounded">
                     
                      <h3 class="font-size-20">EXISTING ACCOUNT</h3>
                      <p class="px-3 mb-3">Proceed directly by entering your loginId and password </p>
    
                      <div class="row">
    
                        <div class="col-md-2"></div>
                      
                        <div class="col-md-8">
                            <form action="#" method="post">
                                <div class="form-group row">
                                  <div class="col-md-12 mb-4 mb-lg-0">
                                    <input type="text" class="form-control" placeholder="Email">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Password">
                                  </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-md-6 mx-auto">
                                      <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
                                    </div>
                                  </div>
                             
                              </form>
                         
    
                        </div>
                        <div class="col-md-2"></div>
    
                      </div>
                       </div>
                  </div>
        
                  <div class="col-lg-6 col-md-6 mb-4 mb-lg-0" data-aos="fade-up">
                      <div class="block-team-member-1 text-center rounded">
                       
                        <h3 class="font-size-20">CREATE NEW ACCOUNT</h3>
                        <p class="px-3 mb-3">Fill in your details to register to our banking portal ! Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam officia, recusandae fugit beatae soluta tenetur amet deleniti obcaecati ab nobis minima quidem tempora unde animi autem illum esse maiores laudantium.</p>
                        <div class="form-group row">
                            <div class="col-md-6 mx-auto">
                              <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Register">
                            </div>
                          </div>
                      </div>
                    </div>
        
                  
                </div>
            </div>
          </div>
        </div>
      

    <div class="site-section block-13 overlay bg-image" id="testimonials-section" data-aos="fade" style="background-image: url('images/hero_1.jpg');">
    </div>

    
    <div class="site-section" id="netbanking-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 mb-5 mb-lg-0">
              <div class="block-heading-1">
                <h2>Netbanking</h2>
              </div>
            </div>
            <div class="row">
              <div class="col pb-4">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nihil sed sit, nobis odio perspiciatis voluptas cumque non ipsa incidunt voluptates. Nesciunt quibusdam officia minus corporis consequuntur magni esse omnis.
                
              </div>
            </div>
          </div>

            <div class="row">

              <!-- <div class="col-md-3"></div> -->
                <div class="col-lg-6 col-md-6 mb-lg-0 mx-auto " data-aos="fade-up">
                  <div class="block-team-member-1 text-center rounded">
                   
                    <h3 class="font-size-20">EXISTING ACCOUNT</h3>
                    <p class="px-3 mb-3">Proceed by entering your loginId and password </p>

                    <div class="row">
    
                        <div class="col-md-2"></div>
                      
                        <div class="col-md-8">
                            <form action="#" method="post">
                                <div class="form-group row">
                                  <div class="col-md-12 mb-4 mb-lg-0">
                                    <input type="text" class="form-control" placeholder="Email">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Password">
                                  </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-md-6 mx-auto">
                                      <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Login">
                                    </div>
                                  </div>
                             
                              </form>
                         
    
                        </div>
                        <div class="col-md-2"></div>
    
                      </div>
  
                     </div>
                </div>
                <!-- <div class="col-md-3"></div> -->
           
              </div>
          </div>
        </div>
      </div>
  

      <div class="site-section bg-light" id="services-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12">
            <div class="block-heading-1">
              <h2>Our Services</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-signal d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Business Analytics</h3>
            <p>Accusantium dignissimos voluptas rem consequatur ratione illo sit quasi.</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-anchor d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Investment Solutions</h3>
            <p>Praesentium magnam pariatur quae necessitatibus eligendi voluptate ducimus.</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-magnet d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Individual Approach</h3>
            <p>Accusantium dignissimos voluptas rem consequatur ratione illo sit quasi.</p>
          </div>

          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-briefcase d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Business Analytics</h3>
            <p>Accusantium dignissimos voluptas rem consequatur ratione illo sit quasi.</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-money d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Investment Solutions</h3>
            <p>Praesentium magnam pariatur quae necessitatibus eligendi voluptate ducimus.</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4">
            <span class="icon-umbrella d-block mb-3 display-3 text-secondary"></span>
            <h3 class="text-primary h4 mb-2">Individual Approach</h3>
            <p>Accusantium dignissimos voluptas rem consequatur ratione illo sit quasi.</p>
          </div>

        </div>
      </div>
    </div>



    


    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-8">
                <h2 class="footer-heading mb-4">About Us</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque facere laudantium magnam voluptatum autem. Amet aliquid nesciunt veritatis aliquam.</p>
              </div>
              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">Features</h2>
                <ul class="list-unstyled">
                  <li><a href="#">About</a></li>
                  <li><a href="#">Press Releases</a></li>
                  <li><a href="#">Testimonials</a></li>
                  <li><a href="#">Terms of Service</a></li>
                  <li><a href="#">Privacy</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
              
            </div>
          </div>
          <div class="col-md-4 ml-auto">

            <div class="mb-5">
              <div class="mb-5">
                <h2 class="footer-heading mb-4">Some Paragraph</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat nostrum libero iusto dolorum vero atque aliquid.</p>
              </div>
              <h2 class="footer-heading mb-4">Subscribe to Newsletter</h2>
              <form action="#" method="post">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary text-white" type="button" id="button-addon2">Subscribe</button>
                  </div>
                </div>
              </div>


              <h2 class="footer-heading mb-4">Follow Us</h2>
                <a href="#about-section" class="smoothscroll pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </form>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
            </div>
          </div>
          
        </div>
      </div>
    </footer>

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
  
      
  </body>
</html>