<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">
<title>Login Form</title>
<link
  rel="stylesheet"
  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
  crossorigin="anonymous"
/>


</head>
<body>

 <!-- Sing in  Form -->
        <section class="sign-in pt-16">
            <div class="container ">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="images/signin.jpeg" alt="sing up image"></figure>
                        <a href="create_acc.jsp" class="signup-image-link">Create an account</a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title">Now get your account created INSTANLY on a Single Click!</h2>
               
                        <form method="POST" class="register-form" id="login-form" action="RestServiceController"  >
                        <input type="hidden" name="action" value="kyc"> 
                            <div class="form-group">
                            <div style="font-weight:bold">
                            Please Enter Your Aadhar No.:
                            </div>
                                <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="aadharno" maxlength="12" value="110022001120" />
      <br/>
                            </div>
                           
                               <div class="form-group">
                            <div style="font-weight:bold">
                            Please Select a bank from the following where your KYC is verified :
                            </div>
                              <br/>
                            
                               <select name="bankOption" class="form-control">
										<option>Select from following options: </option>
										<option>HDCC Bank</option>
										<option>Infinty Bank</option>
										
										</select>
										
                            </div>
                           
                            <div class="form-group form-button">
                                <input type="submit" name="signin" id="signin" class="form-submit" value="Submit"/>
                            </div>
                        </form>
                     
                    </div>
                </div>
            </div>
        </section>
</body>
<!-- JS -->
    <script src="resources/jquery.min.js"></script>
    <script src="js/main.js"></script>

</html>