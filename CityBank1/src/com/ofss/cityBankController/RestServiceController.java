package com.ofss.cityBankController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.ofss.cityBankBeans.CityBankCustomer;
import com.ofss.cityBankDAO.DBAccount;
import com.ofss.resources.RestJavaNetClient;
import com.ofss.resources.RestJavaNetInfinityClient;




@WebServlet("/RestServiceController")
public class RestServiceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 Map<String,String> resultMap;
     
   HttpSession session;
   DataSource ds;
	Connection con;
	ArrayList<CityBankCustomer> list;
	@Override
	public void init(ServletConfig config) throws ServletException {
	try {
	InitialContext initcontext = new InitialContext();
	Context c = (Context) initcontext.lookup("java:comp/env");
	ds =(DataSource) c.lookup("jdbc/UsersDB");

	} catch (NamingException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if(action.equals("traditional_way")) {
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
		/*if(action.equals("kyc")) {

			
				String aadharno=request.getParameter("aadharno");
				String bankOption=request.getParameter("bankOption");
				
				if(bankOption.equals("-------------------------------------------------------------------------------")&& aadharno.equals("")) {
					request.setAttribute("msg", "Please Enter Aadhar no");
					request.setAttribute("msg2", "Please Select a bank");
					request.getRequestDispatcher("kycoption.jsp").forward(request, response);
				}
				System.out.println(bankOption);
				if(bankOption.equals("HDCC Bank")) {
					 RestJavaNetClient restobj=new RestJavaNetClient();
					 Map<String,String> resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		            
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}if(bankOption.equals("Infinty Bank")) {
					 RestJavaNetInfinityClient restobj=new RestJavaNetInfinityClient();
					 Map<String,String> resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}
            
      
        }*/
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//doGet(request, response);
		DBAccount account=new DBAccount();
	
		String action=request.getParameter("action");
		CityBankCustomer customer=new CityBankCustomer();
		session=request.getSession();
		try {
			con = ds.getConnection();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}

			account.setConnection(con);
		
		
		if(action.equals("kyc_verify_option")) {
			request.getRequestDispatcher("kycoption.jsp").forward(request, response);
		}
		if(action.equals("traditional_way")) {
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
		if(action.equals("kyc")) {

			
				String aadharno=request.getParameter("aadharno");
				String bankOption=request.getParameter("bankOption");
				
				if(bankOption.equals("-------------------------------------------------------------------------------")&& aadharno.equals("")) {
					request.setAttribute("msg", "Please Enter Aadhar no");
					request.setAttribute("msg2", "Please Select a bank");
					request.getRequestDispatcher("kycoption.jsp").forward(request, response);
				}
				System.out.println(bankOption);
				if(bankOption.equals("HDCC Bank")) {
					 RestJavaNetClient restobj=new RestJavaNetClient();
					resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		        	
		    		System.out.println("Result Map valyues is "+resultMap);
		    		
		    		/*resultMap.entrySet()
		    		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));*/
		    		
		    		//System.out.println("FNAME="+resultMap.get("fname"));
		    		
		    		
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}
				if(bankOption.equals("Infinty Bank")) {
					 RestJavaNetInfinityClient restobj=new RestJavaNetInfinityClient();
			 resultMap=restobj.fetchKYC(aadharno);
		            request.setAttribute("list", resultMap);
		            resultMap.entrySet()
		    		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));
		    		
		            request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
				}
            
	}	if(action.equals("sign_up_form"))
	{
		
		System.out.println("inside signup form ");
		String fname=resultMap.get("fname");
		String mname=resultMap.get("mname");
		String lname=resultMap.get("lname");
		String gender=resultMap.get("gender");
		String birthdate=resultMap.get("bdate");
		String status=resultMap.get("status");
		String caddress=resultMap.get("cadd");
		String paddress=resultMap.get("padd");
		String city=resultMap.get("city");
		String state=resultMap.get("state");
		String phonenumber=resultMap.get("phnno");
		String aadharno=resultMap.get("aadharno");
		String panno=resultMap.get("panno");
		String passno=resultMap.get("passno");
		String email=resultMap.get("emailId");
		String password=request.getParameter("password");
		String repassword=request.getParameter("repassword");
		String pincode=resultMap.get("pincode");
		System.out.println("ResultMap Values \n="+resultMap);
		resultMap.entrySet()
		  .forEach(e -> System.out.println("key"+e.getKey()+"\t "+e.getValue()));
		System.out.println();
	

	
	
		boolean status1=customer.validate(password,repassword);
		if(status1==true){
			System.out.println("Inside status true ");
			
			//request.setAttribute("email", email);	
			//request.setAttribute("msg", "Registration Successful,Please login again");
			request.getRequestDispatcher("test.jsp").forward(request, response);
			  
		
			System.out.println("Going in db");
		try {
			account.insertBankUserDetails(fname,mname,lname,gender,birthdate,status,caddress,paddress,phonenumber,aadharno,panno,passno,email,password,repassword,city,state,pincode);
			//account.insertCustomer( fname,  lname,  email,  password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Inserted one city bank customer details");
			
			
		
			
			
			
			request.getRequestDispatcher("test.jsp").forward(request, response);
			
		}else {
			
			
			request.setAttribute("fname", resultMap.get("fname"));
			request.getRequestDispatcher("kyc_fetch_display.jsp").forward(request, response);
		}



		


}
}
}
