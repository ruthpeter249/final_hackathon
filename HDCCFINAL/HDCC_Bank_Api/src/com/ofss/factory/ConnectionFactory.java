package com.ofss.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionFactory {
	/*private String driver="oracle.jdbc.driver.OracleDriver";
	private String url="jdbc:oracle:thin:@10.180.40.159:1522:HDCCDB";
	private String user="system";
	private String password="oracle";*/
	 private static DataSource ds;
	
	
	static{
		try {
			 InitialContext initcontext = new InitialContext();
				Context c = (Context) initcontext.lookup("java:comp/env");
				ds =(DataSource) c.lookup("jdbc/UsersDB");
					System.out.println("Connection Success");
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				System.out.println("Connection Failed");

				System.out.println("Exception: "+e.getMessage());
			}
	}

			public static DataSource getDataSource() {
				return ds;

	}
	}