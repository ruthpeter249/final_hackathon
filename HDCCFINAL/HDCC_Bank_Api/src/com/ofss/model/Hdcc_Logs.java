package com.ofss.model;

public class Hdcc_Logs {

	private String hdccNumber;
	private String hdccBranchName;
	private String hdccFirstname;
	private String hdccLogTimestamp;
	private String hdccLogAttempt;
	public String getHdccNumber() {
		return hdccNumber;
	}
	public void setHdccNumber(String hdccNumber) {
		this.hdccNumber = hdccNumber;
	}
	public String getHdccBranchName() {
		return hdccBranchName;
	}
	public void setHdccBranchName(String hdccBranchName) {
		this.hdccBranchName = hdccBranchName;
	}
	public String getHdccFirstname() {
		return hdccFirstname;
	}
	public void setHdccFirstname(String hdccFirstname) {
		this.hdccFirstname = hdccFirstname;
	}
	public String getHdccLogTimestamp() {
		return hdccLogTimestamp;
	}
	public void setHdccLogTimestamp(String hdccLogTimestamp) {
		this.hdccLogTimestamp = hdccLogTimestamp;
	}
	public String getHdccLogAttempt() {
		return hdccLogAttempt;
	}
	public void setHdccLogAttempt(String hdccLogAttempt) {
		this.hdccLogAttempt = hdccLogAttempt;
	}
	
}
