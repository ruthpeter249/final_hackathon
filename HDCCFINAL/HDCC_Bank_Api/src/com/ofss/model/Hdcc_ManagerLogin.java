package com.ofss.model;

public class Hdcc_ManagerLogin {
	private String email;
	private String pass;
	private String Managerid;
	private String Mname;
	
	public String getManagerid() {
		return Managerid;
	}
	public void setManagerid(String managerid) {
		Managerid = managerid;
	}
	public String getMname() {
		return Mname;
	}
	public void setMname(String mname) {
		Mname = mname;
	}

	private String emailmsg = " ";
	private String passmsg = " ";
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getEmailmsg() {
		return emailmsg;
	}
	public void setEmailmsg(String emailmsg) {
		this.emailmsg = emailmsg;
	}
	public String getPassmsg() {
		return passmsg;
	}
	public void setPassmsg(String passmsg) {
		this.passmsg = passmsg;
	}
	
	public boolean validateUser(String email,String pass)
	{
		if(email.equals(""))
			emailmsg="Please enter email";
		else if(!(email.matches("\\w+\\@\\w+\\.\\w{2,4}")))
			emailmsg="Email not Valid,Please Enter Valid email";
		
		
		if(pass.equals(""))
			passmsg="Please Enter password";
		  String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		 if(!(pass.matches(pattern)))
			passmsg="a digit must occur at least once\r <br/>" + 
					"a lower case letter must occur at least once\r <br/>" + 
					"an upper case letter must occur at least once\r <br/>" + 
					" a special character must occur at least once\r <br/>" + 
					" no whitespace allowed in the entire string\r <br/>" + 
					"at least 8 characters";
		 
		 if(emailmsg.equals("") && passmsg.equals(""))
		 {
			 System.out.println("emailmsg"+emailmsg);
			 

						return true;
					}
					
					return false;
		}
		
}
