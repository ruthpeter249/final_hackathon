package com.ofss.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.ofss.model.Hdcc_Customer;
import com.ofss.model.Hdcc_CustomerLogin;
import com.ofss.model.Hdcc_Logs;
import com.ofss.model.Hdcc_ManagerLogin;


public class DBAccount {
	Connection con;
	PreparedStatement pstmt;
	ArrayList<Hdcc_CustomerLogin> list;
	ArrayList<Hdcc_Customer> list1;
	ArrayList<Hdcc_ManagerLogin> listmgr;
	ArrayList<Hdcc_Logs> logs;
	
	public void setConnection(Connection con) {
	this.con = con;
	}
	
	public boolean checkCustomerLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("inside login code ");
		String sql="select * from HDCC_CUSTOMER";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		ResultSet result=pstmt.executeQuery();
		list=new ArrayList<Hdcc_CustomerLogin>();
		while(result.next()){
			String uemail=result.getString("HDCC_CUST_EMAILID");
			String upassword=result.getString("HDCC_CUST_PASSWORD");
			Hdcc_CustomerLogin c=new Hdcc_CustomerLogin();
		c.setEmail(uemail);
		c.setPass(upassword);
			list.add(c);
		}
		Iterator itr=list.iterator();
	while(itr.hasNext()){
		Hdcc_CustomerLogin c=(Hdcc_CustomerLogin) itr.next();
		if(c.getEmail().equals(email) && c.getPass().equals(password)){
			return true;
		}
	
	}return false;
	}
	
	
	public boolean checkManagerLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("inside login code ");
		String sql="select * from HDCC_MANAGER";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		System.out.println("preparedStatemnt");
		ResultSet result=pstmt.executeQuery();
		listmgr=new ArrayList<Hdcc_ManagerLogin>();
		while(result.next()){
			String memail=result.getString("HDCC_MGR_EMAILID");
			String mpassword=result.getString("HDCC_MGR_PASSWORD");
			Hdcc_ManagerLogin m=new Hdcc_ManagerLogin();
		m.setEmail(memail);
		m.setPass(mpassword);
			listmgr.add(m);
		}
		
		Iterator itr=listmgr.iterator();
	while(itr.hasNext()){
		Hdcc_ManagerLogin m=(Hdcc_ManagerLogin) itr.next();
		if(m.getEmail().equals(email) && m.getPass().equals(password)){
			System.out.println("validated for manager");

			return true;
		}
	
	}return false;
	}
	
	public ArrayList<Hdcc_Logs> getLogs() throws SQLException {
		System.out.println("Inside getLogs()");
		logs = new ArrayList<Hdcc_Logs>();
		String sql = "select * from HDCC_LOG_VIEW";
		PreparedStatement pstmt = con.prepareStatement(sql);
		ResultSet rst =pstmt.executeQuery();
		while(rst.next()){
		Hdcc_Logs l = new Hdcc_Logs();
		l.setHdccNumber(rst.getString("HDCC_IFSCNUMBER"));
		l.setHdccBranchName(rst.getString("HDCC_BRANCHNAME"));
		l.setHdccFirstname(rst.getString("HDCC_CUST_FIRSTNAME"));
		l.setHdccLogAttempt(rst.getString("HDCC_LOG_ATTEMPT"));
		l.setHdccLogTimestamp(rst.getString("HDCC_LOG_TIMESTAMP"));
		logs.add(l);
		System.out.println(logs);
		System.out.println(rst.getString("HDCC_IFSCNUMBER"));
		l=null;
		}
		return logs;
	}
	
	public String getManagerName(String email) throws SQLException {
		
		System.out.println("Inside getManagerName()");
		Hdcc_ManagerLogin manager = new Hdcc_ManagerLogin();
		
		String sql = "select HDCC_MNAME from HDCC_MANAGER WHERE HDCC_MGR_EMAILID = ?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1,email);
		ResultSet rst =pstmt.executeQuery();
		while (rst.next()){
			manager.setMname(rst.getString("HDCC_MNAME")); 
		System.out.println(rst.getString("HDCC_MNAME"));
		System.out.println(manager.getMname());
		}
		
		return manager.getMname(); 
		
	}
	
	
	
	public ArrayList<Hdcc_Customer> getBlackListedCustomers() throws SQLException {
		System.out.println("Inside getBlackListedCustomers()");
		list1 = new ArrayList<Hdcc_Customer>();
		String sql = "select HDCC_CUST_FIRSTNAME,HDCC_CUSTID,HDCC_CUST_EMAILID,HDCC_CUST_LASTLOGIN,HDCC_KYC from HDCC_CUSTOMER where HDCC_CUST_BLACKLIST='Y'";
		PreparedStatement pstmt = con.prepareStatement(sql);
		ResultSet rst =pstmt.executeQuery();
		while(rst.next()){
		Hdcc_Customer bl = new Hdcc_Customer();
		bl.setFname(rst.getString("HDCC_CUST_FIRSTNAME"));
		bl.setCustID(rst.getString("HDCC_CUSTID"));
		bl.setEmailId(rst.getString("HDCC_CUST_EMAILID"));
		bl.setLastlogin(rst.getString("HDCC_CUST_LASTLOGIN"));
		bl.setKycCheck(rst.getString("HDCC_KYC"));
		
		System.out.println(rst.getString("HDCC_CUST_FIRSTNAME"));
		
		list1.add(bl);
		bl=null;
		}
		return list1;
	}
	
	

	
	
	

}


