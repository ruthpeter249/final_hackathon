package com.ofss.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.math.BigDecimal;

import javax.sql.DataSource;

import com.ofss.factory.ConnectionFactory;
import com.ofss.model.Hdcc_Account;
import com.ofss.model.Hdcc_Customer;

public class Hdcc_CustomerDAO {
private Connection connection;
private PreparedStatement preparedStatement;
private CallableStatement callablestatement;
private PreparedStatement lockstatement;
private String sql,sql1;
private ResultSet resultSet;
private List<Hdcc_Customer> hdcc_cust_List = new ArrayList<Hdcc_Customer>();
private DataSource datasource = ConnectionFactory.getDataSource();


	public Hdcc_Customer getHdccCustomer(String hdcc_aadharno)
    	{
		Hdcc_Customer hdcc_cust = new Hdcc_Customer();

    		try {
    				System.out.println("in getHdcc_Customers() :: Hdcc_CustomerDAO");
    				sql = "select * from HDCC_CUSTOMER where HDCC_CUST_AADHARNO = ? and HDCC_KYC='Y'";
    				connection = datasource.getConnection();
    				System.out.println("got connection");
    				if (connection != null) {
    					preparedStatement = connection.prepareStatement(sql);
        				preparedStatement.setString(1, hdcc_aadharno);
        	            resultSet = preparedStatement.executeQuery();
    					System.out.println("inside report code");
   					 callablestatement = connection.prepareCall("{call HDCC_LOGGER(?,?)}");
   					callablestatement.setString(1, hdcc_aadharno);
   					
   					String str;
    					if(!resultSet.isBeforeFirst() && resultSet.getRow() == 0) {
    						str="failed";
    					}else {
    						str="success";
        						
    					}
    					callablestatement.setString(2,str);
    					callablestatement.executeUpdate();
    	   				
    					if (resultSet.next()) {
    						hdcc_cust.setCustID(resultSet.getString("HDCC_CUSTID"));
    						hdcc_cust.setFname(resultSet.getString("HDCC_CUST_FIRSTNAME"));
							hdcc_cust.setMname(resultSet.getString("HDCC_CUST_MIDDLENAME"));
							hdcc_cust.setLname(resultSet.getString("HDCC_CUST_LASTNAME"));
							hdcc_cust.setPadd(resultSet.getString("HDCC_CUST_PERMANENTADDRESS"));
							hdcc_cust.setCadd(resultSet.getString("HDCC_CUST_CURRENTADDRESS"));
							hdcc_cust.setPincode(resultSet.getString("HDCC_CUST_PPINCODE"));
							hdcc_cust.setCity(resultSet.getString("HDCC_CUST_PCITY"));
							hdcc_cust.setState(resultSet.getString("HDCC_CUST_PSTATE"));
							hdcc_cust.setEmailId(resultSet.getString("HDCC_CUST_EMAILID"));
							hdcc_cust.setOccupation(resultSet.getString("HDCC_CUST_OCCUPATION"));
							hdcc_cust.setBdate(resultSet.getString("HDCC_CUST_BIRTHDATE"));
							hdcc_cust.setAadharno(resultSet.getString("HDCC_CUST_AADHARNO"));
							hdcc_cust.setPanno(resultSet.getString("HDCC_CUST_PANNO"));
							hdcc_cust.setPassno(resultSet.getString("HDCC_CUST_PPNO"));
							hdcc_cust.setKycCheck(resultSet.getString("HDCC_KYC"));
							hdcc_cust.setIfsc(resultSet.getString("HDCC_IFSCNUMBER")); 
							hdcc_cust.setPhnno(resultSet.getString("HDCC_PHONE"));
							hdcc_cust.setGender(resultSet.getString("HDCC_GENDER"));
							hdcc_cust.setStatus(resultSet.getString("HDCC_MARITAL_STATUS"));
							
							return hdcc_cust;
    					}
    					}
    				
    		}
    		catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		finally {
    			try {
    				connection.close();
    			}
    			catch (SQLException e) {
    				System.out.println("Exception :: " + e.getMessage());
    			}
    		}
    		return null;
    	}
    
	
	
    public String testConnection() throws SQLException
    {
		connection = datasource.getConnection();
    	if(connection!=null)
    	{
    		return("Connection success from HdccBankDAO");
    	}
    	else
    		return("Connection failed!");
    }
     
     
    public List<Hdcc_Customer> getAllHdccCustomer()
    {
    	try {
    		System.out.println("in getAllHdccCustomers() :: Hdcc_CustomerDAO");
    		sql="select * from  HDCC_CUSTOMER where HDCC_KYC='Y'";

    		connection = datasource.getConnection();
			
    		if(connection!=null)
    		{
    			preparedStatement=connection.prepareStatement(sql);
    			resultSet=preparedStatement.executeQuery();
    			
    			while(resultSet.next())
    			{
					Hdcc_Customer hdcc_cust = new Hdcc_Customer();
					hdcc_cust.setCustID(resultSet.getString("HDCC_CUSTID"));
					hdcc_cust.setFname(resultSet.getString("HDCC_CUST_FIRSTNAME"));
					hdcc_cust.setMname(resultSet.getString("HDCC_CUST_MIDDLENAME"));
					hdcc_cust.setLname(resultSet.getString("HDCC_CUST_LASTNAME"));
					hdcc_cust.setPadd(resultSet.getString("HDCC_CUST_PERMANENTADDRESS"));
					hdcc_cust.setCadd(resultSet.getString("HDCC_CUST_CURRENTADDRESS"));
					hdcc_cust.setPincode(resultSet.getString("HDCC_CUST_PPINCODE"));
					hdcc_cust.setCity(resultSet.getString("HDCC_CUST_PCITY"));
					hdcc_cust.setState(resultSet.getString("HDCC_CUST_PSTATE"));
					hdcc_cust.setEmailId(resultSet.getString("HDCC_CUST_EMAILID"));
					hdcc_cust.setOccupation(resultSet.getString("HDCC_CUST_OCCUPATION"));
					hdcc_cust.setBdate(resultSet.getString("HDCC_CUST_BIRTHDATE"));
					hdcc_cust.setAadharno(resultSet.getString("HDCC_CUST_AADHARNO"));
					hdcc_cust.setPanno(resultSet.getString("HDCC_CUST_PANNO"));
					hdcc_cust.setPassno(resultSet.getString("HDCC_CUST_PPNO"));
					hdcc_cust.setKycCheck(resultSet.getString("HDCC_KYC"));
					hdcc_cust.setIfsc(resultSet.getString("HDCC_IFSCNUMBER"));
					hdcc_cust.setPhnno(resultSet.getString("HDCC_PHONE"));
					hdcc_cust.setGender(resultSet.getString("HDCC_GENDER"));
					hdcc_cust.setStatus(resultSet.getString("HDCC_MARITAL_STATUS"));

					System.out.println(hdcc_cust);
					hdcc_cust_List.add(hdcc_cust);
    			}
    			return hdcc_cust_List;
    		}


    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		System.out.println("Error message: "+e.getMessage());
    	}
    	finally {
    		try {
    			connection.close();
    		}
    		catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    	return null;
    }
    
    
    
    public Hdcc_Account getHdccAccount(String hdcc_aadharno)
	{	
		try {
			System.out.println("in getHdccAccount() :: Hdcc_CustomerDAO");
			sql = "select * from HDCC_CUST_ACC where HDCC_CUST_AADHARNO = ?";
			connection = datasource.getConnection();
			if (connection != null) {
				preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, hdcc_aadharno);
	           resultSet = preparedStatement.executeQuery();
				if (resultSet.next()) {
					Hdcc_Account hdcc_cust = new Hdcc_Account();
					hdcc_cust.setAadharno(resultSet.getString("HDCC_CUST_AADHARNO"));
					hdcc_cust.setAccbalance(resultSet.getString("HDCC_ACCOUNTBALANCE"));
					hdcc_cust.setAcccreatedate(resultSet.getString("HDCC_ACCOUNTCREATEDATE"));
					hdcc_cust.setAccno(resultSet.getString("HDCC_ACCNO"));
					hdcc_cust.setAcctype(resultSet.getString("HDCC_ACCTYPE"));
					hdcc_cust.setIfsc(resultSet.getString("HDCC_IFSCNUMBER"));
					
					return hdcc_cust;
				}
			}
		}
				catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		finally {
			try {
				connection.close();
			} 
			catch (SQLException e) {
				System.out.println("Exception :: " + e.getMessage());
			}
		}
		return null;
		}
    
    public int withdrawFromHdccCustomer(String hdcc_aadharno,String amount)
	{	Hdcc_Account hdcc_acc = new Hdcc_Account();
	  int updatecount=-1;
	  try {
			System.out.println("in withdrawFromHdccCustomer() :: Hdcc_CustomerDAO");
			sql1 = "select HDCC_ACC_AADHARNO,HDCC_ACCOUNTBALANCE from  HDCC_ACCOUNT where HDCC_ACC_AADHARNO= ?";
			connection = datasource.getConnection();

			if (connection != null) {
				lockstatement = connection.prepareStatement(sql1);
                lockstatement.setString(1, hdcc_aadharno);
				resultSet = lockstatement.executeQuery();
				if (resultSet.next()) {
					System.out.println("inside resultset");
					hdcc_acc.setAccbalance(resultSet.getString("HDCC_ACCOUNTBALANCE"));
					//hdcc_acc.setAccno(resultSet.getString("HDCC_ACCNO"));
					//hdcc_acc.setAcctype(resultSet.getString("HDCC_ACCTYPE"));
					hdcc_acc.setAadharno(resultSet.getString("HDCC_ACC_AADHARNO"));
					//hdcc_acc.setAcccreatedate(resultSet.getString("HDCC_ACCOUNTCREATEDATE"));
					//hdcc_acc.setIfsc(resultSet.getString("HDCC_IFSCNUMBER"));					
				}
				BigDecimal  newamount  = new BigDecimal(amount);
				BigDecimal  accbalance  = new BigDecimal(hdcc_acc.getAccbalance());
				if(accbalance.compareTo(newamount) != -1)
				{
					BigDecimal diff = accbalance.subtract(newamount);
					sql = "UPDATE HDCC_ACCOUNT SET HDCC_ACCOUNTBALANCE = ? WHERE HDCC_ACC_AADHARNO = ? ";
					preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setBigDecimal(1,diff);
					preparedStatement.setString(2, hdcc_aadharno);
					updatecount = preparedStatement.executeUpdate();
					return updatecount;
				}
				else
				{
					System.out.println("error");
					return -1;
				}
				}
	  		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	  			}
	  			finally {
	  				try {
	  					connection.close();
	  				} 
	  				catch (SQLException e) {
	  					System.out.println("Exception :: " + e.getMessage());
	  				}
	  			}
	  				return 0;
			}	
    
    public int depositInHdccCustomer(String hdcc_aadharno,String amount)
	{	Hdcc_Account hdcc_acc = new Hdcc_Account();
	  int updatecount=-1;
	  try {
			System.out.println("in depositeInHdccCustomer() :: Hdcc_CustomerDAO");
			sql1 = "select HDCC_ACC_AADHARNO,HDCC_ACCOUNTBALANCE from  HDCC_ACCOUNT where HDCC_ACC_AADHARNO= ?";
			connection = datasource.getConnection();
			if (connection != null) {
				lockstatement = connection.prepareStatement(sql1);
                lockstatement.setString(1, hdcc_aadharno);
				resultSet = lockstatement.executeQuery();
				if (resultSet.next()) {
					System.out.println("inside resultset");
					hdcc_acc.setAccbalance(resultSet.getString("HDCC_ACCOUNTBALANCE"));
					//hdcc_acc.setAccno(resultSet.getString("HDCC_ACCNO"));
					//hdcc_acc.setAcctype(resultSet.getString("HDCC_ACCTYPE"));
					hdcc_acc.setAadharno(resultSet.getString("HDCC_ACC_AADHARNO"));
					//hdcc_acc.setAcccreatedate(resultSet.getString("HDCC_ACCOUNTCREATEDATE"));
					//hdcc_acc.setIfsc(resultSet.getString("HDCC_IFSCNUMBER"));					
				}
				BigDecimal  newamount  = new BigDecimal(amount);
				BigDecimal  accbalance  = new BigDecimal(hdcc_acc.getAccbalance());
					BigDecimal sum = accbalance.add(newamount);
					sql = "UPDATE HDCC_ACCOUNT SET HDCC_ACCOUNTBALANCE = ? WHERE HDCC_ACC_AADHARNO = ? ";
					preparedStatement = connection.prepareStatement(sql);
					preparedStatement.setBigDecimal(1,sum);
					preparedStatement.setString(2, hdcc_aadharno);
					updatecount = preparedStatement.executeUpdate();
					return updatecount;
				}
	  		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	  			}
	  			finally {
	  				try {
	  					connection.close();
	  				} 
	  				catch (SQLException e) {
	  					System.out.println("Exception :: " + e.getMessage());
	  				}
	  			}
	  				return 0;
			}			
	}

    
    
    
    
    
    
    
    